# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
def format(s,uselocale,*sep):
	"""Return first page. ie 123-127 => <sep1>123<sep2>"""
	if not s: return ''		 # if no page we return an empty string
	sep0 = sep[0].split('|')[0]	# singular form
	sep1 = sep[1].split('|')[0]	# singular form
	try:
		p1,p2 = s.split('-')
		return sep0 + p1 + sep1
	except:
		return sep0 + s + sep1	# no page range or pages != integer


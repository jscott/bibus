# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
def format(s,uselocale,*sep):
	"""Return abbreviated pages. ie 123-127 => <sep1>123-7<sep2>
	<sep1> = singular form|plural form
	<sep2> = idem
	"""
	if not s: return ''		 # if no page we return an empty string
	#
	if sep[0].find('|') != -1: sep0 = sep[0].split('|')[1]	# plural form
	else: sep0 = sep[0]
	if sep[1].find('|') != -1: sep1 = sep[1].split('|')[1]	# plural form
	else: sep1 = sep[1]
	try:
		p1,p2 = s.split('-')
		p1=p1.strip()
		p2=p2.strip()
		if len(p2) != len(p1) or int(p2)<int(p1):
			return sep0 + s + sep1
		else:
			for pos in xrange(len(p1)):
				if p1[pos] != p2[pos]:
					return sep0 + '-'.join((p1,p2[pos:])) + sep1

	except:
		return sep[0].split('|')[0] + s + sep[1].split('|')[0]	# no page range or pages != integer => singular form


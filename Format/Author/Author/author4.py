# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
def __abbrev(st,s,sep2,sep3):
	"""sep2 is put after each letter of the abbreviated forename and sep3 at the end. Remove trailing ' '"""
	return (sep2.join(map(lambda x:x[0].upper(),st.split(s)))+sep3)

def __abbreviate(s,sep2,sep3):
	"""We take the first letter of each forename. Separated by ' ' or '-' in that order"""
	if s.find(' ') != -1: return __abbrev(s,None,sep2,sep3)
	elif s.find('-') != -1: return __abbrev(s,' ',sep2,sep3)
	else: return (s[0]+sep3)

def format(s,uselocale,*sep):
	"""s = Name, Firstname. sep0,sep1 = separators
	out = 'Martineau<sep1>P<sep2>E<sep2>U<sep3>"""
	try:
		name,firstname = s.split(',')
	except ValueError:
		name,firstname = s,''
	name = name.strip()
	firstname = firstname.strip()
	if name != '' and firstname != '':
		return u"%s%s%s" %(name,sep[0],__abbreviate(firstname,sep[1],sep[2]))
	elif (name,firstname) == ('',''):
		return u''
	elif firstname == '':
		return name
	else:	# name =''
		return __abbreviate(firstname,sep[1],sep[2])


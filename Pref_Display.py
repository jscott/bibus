#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
# generated by wxGlade 0.4.1 on Sat Dec 02 22:10:57 2006

import wx
import BIB

COLORS = {_('Black'):wx.BLACK,_('White'):wx.WHITE,_('Red'):wx.RED,_('Blue'):wx.BLUE,_('Green'):wx.GREEN,_('Cyan'):wx.CYAN,_('Light_grey'):wx.LIGHT_GREY}

class Pref_Display_Base(wx.Panel):
    def __init__(self, *args, **kwds):
        # begin wxGlade: Pref_Display_Base.__init__
        kwds["style"] = wx.TAB_TRAVERSAL
        wx.Panel.__init__(self, *args, **kwds)
        self.sizer_3_staticbox = wx.StaticBox(self, -1, _("Reference display"))
        self.sizer_2_staticbox = wx.StaticBox(self, -1, _("Reference list"))
        self.label_1 = wx.StaticText(self, -1, _("Reference fields"))
        self.fields = wx.ListBox(self, -1, choices=[])
        self.b_add = wx.Button(self, -1, _("Add"))
        self.b_remove = wx.Button(self, -1, _("Remove"))
        self.b_up = wx.Button(self, -1, _("Up"))
        self.b_down = wx.Button(self, -1, _("Down"))
        self.label_2 = wx.StaticText(self, -1, _("Displayed fields"))
        self.fieldsChoice = wx.ListBox(self, -1, choices=[])
        self.label_3 = wx.StaticText(self, -1, _("Field name color"))
        self.key_color = wx.Choice(self, -1, choices=[])
        self.label_4 = wx.StaticText(self, -1, _("Field color"))
        self.text_color = wx.Choice(self, -1, choices=[])

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.add, self.b_add)
        self.Bind(wx.EVT_BUTTON, self.remove, self.b_remove)
        self.Bind(wx.EVT_BUTTON, self.up, self.b_up)
        self.Bind(wx.EVT_BUTTON, self.down, self.b_down)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: Pref_Display_Base.__set_properties
        pass
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: Pref_Display_Base.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.StaticBoxSizer(self.sizer_3_staticbox, wx.HORIZONTAL)
        grid_sizer_1 = wx.FlexGridSizer(2, 2, 5, 5)
        sizer_2 = wx.StaticBoxSizer(self.sizer_2_staticbox, wx.HORIZONTAL)
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        sizer_6 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_4.Add(self.label_1, 0, wx.ADJUST_MINSIZE, 0)
        sizer_4.Add(self.fields, 1, wx.TOP|wx.BOTTOM|wx.EXPAND|wx.ADJUST_MINSIZE, 5)
        sizer_2.Add(sizer_4, 1, wx.EXPAND, 0)
        sizer_6.Add(self.b_add, 0, wx.ADJUST_MINSIZE, 0)
        sizer_6.Add(self.b_remove, 0, wx.TOP|wx.ADJUST_MINSIZE, 2)
        sizer_6.Add(self.b_up, 0, wx.TOP|wx.ADJUST_MINSIZE, 10)
        sizer_6.Add(self.b_down, 0, wx.TOP|wx.ADJUST_MINSIZE, 2)
        sizer_2.Add(sizer_6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_5.Add(self.label_2, 0, wx.ADJUST_MINSIZE, 0)
        sizer_5.Add(self.fieldsChoice, 1, wx.TOP|wx.BOTTOM|wx.EXPAND|wx.ADJUST_MINSIZE, 5)
        sizer_2.Add(sizer_5, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)
        grid_sizer_1.Add(self.label_3, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.key_color, 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.label_4, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.text_color, 0, wx.ADJUST_MINSIZE, 0)
        sizer_3.Add(grid_sizer_1, 1, wx.EXPAND, 0)
        sizer_1.Add(sizer_3, 0, wx.EXPAND, 0)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        sizer_1.SetSizeHints(self)
        # end wxGlade

    def add(self, event): # wxGlade: Pref_Display_Base.<event_handler>
        print "Event handler `add' not implemented!"
        event.Skip()

    def remove(self, event): # wxGlade: Pref_Display_Base.<event_handler>
        print "Event handler `remove' not implemented!"
        event.Skip()

    def up(self, event): # wxGlade: Pref_Display_Base.<event_handler>
        print "Event handler `up' not implemented!"
        event.Skip()

    def down(self, event): # wxGlade: Pref_Display_Base.<event_handler>
        print "Event handler `down' not implemented!"
        event.Skip()

# end of class Pref_Display_Base


class Pref_Display(Pref_Display_Base):
    def __init__(self, parent, id, (LIST_DISPLAY,KEY_COLOR,TEXT_COLOR)):
        self.LIST_DISPLAY = LIST_DISPLAY
        self.KEY_COLOR = KEY_COLOR
        self.TEXT_COLOR = TEXT_COLOR
        Pref_Display_Base.__init__(self,parent,id)

    def _Pref_Display_Base__set_properties(self):
        Pref_Display_Base._Pref_Display_Base__set_properties(self)
        self.fields.AppendItems([BIB.NAME_FIELD[field] for field in BIB.BIB_FIELDS])
        self.fieldsChoice.AppendItems([BIB.NAME_FIELD[field] for field in self.LIST_DISPLAY])
        self.key_color.AppendItems(COLORS.keys())
        self.text_color.AppendItems(COLORS.keys())
        for coln,col in COLORS.iteritems():
            if col == self.KEY_COLOR:
                self.key_color.SetStringSelection(coln)
                break
        for coln,col in COLORS.iteritems():
            if col == self.TEXT_COLOR:
                self.text_color.SetStringSelection(coln)
                break

    def add(self, event): # wxGlade: Pref_Display.<event_handler>
        if self.fields.GetSelection() != -1:
            self.fieldsChoice.Append(self.fields.GetStringSelection())
            #self.fields.Delete(self.fields.GetSelection())
            
    def remove(self, event): # wxGlade: Pref_Display.<event_handler>
        if self.fieldsChoice.GetSelection() != -1:
            #self.fields.Append(self.fieldsChoice.GetStringSelection())
            self.fieldsChoice.Delete(self.fieldsChoice.GetSelection())
            
    def up(self, event): # wxGlade: Pref_Display.<event_handler>
        selId = self.fieldsChoice.GetSelection()
        if selId > 0:    # it is not the first item
            selStr = self.fieldsChoice.GetStringSelection()
            self.fieldsChoice.Delete(selId)
            self.fieldsChoice.InsertItems([selStr],selId-1)
            self.fieldsChoice.SetSelection(selId-1,True)
            
    def down(self, event): # wxGlade: Pref_Display.<event_handler>
        selId = self.fieldsChoice.GetSelection()
        if selId+1 < self.fieldsChoice.GetCount() and selId != -1:    # not the last item
            selStr = self.fieldsChoice.GetStringSelection()
            self.fieldsChoice.Delete(selId)
            self.fieldsChoice.InsertItems([selStr],selId+1)
            self.fieldsChoice.SetSelection(selId+1,True)
            
    def getSettings(self):
        return tuple([BIB.FIELD_NAME[field] for field in self.fieldsChoice.GetStrings()]),\
                COLORS[self.key_color.GetStringSelection()],\
                COLORS[self.text_color.GetStringSelection()]

# end of class Pref_Display



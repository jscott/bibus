# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# EndNote refer format
# from http://www.ecst.csuchico.edu/~jacobsd/bib/formats/endnote.html
#
import BIB

DEFAULT_ENCODING = 'latin_1'

class exportRef(object):
	"""Class is iterable. Return records one by one."""
	# conversion OOo <-> EndNote Publication Type: dictionary Type[OOo Name]=EndNote/refed.
 	Type={
	'ARTICLE':'Journal Article' ,
	'BOOK':'Journal Article' ,
	'BOOKLET':'Journal Article' ,
	'CONFERENCE':'Congresses' ,
	'INBOOK':'Journal Article' ,
	'INCOLLECTION':'Journal Article' ,
	'INPROCEEDINGS':'Congresses' ,
	'JOURNAL':'Newspaper Article' ,
	'MANUAL':'Technical Report' ,
	'MASTERTHESIS':'Journal Article' ,
	'MISC':'Journal Article' ,
	'PHDTHESIS':'Journal Article' ,
	'PROCEEDINGS':'Congresses' ,
	'TECHREPORT':'Technical Report' ,
	'UNPUBLISHED':'Journal Article' ,
	'EMAIL':'Journal Article' ,
	'WWW':'Journal Article' ,
	'CUSTOM1':'Journal Article' ,
	'CUSTOM2':'Journal Article' ,
	'CUSTOM3':'Journal Article' ,
	'CUSTOM4':'Journal Article' ,
	'CUSTOM5':'Journal Article'}

	def __init__(self,infile):
		self.infile = infile	# must be a file type. Need a write() function.

	def write(self,ref):
		"""write(ref)"""
		record = self.__convertRecord(ref)
		if record:
			self.infile.write(record)
			self.infile.write('\n\n')		# add a blank line to separate records

	def __convertRecord(self,ref):
		"""Convert a OOo reference to an endnote record
		input ==
		('Id','Identifier', 'Bibliographic_Type', 'Address', 'Annote', 'Author', 'Booktitle', 'Chapter', 'Edition', 'Editor','HowPublished', 'Institution', 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher', 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1', 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN','Abstract')"""
		#
		record=[]
		# Type
		record.append( "PT  - %s"%exportRef.Type[BIB.BIB_TYPE[ref[2]]] )
		# Address
		if ref[3] and ref[11]:
			record.append( "AD  - %s. %s"%(ref[11],ref[3]) )
		elif ref[3]:
			record.append( "AD  - %s"%ref[3] )
		elif ref[11]:
			record.append( "AD  - %s"%ref[11] )
		# Annote
		#if ref[4]: record.append( ????
		# Author
		if ref[5]:
			record.extend( map( lambda x: "FAU  - %s"%x , ref[5].split(BIB.SEP) ) )
			record.extend( map( lambda x: "AU  - %s"% ' '.join( map(lambda x:x.strip(), x.split(',')) ), ref[5].split(BIB.SEP) ) )
		# Booktitle
		if ref[6]:
			record.append( "TI  - %s"%ref[6] )
		# Chapter
		# if ref[7]: Don't know where to put the chapter
		# Edition
		# if ref[8]: record.append( "IP  - %s"%ref[8] )
		# Editor
		# if ref[9]: record.extend( map( lambda x: "FAU %s"%x , ref[9].split(BIB.SEP) ) )
		# HowPublished
		# if ref[10]: record.append( "%%9 %s"%ref[10] )
		# Institution
		# if ref[11]: see AD = address
		# Journal
		if ref[12]:
			record.append( "TA  - %s"%ref[12] )
		# Year(24) Month(13)
		if ref[24]:
			record.append( "DP  - %s %s"%(ref[24],ref[13]) )
		# Note
		#if ref[14]:
		# Number
		if ref[15]:
			record.append( "IP  - %s"%ref[15] )
		# Organizations
		# see Institution
		# Pages
		if ref[17]:
			record.append( "PG  - %s"%ref[17] )
		# Publisher
		# if ref[18]: record.append(
		# School
		# if ref[19]:
		# Series
		# if ref[20]: record.append(
		# Title
		if ref[21]:
			record.append( "TI  - %s"%ref[21] )
		# Report_Type
		# if ref[22]: record.append( "%%9 %s"%ref[22] )
		# Volume
		if ref[23]:
			record.append( "VI  - %s"%ref[23] )
		# YearVI
		# see Month
		# URL
		if ref[25]:
			record.append( "URLS  - %s"%ref[25] )
		# Custom1 ... Custom5
		# Where to put them ?
		# ISBN
		#if ref[31]:
		# Abstract
		if ref[32]:
			record.append( "AB  - %s"%ref[32] )
		#
		return '\n'.join(record)

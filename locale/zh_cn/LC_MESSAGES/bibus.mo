��    �     �    ,      �!  F  �!     �"     #     #      #     )#     +#     =#  -   M#     {#     ~#     �#     �#     �#     �#     �#     �#     �#     �#     �#  
   �#  0   $  	   3$     =$     C$  	   G$     Q$  	   `$     j$     x$     �$  	   �$     �$     �$     �$     �$  
   �$     �$     �$     �$     %     %     2%  #   A%  w   e%  M  �%     +'     1'     6'  	   ;'     E'     N'  7   U'     �'     �'     �'     �'     �'     �'  /   �'     (      (     3(     H(     `(  !   |(  &   �(     �(     �(     �(     �(     �(     �(     
)     )     ")     ')     -)     D)  !   Z)  !   |)     �)     �)     �)     �)     �)     �)     �)     �)     �)     *     *     +*     =*     N*     \*     u*     |*     �*     �*     �*  $   �*  
   �*     �*     +     +     +     1+     ?+     D+  
   I+     T+     Y+     b+  
   r+     }+     �+     �+     �+     �+     �+     �+     �+     �+     �+      ,     ,     ',  "   <,     _,     {,     �,     �,  
   �,     �,     �,     �,     �,     �,     �,     �,     �,     -     -     5-     U-     m-     z-  	   �-     �-     �-     �-     �-     �-     �-     �-  
   .     .  '   5.     ].     r.     ~.     �.     �.  *   �.     �.     �.     �.  *   �.     /     3/     M/     T/     c/     x/     �/     �/  )   �/     �/     �/     �/     0     0  '   20  �   Z0     1      1     (1     11     :1     F1     R1  
   Z1  H   e1  $   �1     �1     �1     �1     2     2     -2  )   B2     l2     t2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2  	   3  	   3     3     #3  	   *3     43     93     J3     Q3     V3     ^3     m3  	   3     �3     �3     �3     �3     �3     �3     �3     4  
   4     4     4     (4     14     74     =4  	   B4     L4  %   l4     �4     �4     �4      �4      5  $   5     B5     G5     P5     n5     z5     �5  	   �5     �5  	   �5     �5     �5     �5     �5     �5     
6     6  
   6     "6     &6     *6     06  	   @6     J6     [6     m6     ~6     �6  
   �6     �6     �6     �6     �6     �6     �6     �6     7     7     7  
   )7     47     ;7  
   K7     V7     d7  	   }7     �7  
   �7  
   �7  	   �7     �7     �7     �7     �7  
   �7      8     8     '8     <8     M8  	   Z8     d8  
   m8     x8  0   8  Z   �8  >   9  M   J9  =   �9  g   �9  "   >:  <   a:     �:     �:     �:  
   �:      �:     �:     ;     ;     ;  	   %;     /;     <;     P;     c;  
   {;  	   �;     �;     �;     �;     �;     �;  
   �;     �;     �;     <     <     &<     /<  ;   ;<  5   w<  '   �<  [   �<  p   1=  #   �=  	   �=     �=  	   �=     �=     �=  %   �=      >     3>     K>     j>     �>     �>     �>     �>     �>     �>     �>     �>     ?     ?  �   ?  K   �?  B   @  #   T@  3   x@  c   �@     A     A     A     %A  $   -A     RA     XA     gA     oA     |A     �A     �A     �A     �A     �A     �A     �A  f  	B    pC     xD     �D     �D     �D     �D     �D     �D  +   �D     E     E     E      E     'E     :E     HE     OE     XE     eE     �E     �E  $   �E     �E     �E     �E     �E     �E     �E  	   F     F     F     ,F     3F     :F     GF     ZF     gF     tF     �F     �F     �F     �F     �F  )   �F  s   �F  *  YG     �H     �H     �H     �H     �H     �H  5   �H     �H  	   �H     �H     I     I     +I  '   >I     fI     mI     �I     �I     �I     �I     �I     �I  	   �I     �I     J     J      J     -J     :J     AJ     HJ     OJ     hJ  !   uJ     �J     �J  	   �J     �J     �J  	   �J     �J     �J     �J     K     K     ,K     <K     OK     bK     rK     �K     �K     �K     �K  	   �K     �K     �K  	   L     L     L     L     ,L     3L     :L     AL     HL  	   OL     YL     fL     sL     �L     �L     �L     �L  	   �L     �L     �L     �L     �L     �L     M     %M  !   8M     ZM     sM  	   zM     �M  	   �M     �M     �M     �M     �M     �M  	   �M     �M     �M     N     N     N     -N     @N     MN     ZN     aN     hN  $   �N     �N     �N     �N     �N     
O     O  &   =O     dO     tO     �O     �O     �O     �O     �O     �O     �O  *   �O     P     +P     DP     KP     ZP     mP     �P     �P     �P     �P     �P     �P     �P     �P     Q  �   0Q     �Q     �Q     �Q     �Q     �Q     �Q     R     
R  <   R  '   NR  	   vR     �R  	   �R     �R     �R     �R     �R     �R     S     S     &S     3S     CS     JS     NS     ]S     dS     qS  	   xS     �S     �S     �S     �S  	   �S  	   �S     �S     �S     �S     �S     �S  	   �S  	   T     T      T     'T     .T     ;T     RT     aT     vT     �T     �T     �T     �T     �T     �T     �T     �T  	   �T     �T     �T     U     U     .U     DU     ZU     mU     �U     �U     �U     �U     �U  	   �U  	   �U     �U  	   �U     �U     �U     V     V     V     8V     ?V     FV     RV     VV     ]V     dV     yV     �V     �V     �V     �V     �V     �V     �V     �V     �V     �V     W      W     -W     4W     JW     QW     ^W     kW     rW  	   �W     �W     �W     �W     �W  	   �W     �W     �W  !   �W  
   X  
   X  
   !X  	   ,X     6X     CX     VX     iX  	   yX     �X     �X     �X  	   �X  "   �X  G   �X  -   Y  A   GY  9   �Y  N   �Y     Z     Z     8Z     EZ     RZ     _Z     fZ     �Z     �Z     �Z     �Z  	   �Z     �Z     �Z     �Z     �Z  	   �Z     �Z  	   
[     [     ![     .[     5[     H[     O[     V[     i[  	   �[     �[     �[     �[  *   �[     �[  /   \  ?   @\     �\  	   �\     �\  	   �\     �\     �\  !   �\     �\     ]     ]     ,]  	   L]  	   V]     `]     p]     w]     �]     �]     �]     �]     �]  �   �]  @   o^  ,   �^     �^  :   �^  F   +_     r_     v_     �_     �_  &   �_     �_     �_     �_     �_     �_     �_     `     (`      /`     P`     ]`  	   m`     �       8      @   m      Z   5  �           {   ^   G      ?       |      D   �         �   �   q   �   U  �   .  `           �       ~       �       �              �   o      j   r        �   �   l   �  c   D      "       �   z   ]  �   �          K          i       /   >      w   �   �   E      q  �           �         f  7      6   �   ,       n  �       �     9  -  �   6  P      �   �     3      &      �                   �          �           �              �   g  �           ;   4       B  �           �   �   �   x          9           �       v              �   &   X   �       B       )   �                  Q   �         F   �  L   M   /  �       �   �             �         #  �   u   o        E  �  �   I      �  �   Y  �   U       �   �   �               )  �   M      �     �      �      W   e   �   �   	  �   k  �   �   �   ]   g           u      Z  �   1   .       �   �   l  
  �                     #      8           f           �                 +            �   �    ^      d  �     N   '           �   �       J       =     ;  �   <      R       
       �       [   �   N  �       �   k   �  %  C       h   V      A  A           �   +       V       2   s   $      _  (              }   x  t   S   	   �   F  �   [  ?      �   P      c  �       �   :       '     �   �  �   i      �   �   �   �   �  �   �   �       �   m      ~      7       �   y   X  Q      z  �             `  |      *  p  n       �   0  y  1  b  2    �   Y       �   �       �   �   >   �   !   C       a   @  �             �  J      R      �           �      �       �   O     \  4      H   �       K   {  �   e  (   �              S     �   �  d   �   s  G   *   v   �  �   $       I   0       �   �   r          w      �     _   �  <       h  ,  �   W  �   �     H       \   �   j     O   -   �     5   !  �       b   �       %   p   �       t  �   3    �          :  �   �       T   �   T              �   �     �   �   "  �   L  =  �           a  }   
Bibus can directly insert citations in OpenOffice.org
if you start OpenOffice.org in 'listening mode'.


Do you want to activate this mode by default?


A document will open in OpenOffice.org,
Click on 'Accept UNO connections',
Then restart OpenOffice.org.
Under Windows, you must also quit the OpenOffice.org Quickstarter.

 %s connection parameters %s references : %s selected ,  , et al. - - Click on Cancel - Click on Next - Follow the instructions in mysql_config.txt -- :  ;  ARTICLE Abbreviate author list with Acrobat file Activate Add Add Text Add a child to the selected key Add a field Add a line Add a reference associated with the selected key Add child After All All files All references Anonymous Anonymous ... Anonymous author format As in database Ascending Author Author list Author list format Author-Date Base Style Before last BibTeX Bibliographic index BibliographicType Bibliography title Bibus WEB site Bibus may use two database engines: Bibus needs a USERNAME to identify yourself.
Please enter a name in the box below.
Your login name might be a good idea Bibus uses a file to store the bibliographic database.
Please enter in the box below the path to 
the bibliographic database you want to use.
- If the file EXIST, Bibus will 
	- not modify it
	- assume it to be a correct Bibus database.
- If the file DOES NOT EXIST, Bibus will 
	- create it
	- use it as your bibliographic database. Black Blue Bold Booktitle Brackets Cancel Cannot connect to Openoffice. See Documentation.
%s
%s) Caps Capture ... Capture Citations Capture from database Capture from field Capture selection Capture the references absent from the database Case Change author list Check for duplicates Choose a File to import Choose the destination file Choose the file location and name Choose the name of the SQLite database Citation Cited Clear Connect Connect to database Connection settings Connection type Content Copy Count Create Index on Insert Create a new category Create the Bibliography if absent Creating or choosing the database Creating styles ... Current key Cut Cyan Database Database = %s Database = None Database Error Database Name Database choice Database engine Database file ... Database file... Database type Database used at startup Delete Delete query Delete reference Delete the selected reference Delete this key Deleting old index and citations ... Descending Directory ... Display Displayed fields Document position Documentation Done Down Duplicates Edit Edit ... Edit Identifier Edit query Edit reference Edit shortcuts ... Edit the selected reference Editor Encoding Error Error during query Expert Expert Search Export Export a file Export to Medline format Export to RIS format Export to Refer format for EndNote Export to a SQLite database Field Field Value Field color Field name Field name color Fields Fields formatting Fields ordering File File ... File name ... Fill character in tab stops Finalize Finalize the document Finalizing the current document First Connection Wizard First Editor First author First key Format Format Author as Format Bibliography Format Editor as Format fields as Format reference as Formating citations ... Formatting Fuse: [1] [3] [2] -> [1; 3; 2] Fuse: [Martin] [John] -> [Martin; John] Fusing citations ... GoTo Writer Green Help Hilight Citations Hilight citations with a yellow background Host If a duplicate is found If at least If author Field is empty, replace it with: If you want to use MySQL If you want to use SQLite Import Import && Quit Import a BibTeX file Import a Medline file Import a RIS file Import a Refer file Import a Refer file exported from EndNote Import a file Import a text file Insert Insert Citation Inserting citations in text Inserting citations in text (%s ranges) It seems that your database is not compatible with your PySQLite version.
If it does not work,
see the SQLite site about database format change in SQLite3 http://www.sqlite.org/version3.html Italic Joining Key type Language Last Editor Last author License Light_grey List all authors on first occurence if authors number is not higher than List more authors until it is unique Load ... Looking for duplicates ... Main Fields Main Fields = %s Main fields (+ Abstract) Main fields searched Making a copy of the current document ... Medline Medline Search Microsoft Word Document Middle Editors Middle authors Mode Month MySQL or SQLite Name Name format New New ... New Name format New Query New query New reference NewKey No Choice None Norma Jean Baker Normal Note Nothing Number entries Number of records Numbering OK Online Open URL OpenOffice connection settings OpenOffice.org OpenOffice.org Text Document OpenOffice.org connection Other Fields Page Setup Pages PassWord Password Paste Paths Pipe Pipe name Please choose the file encoding Please enter password for database %s Please select a style file Please select the file Please, enter the new name Please, enter the new query name Please, enter the query name Please, select the default directory Port Position Position of the tab stop (mm) Preferences Preview Preview ... Preview : Preview printing Print ... Print references Printed fields Printing Pubmed search ... Put a letter after the year Queries Quit Quit Bibus RIS Red Refer Refer (EndNote) Reference Reference Editor Reference display Reference fields Reference list Reference type References References formatting Remove Rename Query Rename folder Rename query Rename this key SQLite SQlite database file Save Save as ... Save as... Search Search && Close Search ... Search PubMed Search Pubmed on the Web Search in Search the database Second key Select All Selection Separate authors with Separator 1 Separator 2 Separator 3 Separators Separators ... Set printer page format Set some preferences Setting username Settings ... Shortcuts Show All Small Caps Socket Solving duplicates ... (%s series of duplicates) Some needed fields are absent from the selected database/tables.
Continue at your own risk Sorry but codec '%s' is not available.
I will try to use ascii Sorry but the SQLite python extension is not available.
Read installation.txt Sorry but the key you are moving already exist in this folder Sorry, but I was not able to find the python module for the %s database.
Please check your installation Sorry, you cannot modify this key. Sorry, you cannot move a reference associated with this key. Sort Ascending Sort Descending Sort bibliography by Sort order Sort: [1] [3] [2] -> [1] [2] [3] Sorting Standard Start at record Style Style ... Style author Style creation date Style informations Style modification date Style name Subscript Superscript Supplementary Fields Supplementary fields TCP/IP Tab stop is right aligned Tabulation Tag Tag reference Tag the selected reference Tagged Template Text Window The identifier was not unique and it has been changed to %s The key must be unique.
Please choose a new key name. The selected style is not a bibus style The version of the style file is too old.
 I will convert it to the new Bibus style format. The version of the style file is too recent. Please update bibus to a new version.
 I will open a default style. There is %s such records in Medline Third key Title Underline Up Update Index on Insert Update the Bibliography automatically Updating index ... Updating references ... Use current language for month Use range: [1] [3] [2] -> [1-3] UserName Username Using separator Warning Welcome to Bibus! Welcome to bibus What do you want to export? Where to save the style file? White Year You can set here the environment variable $FILES.
This variable is substituted when an URL is opened.
This allows you to set a central repository
for all the articles.
 You cannot edit default styles
.I have made a copy of the style for editing You cannot use %r and %r characters.
Please choose a new key name. You did not select a valid database You must first edit Pref/Shortcuts to use this menu You must first save the current document before using this function.
 Should I save it and proceed? ]-[ a Field a String authors authors, replace authors 2 to n with below case sensitive default in 'ARTICLE' in database keep the new reference keep the old reference last save password (Not secure!) shortcut using separator when there are more than Project-Id-Version: Bibus
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-09-30 21:25+0200
PO-Revision-Date: 2006-02-13 20:46+0800
Last-Translator: Pierre Martineau <pmartino@users.sourceforge.net>
Language-Team: Chinese/Simplified <i18n-translation@lists.linux.net.cn>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit 

要不要用默认方式启动？


点“启动”将打开一个OpenOffice.org文件，
请启用宏，在文件中点击“Accept·UNO·connections”
然后重启OpenOffice.org。
如果是在Windows中，还需要退出OpenOffice.org的快速启动器。

 %s连接参数 选中%s个条目中的%s个 ， , et al.
，等 - 请点击“取消” 请点击“下一步” 根据mysql_config.txt的内容进行设置 -- ： ； 文章 缩写作者列表 Acrobat文件 启动 添加>> 添加文本 为选定键添加一个子键 添加字段 添加一行 为选定键添加一个参考文献 添加子目录 后于 全部 所有文件 所有引用 匿名 匿名... 匿名作者格式 同数据库 升序 作者 作者列表 作者列表格式 写作日期 基础风格 倒数第二 BibTeX 书目索引 书目类型 书目抬头 Bibus网站 Bibus可以使用两种数据库引擎： Bibus需要一个用户名来标识你自己
请在下面的对话框中输入用户名
你的登录名不错呀！ Bibus使用一个文件来存贮书目数据
请在下面的对话框中输入你想要使用的书目数据库的路径
-如果这个文件已经存在，
	-Bibus不会改动它
	-而假定它就是一个正确的Bibus数据库
-如果不存在，Bibus会新建一个
	-作为你的书目数据库 黑色 蓝色 粗体 书名 括号 取消 无法与Openoffice连接，请查阅文档。
%s
%s) 大写 捕获... 捕获引用 从数据库中捕获 从字段中捕获 捕获当前选择 捕获数据库中没有的参考文献 案例 改变作者列表 查找重复 请选择要导入的文件 选择目标文件 选择文件位置及名字 选择SQLite数据库 引用 已引用 清除 连接 连接到数据库 连接设置 连接类型 目录 复制 计数 在插入时新建索引 新建类别 如果没有书目就新建一个 新建或选择数据库 新建风格... 当前键 剪切 青色 数据库 数据库%s 数据库=没有 数据库出错 数据库名 选择数据库 数据库引擎 数据库文件... 数据库文件... 数据库类型 启动时载入的数据库 删除 删除查询 删除参考文献 删除选定的参考文献 删除键 删除旧的索引和引用... 降序 目录... 显示 显示字段 文件位置 说明 完成 向下 重复 编辑 编辑... 编辑标志 编辑查询 编辑参考文献 编辑快捷方式... 编辑选定的参考文献 编辑 编码 出错了 检索时出错 专家 高级搜索 导出 导出一个文件 导出为Medline格式 导出为RIS格式 导出为EndNote格式Refer文件 导出到SQLite数据库 字段 字段值 字段颜色 字段名 字段名颜色 字段 字段格式 字段顺序 文件 文件... 文件名... 在制表符后填入字符 完成 完成文档 完成当前文档 初次连接向导 第一编者 第一作者 首键 格式 将作者格式排列为 自动应用引用参考文献格式 将编者格式排列为 将字段格式排列为 按以下格式排列引用 正在格式化引用 自动应用格式 融合：[1][3][2]->[1；2；3] 融合：[Martin][John]->[Martin;John] 融合引用... 转到文字编辑器 绿色 帮助 高亮引用 以黄色背景高亮引用 主机 如果发现重复 如果至少 如果作者字段为空，则替换为： 如果你想使用MySQL 如果你想使用SQLite 导入 导入&&退出 导入BibTeX文件 导入Medline文件 导入RIS文件 导入文献管理文件 导入EndNote格式Refer文件 导入文件 导入文本文件 插入 插入引用 在文字中插入引用 插入引用(%s个范围) 你的数据库与你的PySQLite版本不兼容
如果不能运行，请到SQLite网站查找数据库格式资料
http://www.sqlite.org/version3.html 斜体 正在合并 键类 语言 末位编者 末位作者 许可 浅灰 在首次出现时列出所有作者，如果作者数少于 列出更多作者，直到成为唯一 载入... 寻找重复问题... 主字段 主字段=%s 主要字段(+摘要) 要检索的主要字段 备份当前文档... Medline数据库 搜索Medline Microsoft Word文档 中间编者 中间的作者 模式 月 MySQL或SQLite 名字 名字格式 新建 新建... 新名字格式 新建查询 新建查询 新参考文献 新建键 无选择 没有 Norma·Jean·Baker 正常 注释 无 入口数 记录数 正在加序号 确定 在线 打开网址 OpenOffice连接设置 OpenOffice.org OpenOffice.org文档 连接OpenOffice.org 其它字段 页面设置 页码 密码 密码 粘贴 路径 管道 管道名 请选择文件编码 请输入数据库密码 请选择风格文件 请选择文件 请输入新的名字 请输入新查询名 请输入查询名 清选择默认目录 端口 位置 制表符位置(厘米) 选项 预览 预览... 预览： 打印预览 打印... 打印参考文献 已打印的字段 打印 检索Pubmed... 在年份后加上字符 查询 退出 退出Bibus RIS 红色 引用 Refer文件(EndNote) 参考文献 引文编辑器 引用显示 引用字段 引用列表 引用类型 参考文献 引用格式 <<移除 重命名查询 重命名文件夹 重命名查询 重命名键 SQLite SQlite数据库文件 保存 另存为... 另存为... 搜索 搜索&&关闭 查找... 检索PubMed 检索Pubmed网络数据库 搜索 在数据库中检索 第二键 全选 选择 用于分隔多个作者和符号 分隔符1 分隔符2 分隔符3 分隔符 分隔符... 设置打印纸型 设置一些选项 设置用户名 设置... 快捷方式 全部显示 小写 套接字 解决重复问题...(%s个重复) 所选的数据库/表缺少一些必需的字段
请自行承担风险 ‘%s’ 转换器不存在
将使用ascii码 SQLite的python扩展不可用，
请阅读installation.txt文件 对不起，要移动的键已经在目标目录中了。 对不起，无法找到数据库%s的python模块。
请检查安装情况。 无法修改 对不起，无法移动 升序排列 降序排列 排序书目 排序 排序：[1][3][2]->[1][2][3] 排序 标准 记录时启动 风格 风格... 风格作者 风格创建日期 风格信息 风格改动日期 风格名 下级脚本 超脚本 补充字段 补充字段 TCP/IP 制表符右对齐 列表 标签 标记参考文献 标记选定的参考文献 已标记 模板 从文本窗口导入 标志不唯一已改为%s 键名必需唯一，请选择别的名字 所选风格不是bibus风格 风格文件版本太旧
将转换为新格式2 风格文件太新，请更新bibus
现在先使用默认风格 Medline中有%s记录 第三键 标题 下划线 向上 在插入时更新索引 自动更新插入的参考文献 更新索引... 更新引用... 应用当前语言格式 使用范围：[1][3][2]->[1-3] 用户名 用户名 使用分隔符 警告 欢迎使用Bibus！ 欢迎使用bibus 你要导出什么？ 把风格文件存到哪里？ 白色 年 你可以在这里为$FILES设置环境变量
这个变量会在打开URL时自动替换
这样你就可以为所有文章设定一个中心库
 默认风格不可编辑
我已另外复制了一个用于编辑 不能使用%r 和 %r 字符
请重新命名 数据库不存在 你必需先编辑选项/快捷方式才能使用本菜单 必需先保存文档才能使用这个功能。
保存并继续吗？ ]-[ 一个字段 一个字符串 作者 多位作者，将作者2到n替换为 以下 大小写敏感 默认 在“文章” 数据库内 保留新参考文献 保留旧参考文献 末次 保存密码(这样不安全！) 快捷方式 使用分隔符 当多于 
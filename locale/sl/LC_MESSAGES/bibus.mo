��    N     �    �$      H1  @  I1     �2     �2     �2  <   �2     �2     3     3     3  o   3  o   �3  ;   �3     84     ;4     @4     C4     F4     N4     j4     s4     �4     �4     �4     �4     �4  
   �4  0   �4  	   �4     5     5     %5     +5  	   /5     95     H5  	   O5     Y5     g5     5  	   �5     �5  �   �5  �   I6  �   �6     �7     �7     �7     �7     �7  
   �7  X   �7     N8     Z8     a8     u8     �8     �8     �8  B   �8  w   �8  P  v9     �:     �:     �:  	   �:     �:  
   �:     �:     �:     ;     ;     ;     ;  0   $;  7   U;     �;     �;     �;     �;     �;     �;  /   �;     <      <     3<     ;<     J<     _<     w<  !   �<  &   �<     �<     �<     �<     �<     =     =     =     =     !=  
   5=  
   @=     K=     [=     c=     h=     n=     �=     �=  !   �=     �=     �=     �=     �=      >     >     >     >      >     (>     ,>  	   1>     ;>     D>     R>     b>     t>     �>     �>     �>     �>     �>  p   �>     @?     T?     d?     s?     �?     �?     �?     �?     �?     �?     �?     �?     �?     @      @  $   0@  
   U@     `@     h@     v@     ~@     �@     �@     �@     �@     �@     �@     �@     �@  
   �@     A     A     A     A     +A  
   8A     CA     RA     eA     �A     �A     �A     �A  &   �A  "   �A     B     B     B  >   ,B     kB     rB  $   �B     �B     �B     �B  +   �B     �B     C     'C  "   <C     _C     {C     �C     �C  
   �C     �C     �C     �C     �C     �C     �C     �C     D     D     +D     4D     JD     VD     vD     �D     �D  	   �D     �D     �D     �D     �D     �D      E     E     ,E     1E     CE  '   bE     �E     �E     �E     �E     �E     �E     �E  *   �E  �   �E     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F  
   �F     �F     G  *   G  r   HG     �G     �G     �G     �G      H     H     (H  )   <H     fH     tH  !   �H  7   �H     �H     �H     I     I     I     %I  '   AI     iI  �   uI     4J     ;J     CJ     KJ     SJ     hJ     ~J     �J     �J     �J     �J     �J     �J     �J  
   �J  H   �J  $   ,K     QK     ZK     uK     |K     �K     �K     �K     �K     �K     �K     �K     �K  +   �K  )   L     CL     _L     gL  
   vL     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     M     M     M     3M     ?M     OM     aM     fM     rM     vM     ~M     �M  	   �M     �M  	   �M     �M     �M     �M  	   �M     �M     N     N     N     0N     7N     CN  
   QN     \N     aN     iN     pN     N  	   �N     �N     �N     �N     �N     �N     �N     �N     O  "   'O     JO     XO  	   eO     oO  
   {O     �O     �O     �O     �O     �O     �O     �O  	   �O  �   �O  (   JP  1   sP     �P  %   �P     �P     Q     Q     =Q      XQ     yQ  $   �Q     �Q     �Q     �Q     �Q     �Q     �Q  	   R     R  	   "R     ,R     =R     LR     UR     \R     pR  	   ~R     �R     �R     �R     �R  
   �R     �R  ,   �R  $   �R     $S     @S     XS     \S     bS  	   rS     |S     �S     �S     �S     �S  
   �S     �S     �S     �S     T     T     T     .T     :T     AT     MT     ZT     oT  
   tT     T     �T     �T  
   �T     �T     �T     �T  	   �T     �T  
   U  
   U  	   U     (U     >U     JU     VU  
   bU     mU     |U     �U     �U     �U     �U  	   �U     �U  
   �U     �U  0   �U  Z   )V  J   �V  >   �V  S   W  M   bW  =   �W     �W  g   X  <   uX     �X     �X     �X      �X     Y     Y     Y     (Y  	   .Y     8Y     EY     YY     lY     Y  
   �Y     �Y  	   �Y     �Y     �Y     �Y     �Y  
   �Y     �Y  >   Z  
   TZ     _Z     cZ     qZ     xZ     �Z  )   �Z  M   �Z  8   [  H   >[  4   �[  5   �[  \   �[  _   O\  %   �\  '   �\  W   �\  �   U]  p   �]  #   T^  	   x^  D   �^     �^     �^     �^     �^     �^  	   �^     �^     _     _     _     0_     G_  %   d_     �_     �_     �_  &   �_  
   �_  
   �_      `     `     ?`     D`  	   R`     \`     e`     n`     ~`     �`     �`     �`     �`     �`     �`     �`     �`     a     a  �   a  K   �a  D   b  V   Tb  #   �b  3   �b  c   c  (   gc     �c     �c     �c     �c     �c  $   �c     �c     �c     �c     �c     �c  A   d     Gd     ]d     ld     �d     �d     �d     �d     �d     �d  	   �d     �d     �d     e     e     e     #e  T  <e  \  �g  "   �h     i     +i  7   -i     ei     ~i     �i     �i  �   �i  z   j  8   �j     �j     �j     �j     �j     �j     �j     �j     �j  	   k     k     k     -k     Lk     Xk  (   fk     �k     �k     �k     �k     �k     �k  
   �k  	   �k     �k     �k     �k     l     %l     4l  �   :l  �   �l  �   nm     :n     Jn     bn     nn  	   un     n  T   �n     �n     �n     �n     
o     o     2o     Io  L   ]o  �   �o  �  /p     �q     �q     �q     �q     �q  
   �q     �q     �q     �q     r     r  	   r  >   "r  E   ar     �r  
   �r     �r     �r     �r     �r  (   s     -s     ?s     Xs     as     ys     �s     �s     �s  $   �s     t     t     t     .t     7t     Dt     Qt     Zt     bt     t     �t     �t     �t     �t     �t     �t     �t     �t  #   u     )u     @u     Wu     `u     pu     yu     �u     �u     �u     �u     �u     �u     �u     �u     �u     �u     v     1v     Ev     [v     wv     �v  �   �v     Cw     ]w     sw     �w     �w     �w     �w     �w     x     x     x     'x     ?x     Nx     ex  &   xx  	   �x     �x     �x     �x     �x     �x     �x     �x     y     y     %y     -y     ;y     Ky     Ty     \y  	   by     ly     �y     �y     �y     �y     �y     �y     �y     �y  )   z  '   ,z  "   Tz     wz     ~z     �z  G   �z     �z     �z  !   {     1{     C{     J{  (   Z{     �{     �{     �{     �{     �{     |     	|     |  	   $|     .|     @|     F|     W|     j|     s|     �|     �|     �|  	   �|     �|     �|  #   �|     }     5}  
   B}     M}     Y}     `}     s}     �}     �}     �}     �}     �}     �}     �}  )   ~     :~     O~     ^~     f~     m~     r~     y~      �~  �   �~  	   @  
   J     U     ]     k     {     �     �     �     �     �     �     �  +   �  p   �     l�     r�     ��     ��     ��     ɀ     ܀  3   �     %�     4�  !   O�  :   q�     ��     ��     ԁ     ��     �     �  +   �     ?�  �   H�     �     &�     -�     :�     A�     S�     d�     k�     ��     ��     ��     ��     ��     ��     Ń  M   у  (   �     H�     T�  
   j�  
   u�     ��     ��     ��     ��     ��     ��     ń     ߄  %   ��  (   �     E�     [�     c�  
   s�     ~�     ��     ��     ��     ƅ  
   ͅ  	   ؅     �      �     �     �     $�  $   :�     _�     p�     �     ��     ��     ��     ��     ��     ǆ     ن     �     ��  	   �     �  	   /�     9�  !   E�     g�     ~�     ��     ��     ��  
   ��     ��     ��     ȇ  	   ч     ۇ     �     ��     �  	   �  	    �     *�     @�     _�  #   n�     ��  "   ��     ͈     ڈ     �     �     ��     �     �     �     "�     *�     /�     6�  
   <�  �   G�  /   ։  @   �     G�  ,   f�      ��     ��     ϊ     �  *   ��  #   %�     I�     i�     o�  "   x�  
   ��  	   ��     ��  
   ��     ɋ     ܋     �     ��     �     �     �     ,�  
   ;�     F�     Y�  	   r�     |�     ��     ��  4   ��  ,   ˌ      ��     �     -�     4�     :�     J�     P�     d�     s�     ��     ��     ��     ��     ��     ō     ލ     �     �     �     *�     1�     A�     S�     s�     z�     ��     ��     ��  	   ��     ��     ��     ގ     ��     �     �  
   )�     4�     :�  	   J�  	   T�  	   ^�     h�     p�     |�  !   ��     ��  &   Ï     �     �     �     �  	   *�  1   4�  d   f�  O   ː  <   �  L   X�  I   ��  4   �  $   $�  U   I�  <   ��     ܒ     ��     �  $   �     C�     Q�     Y�     k�     p�     y�     ��     ��     ��     ��  	   ԓ     ޓ  	   �  	   �     ��     	�     �     !�     4�  P   P�     ��     ��     ��  	   Ô     ͔     ֔  &   �  L   �  @   [�  O   ��  4   �  >   !�  B   `�  D   ��     �     �  W   �  �   w�  v   �      ~�     ��  \   ��     
�     �     �     #�     '�  
   8�     C�     K�     S�     q�     ��     ��      ��     �     ��     �  )   -�     W�     f�     t�  &   ��  	   ��     Ś     ݚ  	   �     ��     	�     �     �  	   #�     -�     B�     V�  $   l�     ��     ��     ��     ��  �   ��  I   d�  A   ��  c   �  &   T�  I   {�  s   ŝ  8   9�     r�     v�     }�     ��     ��  (   ��     ��     ��  #   ʞ     �     ��  D   ��     C�     Y�     j�     ��     ��     ��     ��     ȟ     ϟ  	   ן     �     �     ��     �     #�     )�         �   9   7      s      �        =  0          8  G  �  �     X  .  (      �   0      �  <      #      *       �   �      J  �   �      e      �   
       *    �  Q      �       �   �   B             r        N  �         �   V  D         �               `  �  i  ^       �  �      C      �       �                 �  z    I      m       �  �   �                   @       �       .                 U  �      m          �   �          y  :       ]   �  �   n   �  ;  �   h   z      �   8   e       6  �  L  �        �   l  -   	  �  �   �   �       B     @  C  K   '  �  �     �  �   \         %   �   n  �   �    �   M  p   �   k  5      �             j         �  �   �  �  �    J   �   �         |  7  $       v      9  &          I  �      [  �   �  3  �   �      	  �        $  /   ~   �   �   W              �   #       �      U   �    �  %  ,  y   �       �      �   �    �  �   L   c       F  �              �      �  �   G      $  E  �  B  �  �           �  �     <  �  �   `   �   �   �   �           �  �        �       l   �   H    E  �  �   f          �       �   '   �   P    _    �  ]      P   5  a   @      5       2           H  �  S  �  �   �  D  0   !  X   u  �   �    �     �          �  =  �   k   �      V   _   �  #   �      ^    �      �  g  �   q   �   !   �      O  =   �  4   �      �  �       �    ~  �           �   K  �  �                    �                                +  b  �   �       '  >      �   �   1  o         �      3   �  p  +   -  �   {  6  }          h  �  �      "  ,  i       o   Y       .             �   K  )     	   �              �      t     (      r     �  �      �       �       �   N   N  "   E   F   G   H   I       ;   �       *    �   �   4                        �   �       d           �  �             b   �  �          �     �   >       �   �        �   C   �      �          �      M   �   �  �         x      q  �  A  �      :  �   �   �      �  ,   f       6   �   )   �   �   c    T  �  |       J  T   }       �        �   �      �  Z  �  �          3  �  �   �  �      �  �  �  S       
      �  �   j  �   �  �   1                 �       �   ;  Q       �   �  �      [       L  "  �  �     �  &   �   �   �              u       �      �      �   {   �   �  2  �          ?       >  W      s   t   7   v   w   x   �  �      +      R       �  �  �    D              M      ?  �    �  �   �   &  F      �      �  2  %  �  O   w      �  �  �   �       �  �  /  �             �   1   
      A      (  �  /  �   -      Y      �  �  �   �   g   �  �   �           4  A   �  �     �   Z   �   \           �   �         �   <   �      �                 !  :        �       8  �   �     9                         �           d  a  ?      R  )        �       
Bibus can directly insert citations in OpenOffice.org
if you start OpenOffice.org in 'listening mode'.


Do you want to activate this mode by default?


  1. A document will open in OpenOffice.org,
  2. Click on 'Accept UNO connections',
  3. Quit OpenOffice (and the Quickstarter if used),
  4. Restart OpenOffice.org.  Paste your text below:   and order them by % %(total_number)s reference(s) : %(number_selected)s selected %s connection parameters ,  , et al. - - If you want to use Bibus on a single computer (mono-user), 
            SQLite is presumably the best choice. - If you want to use Bibus on multiple computers (multi-user),
            MySQL is presumably the best choice. - You can change this choice at anytime in the preferences. -- 3306 :  ;  ARTICLE Abbreviate author list with Abstract Acrobat file Activate Add Add Text Add a child to the selected key Add a field Add a line Add a reference associated with the selected key Add child Address Advanced preferences After All All files All references Annote Anonymous Anonymous ... Anonymous author format As in database Ascending Author Author "%(author)s" contains %(nb_comma)s comma.
The correct format for authors is:

"Name, FirstName MiddleName"
or
"Name, FM"

Would you like to re-edit the reference? Author "%(author)s" contains a period.
The correct format for authors is:

"Name, FirstName MiddleName"
or
"Name, FM"

Would you like to re-edit the reference? Author "%(author)s" is very long.
The correct format for authors is:

"Name1, FirstName1 MiddleName1; Name2, FirstName2 MiddleName2"
or
"Name1, FM; Name2, FM"

Would you like to re-edit the reference? Author list Author list format Author-Date BOOK BOOKLET Base Style Because of an error, I didn't format reference type %(typeName)s and field %(fieldName)s Before last BibTeX Bibliographic index BibliographicType Bibliography title Bibus MySQL Setup Bibus WEB site Bibus can directly insert citations
in you favorite word processor Bibus needs a USERNAME to identify yourself.
Please enter a name in the box below.
Your login name might be a good idea Bibus uses a file to store the bibliographic database.

Please enter in the box below the path to 
the bibliographic database you want to use.

- If the file EXIST, Bibus will 
	- not modify it
	- assume it to be a correct Bibus database.

- If the file DOES NOT EXIST, Bibus will 
	- create it
	- use it as your bibliographic database. Black Blue Bold Booktitle Brackets CONFERENCE CUSTOM1 CUSTOM2 CUSTOM3 CUSTOM4 CUSTOM5 Cancel Cannot connect to Openoffice. See Documentation. Cannot connect to Openoffice. See Documentation.
%s
%s) Caps Capture ... Capture Citations Capture from database Capture from field Capture selection Capture the references absent from the database Case Change author list Chapter Check database Check for duplicates Choose a File to import Choose the destination file Choose the file location and name Choose the name of the SQLite database Citation Cited Clean database ... Clear Codage Codecs Common Connect Connect to database Connect... Connection Connection type Content Copy Count Create Index on Insert Create User Create a new category Create the Bibliography if absent Create user... Creating styles ... Creator Current key Custom1 Custom2 Custom3 Custom4 Custom5 Cut Cyan DB engine Database Database = %s Database = None Database Creation Database Error Database Name Database Type Database check Database choice Database cleanup Database cleanup may corrupt your database!
You should make a backup of your database before using this command. Database conversion Database engine Database error Database file ... Database file... Database type Database used at startup Database... Date Delete Delete journal Delete query Delete reference Delete the selected reference Delete this key Deleting old index and citations ... Descending Details Directory ... Display Display only keys like: Displayed fields Divers Document position Documentation Done Down Download styles Duplicate reference Duplicates EMAIL Edit Edit ... Edit Identifier Edit journal Edit query Edit reference Edit shortcuts ... Edit the selected reference Edition Editor Empty Trash Empty Trash and DB cleanup Enter your connection parameters below Enter your email below (Optional): Error Error during query Error in authors? Error in the number of parentheses: %(open)s ( and %(close)s ) Expert Expert Search Experts search is not available yet! Experts' papers Export Export a file Export formatted references in an HTML file Export to BibTeX format Export to Medline format Export to RIS format Export to Refer format for EndNote Export to a SQLite database Field Field Value Field color Field name Field name color Fields Fields formatting Fields ordering File File ... File error operation File name ... Fill character in tab stops Finalize Finalize the document Finalize... Finalizing the current document First Connection Wizard First Editor First author First key Format Format Author as Format Bibliography Format Editor as Format fields as Format reference as Formating citations ... Full Full journal name Fuse: [1] [3] [2] -> [1; 3; 2] Fuse: [Martin] [John] -> [Martin; John] Fusing citations ... GoTo Writer Grants Green HTML Help Hilight Citations Hilight citations with a yellow background Hitting "Detele" will remove all the references from the Trash key.
Some of the references may not be deleted if they are in use by another user. Host Howpublished INBOOK INCOLLECTION INPROCEEDINGS ISBN ISI ISO ISO journal abbreviation Id Identifier If a duplicate is found If at least If author Field is empty, replace it with: If you have the necessary rights (MySQL Administrator),
click the button below to create/change the MySQL database Import Import && Quit Import a BibTeX file Import a Medline XML file Import a Medline file Import a RIS file Import a Refer file Import a Refer file exported from EndNote Import a file Import a text file Import an ISI Web of science file Import an XML library generated with Endnote 9 or Later Import buffer Incorrect position Information Insert Insert Citation Inserting citations in text Inserting citations in text (%s ranges) Institution It seems that your database is not compatible with your PySQLite version.
If it does not work,
see the SQLite site about database format change in SQLite3 http://www.sqlite.org/version3.html Italic JOURNAL Joining Journal Journal Abbreviation Journal abbreviations Journals Keep the best citations:  Key type Keys Language Last Editor Last author License Light_grey List all authors on first occurence if authors number is not higher than List more authors until it is unique Load ... Looking for duplicates ... MANUAL MASTERTHESIS MISC MS Word MSWord Main Main Fields Main Fields = %s Main fields (+ Abstract) Main fields searched Make a copy of the first selected reference Making a copy of the current document ... Mark the selected reference Medline Medline Search MedlineXML Microsoft Word Microsoft Word Document Middle Editors Middle authors Mode Modif Modified by Modify User Grants Modify user... Month MySQL MySQL database MySQL database setup ... MySQL setup MySQL v3 and v4 MySQL v4.1 and v5 Name Name format New New ... New Database... New Name format New Query New journal New query New reference New shortcut menu NewKey No Choice No Database selected Non allowed operation None Norma Jean Baker Normal Normal (rw) Not connected Not sorted Note Nothing Number Number entries Number of records Numbering OK OOo_pipe Open URL OpenDocument Text OpenOffice connection settings OpenOffice.org OpenOffice.org Text Document OpenOffice.org connection OpenOffice.org connection settings Organizations Other Fields PHDTHESIS PROCEEDINGS Page Setup Pages PassWord Password Paste Paths Perso Pipe Pipe name Please choose below the Word Processor you want to use with Bibus.
You can change this setting at any time in Bibus menu Edit/Preferences. Please choose the bibliographic database Please choose the database engine you want to use Please choose the file encoding Please enter password for database %s Please select a style file Please select the file Please, enter the new menu name Please, enter the new name Please, enter the new query name Please, enter the query name Please, select the default directory Port Position Position of the tab stop (mm) Preferences Preview Preview ... Preview : Preview printing Print ... Print references Printed fields Printing PubMed PubMed journal name PubMed search Publisher Pubmed search ... Put a letter after the year Queries Quit Quit Bibus RIS Read-only with private and shared trees (ro) Read-only with shared tree only (rk) Read-only without tree (rr) Records with Z-Score >= Red Refer Refer (EndNote) Reference Reference Editor Reference display Reference fields Reference list Reference type References References formatting Remove Rename Query Rename folder Rename query Rename this key Report_Type SQLite SQLite file SQLite setup SQlite database file Save Save as... School Search Search && Close Search ... Search PubMed Search Pubmed on the Web Search Pubmed with eTBlast Search in Search the database Second key Select All Selection Separate authors with Separator 1 Separator 2 Separator 3 Separators Separators ... Series Set printer page format Set some preferences Setting database and rights Shared Shortcuts Show All Small Caps Socket Solving duplicates ... (%s series of duplicates) Some needed fields are absent from the selected database/tables.
Continue at your own risk Some references were not formatted. Are you sure they are in the database? Sorry but codec '%s' is not available.
I will try to use ascii Sorry but the MySQL python module (MySQLdb) is not available.
Read installation.txt Sorry but the SQLite python extension is not available.
Read installation.txt Sorry but the key you are moving already exist in this folder Sorry, I can't fix this error. Sorry, but I was not able to find the python module for the %s database.
Please check your installation Sorry, you cannot move a reference associated with this key. Sort Ascending Sort Descending Sort bibliography by Sort: [1] [3] [2] -> [1] [2] [3] Sorting Standard Start at record Style Style ... Style author Style creation date Style format Error Style informations Style modification date Style name Styles Subscript Superscript Supplementary Fields Supplementary fields TCP/IP TECHREPORT Tab stop is right aligned Table "%(name)s" is absent from the database.
Should I fix it? Tabulation Tag Tag reference Tagged Template Text Window The PubMed journal name must be non empty The database contains references with NULL Identifiers
Should I delete them ? The database contains wrong links
Should I delete them ? The database contains wrong links in table_modif.
Should I delete them ? The front document in OOo must be a Writer document. The key must be unique.
Please choose a new key name. The reference or the identifier was not unique. The reference identifier has been changed %s The reference or the identifier was not unique. The reference identifier has been changed to %s The search did not return any result. The selected style is not a bibus style The style name is not correct, please avoid: '/' under linux; '\' and ':' under Windows The version of the style file is too old.
 I will convert it to the new Bibus style format.
To get rid of this message, please save it again. The version of the style file is too recent. Please update bibus to a new version.
 I will open a default style. There is %s such records in Medline Third key Time Out (%s min). No answer received. eTBLAST server might be down. Title Trash UNPUBLISHED URL URL/File... Underline Unknown Up Update Bibliographic Index Update Citations Update Index on Insert Update and Pre-format all... Update the Bibliography automatically Updating index ... Updating references ... Use Database: Use OpenOffice.org format for printing Use TCP/IP Use a pipe Use current language for month Use range: [1] [3] [2] -> [1-3] User User Creation User Name UserName Username Using separator Volume WWW Warning Welcome to Bibus ! Welcome to bibus What do you want to export? Where to save the style file? White Word Processor XML From Endnote Year You can set here the environment variable $FILES.
This variable is substituted when an URL is opened.
This allows you to set a central repository
for all the articles.
 You cannot edit default styles
.I have made a copy of the style for editing You cannot use '%' and '_' characters.
Please choose a new key name. You database has been converted to the new format.
The database is now located at
"%s" You did not select a valid database You must first edit Pref/Shortcuts to use this menu You must first save the current document before using this function.
 Should I save it and proceed? Your database is a valid bibus database. ]-[ a Field a String ascii authors authors, replace authors 2 to n with below biblioDB case sensitive cp1252 default eTBlast main page: http://invention.swmed.edu/etblast/index.shtml eTBlast on PubMed ... eTBlast result eTBlast search on PubMed in 'ARTICLE' in database keep the new reference keep the old reference last latin-1 localhost root save password (Not secure!) shortcut using separator utf-8 when there are more than Project-Id-Version: Bibus 1.4.2
POT-Creation-Date: 2008-03-26 17:39+0100
PO-Revision-Date: 2008-03-26 17:39+0100
Last-Translator: Pierre Martineau <pmartino@users.sourceforge.net>
Language-Team: Martin Srebotnjak <miles@filmsi.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Slovenian
X-Poedit-Country: SLOVENIA
X-Poedit-SourceCharset: iso-8859-1
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Poedit-Basepath: /home/pmartino/Desktop/Bibus/bibus-cvs/
X-Poedit-SearchPath-0: bibus
 
Bibus lahko neposredno vstavi citate v OpenOffice.org,
če zaženete OpenOffice.org v 'načinu poslušanja'.


Želite ta način aktivirati kot privzetega?


  1. Dokument se odpre v OpenOffice.org;
  2. Kliknite 'Sprejmi povezave UNO';
  3. Zaprite OpenOffice.org (in Hitri zaganjalnik, če ga uporabljate);
  4. Ponovno zaženite OpenOffice.org.  Spodaj prilepite svoje besedilo:   in jih razvrsti glede na % %(total_number)s sklicev : %(number_selected)s izbranih %s parametri povezovanja , , et al. - - Če želite uporabljati Bibus na posameznem računalniku (posamični uporabnik), 
            je najbrž najboljša izbira SQLite. - Če želite uporabljati Bibus na več računalnikih (več uporabniško),
            je MySQL najbrž najboljša izbira. - To možnost lahko kadar koli spremenite v nastavitvah. -- 3306 : ; ČLANEK Okrajšaj seznam avtorjev z Povzetek Datoteka Acrobat Aktiviraj Dodaj Dodaj besedilo Dodaj otroka izbranemu ključu Dodaj polje Dodaj vrstico Dodaj sklic, povezan z izbranim ključem Dodaj otroka Naslov Napredne nastavitve Po Vse Vse datoteke Vsi sklici Zabeleži Anonimno Anonimno ... Zapis anonimnega avtorja Kot v zbirki podatkov naraščujoče Avtor Avtor "%(author)s" vsebuje %(nb_comma)s vejic.
Pravilen zapis za avtorje je:

"Priimek, Ime SrednjeIme"
ali
"Priimek, InicialkaImena"

Želite preurediti sklic? Avtor "%(author)s" vsebuje piko.
Pravilen zapis za avtorje je:

"Priimek, Ime SrednjeIme"
ali
"Priimek, InicialkaImena"

Želite preurediti sklic? Avtor "%(author)s" je predolg.
Pravilen zapis za avtorje je:

"Priimek, Ime1 SrednjeIme1; Priimek2, Ime2 SrednjeIme2"
ali
"Priimek1, InicialkaImena1; Priimek2, InicialkaImena2"

Želite preurediti sklic? Seznam avtorjev Oblika seznama avtorjev Avtor-datum KNJIGA KNJIŽICA Osnovni slog Zaradi napake vrsta sklica %(typeName)s in polje %(fieldName)s nista bila oblikovana Pred zadnjim BibTeX Bibliografsko kazalo VrstaBibliografije Naslov bibliografije Namestitev Bibus MySQL Spletna stran Bibus Bibus lahko neposredno vstavi citate
v vaš priljubljeni urejevalnik besedil Bibus potrebuje vaše UPORABNIŠKO IME za identifikacijo.
Vnesite ime v spodnje polje.
Vaše sistemsko prijavno ime je dobra izbira. Bibus uporablja za shranjevanje bibliografske zbirke podatkov datoteko.

Prosimo, vnesite v spodnje polje pot do 
bibliografske zbirke podatkov, ki jo želite uporabljati.

- Če datoteka OBSTAJA, Bibus 
	- le-te ne bo spremenil
	- predvideva, da je neoporečna zbirka podatkov Bibus.

- Če datoteka NE OBSTAJA, jo bo Bibus 
	- ustvaril
	- uporabil kot vašo bibliografsko zbirko podatkov. Črna Modra Krepko Naslov knjige Oklepaji KONFERENCA PO_MERI1 PO_MERI2 PO_MERI3 PO_MERI4 PO_MERI5 Prekliči Povezava z OpenOffice.org ni možna. Oglejte si dokumentacijo. Povezava z OpenOffice.org ni možna. Oglejte si dokumentacijo.
%s
%s) Velike črke Zajemi ... Zajemi citate Zajemi iz zbirke podatkov Zajemi iz polja Zajemi izbor Zajemi sklice, ki niso v zbirki podatkov Velike/male črke Spremeni seznam avtorjev Poglavje Preveri zbirko podatkov Preveri za dvojnike Izberite datoteko za uvoz Izberi ciljno datoteko Izberite mesto datoteke in ime Izberite ime zbirke podatkov  SQLite Citat Citirano Počisti zbirko podatkov ... Počisti Kodna tabela Kodne tabele splošne Poveži Poveži se z zbirko podatkov Poveži ... Povezava Vrsta povezave Vsebina Kopiraj Preštej Ustvari kazalo ob vstavljanju Ustvari uporabnika Ustvari novo kategorijo Ustvari bibliografijo ob odsotnosti Ustvari uporabnika ... Ustvarjanje slogov ... Ustvaril Trenutni ključ Po_meri1 Po_meri2 Po_meri3 Po_meri4 Po_meri5 Izreži Cian Pogon zbirke podatkov Zbirka podatkov Zbirka podatkov = %s Zbirka podatkov = nobena Ustvarjanje zbirke podatkov Napaka zbirke podatkov Ime zbirke podatkov Vrsta zbirke podatkov Preverjanje zbirke podatkov Izbor zbirke podatkov Čiščenje zbirke podatkov Čiščenje zbirke podatkov lahko okvari vašo zbirko podatkov!
Pred uporabo tega ukaza priporočamo, da naredite varnostno kopijo svoje zbirke podatkov. Pretvorba zbirke podatkov Pogon zbirke podatkov Napaka zbirke podatkov Datoteka zbirke podatkov ... Datoteka zbirke podatkov ... Vrsta zbirke podatkov Zbirka podatkov ob zagonu Zbirka podatkov ... Datum Izbriši Izbriši revijo Izbriši povpraševanje Izbriši sklic Izbriši izbrani sklic Izbriši ta ključ Brisanje starega kazala in citatov ... padajoče Podrobnosti Mapa ... Prikaz Pokaži samo naslednje ključe: Prikazana polja Razno Položaj dokumenta Dokumentacija Končano Navzdol Prenesi sloge Podvojeni sklic Dvojniki ENASLOV Uredi Uredi ... Uredi identifikacijo Uredi revijo Uredi povpraševanje Uredi sklic Uredi tipke za bližnjice ... Uredi izbrani sklic Izdaja Urejevalnik Izprazni koš Izprazni koš in počisti zbirko podatkov Spodaj vnesite parametre svoje povezave Vnesite svoj e-naslov (neobvezno): Napaka Napaka pri povpraševanju Napaka pri avtorjih? Napaka v številu oklepajev: %(open)s oklepajev in %(close)s zaklepajev napredni Napredno iskanje Napredno iskanje še ni na voljo! Strokovni članki Izvozi Izvozi datoteko Izvozi oblikovane sklice v datoteko HTML Izvozi v zapis BibTeX Izvozi v zapis Medline Izvozi v zapis RIS Izvozi v zapis Refer za EndNote Izvozi v zbirko podatkov SQLite Polje Vrednost polja Barva polja Ime polja Barva imena polja Polja Oblikovanje polj Razvrščanje polj Datoteka Datoteka ... Napaka pri delu z datoteko Ime datoteke ... Zapolni z znaki do tabulatorja Dokončaj Dokončaj dokument Dokončaj ... Zaključevanje trenutnega dokumenta Čarovnik za prvo povezavo Prvi urednik Prvi avtor Prvi ključ Oblika Obliku avtorja kot Oblikuj bibliografijo Oblikuj urednika kot Oblikuj polja kot Oblikuj sklic kot Oblikovanje citatov ... Polno Polno ime revije Zlij: [1] [3] [2] -> [1; 3; 2] Zlij: [Martin] [Janez] -> [Martin; Janez] Zlivanje citatov ... Pojdi v Writer Pravice Zelena HTML Pomoč Poudari citate Poudari citate z rumenim ozadjem Če pritisnete "Izbriši", boste odstranili vse sklice iz ključa Koš.
Nekateri sklici morda ne bodo izbrisani, če jih uporablja nek drug uporabnik. Gostitelj Kakoizdano VKNJIGI VZBRANIHDELIH VZAPISIHRAZPRAV ISBN ISI ISO Okrajšava revije v ISO Id Identifikacija Če obstaja dvojnik Če vsaj Če je polje avtorja prazno, ga zamenjaj z: Če imate potrebne pravice (upravitelj MySQL),
kliknite spodnji gumb za izdelavo/spremembe zbirke podatkov MySQL Uvozi Uvozi && zapri Uvozi datoteko BibTeX Uvozi datoteko Medline XML Uvozi datoteko Medline Uvozi datoteko RIS Uvozi datoteko Refer Uvozi datoteko Refer, izvoženo iz programa EndNote Uvozi datoteko Uvozi datoteko z besedilom Uvozi datoteko ISI Web of science Uvozi knjižnico XML, ustvarjeno z Endnote 9 ali novejšim Uvozni medpomnilnik Nepravilen položaj Informacije Vstavi Vstavi citat Vstavljanje citatov v besedilo Vstavljanje citatov v besedilo (%s obsegov) Ustanova Kot kaže, vaša zbirka podatkov ni kompatibilna z nameščeno različico PySQLite.
Če ne deluje,
si oglejte spletno mesto SQLite o spremembah zapisa zbirke podatkov v SQLite3 http://www.sqlite.org/version3.html Ležeče REVIJA Združevanje Revija Okrajšava revije Okrajšave revij Revije Obdrži najboljše citate:  Vrsta ključa Tipke Jezik Zadnji urednik Zadnji avtor Licenca Svetlo_siva Naštej vse avtorje ob prvi pojavitvi, če je število avtorjev ni večje kot Navedi več avtorjev, dokler ni enkratno Naloži ... Iskanje dvojnikov ... PRIROČNIK MAGISTERIJ RAZNO MS Word MSWord Glavne možnosti Glavna polja Glavna polja = %s Glavna polja (+ Povzetek) Preiskana glavna polja Naredi kopijo prvega izbranega sklica Izdelava kopije trenutnega dokumenta ... Označi izbrani sklic Medline Iskanje Medline MedlineXML Microsoft Word Dokument Microsoft Word Srednji uredniki Srednji avtorji Način Dod. tipke Spremenil Spremeni uporabniške pravice Spremeni uporabnika ... Mesec MySQL Zbirka podatkov MySQL Namestitev zbirke podatkov MySQL ... Nastavitev MySQL MySQL v3 in v4 MySQL v4.1 in v5 Ime Oblika imena Nov Novo ... Nova zbirka podatkov ... Nova oblika imena Novo povpraševanje Nova revija Novo povpraševanje Nov sklic Nov meni z bližnjicami NovKljuč Brez izbire Nobena zbirka podatkov ni izbrana Operacija ni dovoljena Brez Janez Kranjski navadni Navadno (rw) Nepovezano Nerazvrščeno Opomba ničemer Številka Oštevilči vnose Število zapisov Oštevilčevanje V redu OOo_dovod Odpri URL Besedilo OpenDocument Nastavitve povezave OpenOffice OpenOffice.org Dokument z besedilom OpenOffice.org Povezava OpenOffice.org Nastavitve povezave OpenOffice.org Organizacije Druga polja DOKTORAT ZAPISIRAZPRAV Nastavitev strani Strani Geslo Geslo Prilepi Poti osebne Dovod Ime dovoda Spodaj izberite urejevalnik besedil, ki ga želite uporabiti z Bibusom.
To nastavitev lahko vedno spremenite v meniju Bibusa Uredi/Nastavitve. Prosimo, izberite bibliografsko zbirko podatkov Prosimo, izberite vrsto zbirke podatkov, ki jo želite uporabiti Izberite kodno tabelo datoteke Prosimo, vnesite geslo za zbirko podatkov %s Prosimo, izberite datoteko sloga Prosimo, izberite datoteko Vnesite ime novega menija Vnesite novo ime Prosimo, vnesite ime novega povpraševanja Prosimo, vnesite ime povpraševanja Prosimo, izberite privzeto mapo Vrata Položaj Položaj tabulatorskega mesta (mm) Nastavitve Predogled Predogled ... Predogled: Predogled tiskanja Natisni ... Natisni sklice Natisnjena polja Tiskanje PubMed Ime revije v PubMed Iskanje Pubmed Izdajatelj Iskanje Pubmed ... Postavi črko za letnico Poizvedbe Izhod Izhod iz Bibusa RIS Samo za branje z zasebnimi in deljenimi drevesi (ro) Samo za branje samo z deljenim drevesom (rk) Samo za branje brez drevesa (rr) Zapisi z Z-Score >= Rdeča Refer Refer (EndNote) Sklic Urejevalnik sklicev Prikaz sklicev Referenčna polja Seznam sklicev Vrsta sklica Sklici Oblikovanje sklicev Odstrani Preimenuj povpraševanje Preimenuj mapo Preimenuj povpraševanje Preimenuj ta ključ Vrsta_poročila SQLite Datoteka SQLite Nastavitev SQLite Datoteka zbirke podatkov SQlite Shrani Shrani kot ... Šola Išči Išči && zapri Najdi ... Preišči PubMed Preišči Pubmed prek spleta Preišči Pubmed z eTBlast Išči v Preišči zbirko podatkov Drugi ključ Izberi vse Izbor Loči avtorje z Ločilo 1 Ločilo 2 Ločilo 3 Ločila Ločila ... Zbirka Nastavi obliko strani za tiskanje Izberite določene nastavitve Nastavljanje zbirke podatkov in pravic Skupno Tipke za bližnjice Pokaži vse Pomanjšane velike črke Vtičnica Razreševanje dvojnikov ... (%s skupin dvojnikov) Nekatera obvezna polja manjkajo v izbrani zbirki podatkov/tabelah.
Nadaljujete na lastno odgovornost Nekateri sklici niso bili oblikovani. Ste prepričani, da so v zbirki podatkov? Žal kodiranje znakov '%s' ni na voljo.
Uporabljen bo ASCII. Žal modul python MySQL (MySQLdb) ni na voljo.
Preberite si installation.txt Žal razširitev python SQLite ni na voljo.
Preberite si installation.txt Žal ključ, ki ga premikate, v tej mapi že obstaja Žal te napake ni mogoče popraviti. Žal ni mogoče najti modula python za zbirko podatkov %s.
Preverite svojo namestitev Žal ne morete premakniti sklica, povezanega s tem ključem. Razvrsti naraščajoče Razvrsti padajoče Razvrsti bibliografijo Razvrsti: [1] [3] [2] -> [1] [2] [3] Razvrščanje Navadno Začni pri zapisu Slog Slog ... Avtor sloga Datum nastanka sloga Napaka oblike sloga Podatki o slogu Datum spremembe sloga Ime sloga Slogi Podpisano Nadpisano Dopolnilna polja Dopolnilna polja TCP/IP TEHNIČNOPOROČILO Tabulator je poravnan desno Tabela "%(name)s" v zbirki podatkov manjka.
Želite to pomanjkljivost odpraviti? Tabulatorji Označi Označi sklic Označeno Predloga Okno z besedilom Ime revije v PubMed ne sme biti prazno Zbirka podatkov vsebuje sklice z identifikatorji NULL
Jih želite izbrisati? Zbirka podatkov vsebuje napačne povezave
Jih želite izbrisati? Zbirka podatkov vsebuje napačne povezave v table_modif.
Jih želite izbrisati? Prednji dokument v OOo mora biti Writerjev dokument. Ključ mora biti enkraten.
Prosimo, izberite novo ime ključa. Sklic ali identifikator ni bil enkraten, zato je bil spremenjen %s Sklic ali identifikator ni bil enkraten, zato je bil spremenjen v %s Iskanje ni vrnilo zadetkov. Izbrani slog ni slog bibus Ime sloga ni pravilno, v okolju Linux se izogibajte '/', v okolju Windows pa '\' in ':' Različica slogovne datoteke je prestara.
Pretvorjena bo v nov slogovni zapis Bibusa.
Če bi se radi znebili tega opozorila, jo znova shranite. Različica slogovne datoteke je presodobna. Prosimo, posodobite bibus na novejšo različico.
 Odprt bo privzeti slog. V Medline je %s takšnih zapisov Tretji ključ Časovna prekoračitev (%s min). Brez prejetega odgovora. Strežnik eTBLAST morda ne deluje. Naslov Koš NEOBJAVLJENO URL URL/datoteka ... Podčrtano Neznano Navzgor Posodobi bibliografsko kazalo Posodobi citate Posodobi kazalo ob vstavljanju Posodobi in predoblikuj vse ... Samodejno posodobi bibliografijo Posodabljanje kazala ... Posodabljanje sklicev ... Uporabi zbirko podatkov: Uporabi obliko OpenOffice.org za tiskanje Uporabi TCP/IP Uporabi dovod Uporabi trenuten jezik za mesec Uporabi območje: [1] [3] [2] -> [1-3] Uporabnik Ustvarjanje uporabnikov Uporabniško ime Uporabnik Uporabniško ime Uporaba ločila Del SPLET Opozorilo Dobrodošli v Bibus! Dobrodošli v bibus Kaj želite izvoziti? Kam želite shraniti datoteko sloga? Bela Urejevalnik besedil XML iz Endnote Leto Tukaj lahko nastavite okoljsko spremenljivko $FILES.
Ta spremenljivka se zamenja ob odprtju povezave URL.
S tem lahko nastavite osrednje skladišče
za vse članke.
 Privzetih slogov ne morete urejati.
Izdelana je kopija sloga za urejanje. Znakov '%' in '_' ne smete uporabljati.
Vnesite novo ime ključa. Vaša zbirka podatkov je bila pretvorjena v novo obliko.
Zbirka podatkov se zdaj nahaja tukaj:
"%s" Niste izbrali veljavne zbirke podatkov Za uporabo tega menija morate prej urediti Nastavitve/Tipke za bližnjice Najprej morate shraniti trenutni dokument, preden lahko uporabite to funkcijo.
 Jo želite shraniti in nadaljevati? Vaša zbirka podatkov je veljavna zbirka podatkov bibus. ]-[ poljem nizom ASCII Avtorji avtorjev, zamenjaj od 2. do n. avtorja z spodaj biblioDB razločevanje velikih in malih črk CP1250 privzeto Glavna stran eTBlast: http://invention.swmed.edu/etblast/index.shtml eTBlast na PubMed ... Rezultat eTBlast Iskanje eTBlast v PubMed v 'ČLANKU' v zbirki podatkov obdrži novi sklic obdrži stari sklic zadnje Latin-2 localhost root Shrani geslo (ni varno!) tipka za bližnjico uporabi ločilo UTF-8 ko je več kot 
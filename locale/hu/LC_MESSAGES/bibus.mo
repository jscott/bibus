��    *     l  �  �"      H.  @  I.  F  �/     �0  8   �0     #1     %1     >1     Z1     ]1     f1     h1     z1  -   �1  o   �1  o   (2  ;   �2     �2     �2     �2     �2     �2     �2     3     "3     +3     83     A3     E3     N3     n3  
   z3  0   �3  	   �3     �3     �3     �3     �3  	   �3     �3      4  	   4     4     4     74  	   F4     P4     W4     c4     v4     �4     �4  
   �4     �4     �4     �4     �4     �4     �4     �4  B   5  #   J5  w   n5  P  �5  M  77     �8     �8     �8  	   �8     �8  
   �8     �8     �8     �8     �8     �8     �8  7   �8     9     9     +9     =9     S9     f9  /   x9     �9     �9     �9     �9     �9     �9  !   :  &   3:     Z:     c:     i:     o:     v:     }:     �:     �:  
   �:  
   �:     �:     �:     �:     �:     �:     �:     �:  !   ;     4;  !   C;     e;     y;     �;     �;     �;     �;     �;     �;     �;  	   �;     �;     �;     �;     �;     �;     <     <     $<     4<     D<     V<     g<     u<     �<     �<  
   �<     �<     �<     �<     �<  $   �<  
   =     (=     0=     >=     F=     ^=     o=     �=     �=     �=     �=  
   �=     �=     �=     �=     �=  
   �=     �=     �=     	>     %>     ->     4>  &   =>  "   d>     �>     �>     �>     �>  $   �>     �>     �>     �>  +   �>     +?     C?     \?  "   q?     �?     �?     �?     �?  
   �?     �?     �?     �?     @     @     @     !@     /@     K@     T@     j@     v@     �@     �@     �@  	   �@     �@     �@     �@     �@     A      A     4A     LA     QA  '   pA     �A     �A     �A     �A     �A     �A     �A  *   �A     B     B     B     &B     3B     AB     FB     JB  
   MB     XB     pB  *   |B  r   �B     C     3C     MC     TC     cC     xC     �C     �C     �C  )   �C     �C     D  !   D  7   ;D     sD     �D     �D     �D     �D  '   �D     �D  �   �D     �E     �E     �E     �E     �E     �E     �E     �E     F     F     F  
   #F  H   .F  $   wF     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F     
G     #G  +   8G  )   dG     �G     �G  
   �G     �G     �G     �G     �G     �G     �G     H     H     "H     (H     7H     PH     `H     lH     |H     �H     �H     �H     �H     �H     �H  	   �H  	   �H     �H     �H  	   �H     �H     I     I     )I     0I     <I     JI     OI     WI     ^I     mI  	   I     �I     �I     �I     �I     �I     �I     �I     �I     �I     J  "   *J     MJ     [J  	   hJ     rJ  
   ~J     �J     �J     �J     �J     �J     �J     �J  	   �J  �   �J  (   MK  1   vK     �K  %   �K     �K     	L      L      ;L     \L  $   yL     �L     �L     �L     �L     �L     �L  	   �L     �L  	   M     M      M     /M  	   8M     BM     TM     pM     xM  
   }M     �M  ,   �M  $   �M     �M     �M     N     N     N  	   ,N     6N     GN     YN     jN     yN  
   �N     �N     �N     �N     �N     �N     �N     �N     �N     �N     O     O     )O     .O  
   :O     EO     LO     SO  
   cO     nO     |O     �O  	   �O     �O  
   �O  
   �O  	   �O     �O     P     P     P  
   (P     3P     BP     IP     aP     vP     �P     �P  	   �P     �P  
   �P     �P  0   �P  Z    Q  >   [Q  S   �Q  M   �Q  =   <R  g   zR  "   �R  <   S     BS     QS     aS  
   vS      �S     �S     �S     �S     �S  	   �S     �S     �S     �S     T  
   T     *T  	   1T     ;T     GT     \T     qT  
   xT     �T  
   �T     �T     �T     �T     �T     �T     �T  ;   �T  5   -U  '   cU  [   �U  �   �U  p   uV  #   �V  	   
W  
   W  $   W     DW     JW     VW     ZW  	   fW     pW     sW     �W     �W     �W  %   �W     �W     X     $X  &   2X  
   YX  
   dX     oX     �X     �X     �X  	   �X     �X     �X     �X     �X     �X     �X      Y     Y     $Y     @Y     ^Y     dY     sY     �Y  �   �Y  K   2Z  B   ~Z  #   �Z  3   �Z  c   [     }[     �[     �[     �[     �[  $   �[     �[     �[     �[     �[     �[  A   �[     4\     J\     c\     p\     |\     �\     �\     �\  	   �\     �\     �\     �\     �\     �\     ]     ]  �   ]  �  �^  �  �`     b  F   9b     �b     �b  %   �b     �b     �b     �b     �b     �b  3   c  ~   <c  �   �c  H   <d     �d     �d     �d     �d     �d     �d  5   �d  	   �d     �d     e     e     e  :   .e     ie     |e  /   �e     �e     �e     �e     f     	f     f     f  
   (f  	   3f     =f     Kf     gf  	   }f     �f     �f     �f     �f     �f     �f     �f     �f     �f     �f     
g     "g     9g     Rg  ^   bg  ,   �g  �   �g  �  �h  �  j     �k     �k     �k  
   �k     �k     �k     �k     �k     �k     �k     �k     l  R   l  
   al     ll     |l     �l     �l     �l  .   �l     m     m     4m     <m  "   Sm     vm  +   �m  /   �m     �m     �m     n  	   n     n     !n     (n     6n     Rn     cn     pn     �n  	   �n     �n  #   �n     �n     �n  ,   �n     +o  -   Jo     xo     �o     �o     �o     �o     �o     �o  	   �o     �o  	   �o  
   �o     �o     p     p     2p     Cp     Sp     fp     }p     �p     �p     �p  "   �p     �p     �p     q     q     -q     >q     ]q  +   nq  
   �q  
   �q     �q  	   �q  '   �q     �q     r     !r     0r     6r     9r     Or     Xr     ^r     kr     |r     �r     �r  '   �r  "   �r     s     s  	   %s  %   /s  +   Us     �s     �s  
   �s     �s  +   �s     �s     t     t  -   #t     Qt      qt     �t  1   �t      �t     u     u     u  
   #u     .u     ?u     Fu     Yu     ku     qu     zu  .   �u     �u     �u     �u     �u     v     /v     Av     Ov  	   \v     fv     �v     �v     �v     �v     �v     w  %   w  .   8w     gw     �w     �w     �w     �w     �w     �w  *   �w     �w     x     x     %x     5x     Fx     Kx     Ox     Rx     ^x     qx  @   ~x  �   �x     Ny     iy     �y     �y     �y     �y     �y     z     z  2   1z     dz     xz  %   �z  X   �z     {      {  	   -{     7{  '   S{  3   {{     �{  �   �{     �|  
   �|     �|  
   �|  %   �|     �|     �|     �|     �|     }     }     #}  Y   3}  $   �}     �}     �}     �}     �}     �}     �}      ~     ~  
   ~     ~     '~     @~  ;   T~  1   �~     �~     �~  
   �~     �~     �~          '     ;  ,   @     m     �     �     �     �     �     �     �     �     �     �     #�     '�     /�     A�     T�     e�  
   v�  
   ��     ��     ��     ��          Ӏ     ۀ     �     ��      �     �     �     �  
   3�     >�     A�     J�     Q�     _�     o�  '   ��     ��  #   ��     ߁  +   ��     '�     3�  	   A�     K�     Y�     k�     s�     {�     ��  
   ��     ��     ��  	   ��  �   ��  0   t�  ?   ��  (   �  5   �  !   D�     f�     ��  *   ��  &   ̄  4   �     (�  	   -�  ,   7�     d�  
   s�     ~�     ��     ��     ��     ��     ԅ  
   �     �     ��  #   �     7�  	   F�     P�     g�  0   k�  1   ��  !   Ά     ��     �     �     �     '�     0�     C�     Z�     h�     t�     ��     ��     ��     ��     ʇ     ݇     ��     �     �     $�     0�     D�     [�     c�     t�     ��     ��     ��     ��     ��  #   ̈      ��     �     ,�     F�     V�     k�  "   y�     ��     ��     ��     ҉     �     ��     ��  "   �  "   9�     \�  
   |�     ��     ��  	   ��     ��  /   ��  �   �  C   q�  ^   ��  ^   �  E   s�  h   ��  0   "�  A   S�     ��     ��  3   ��     �  %   �  	   '�     1�     :�     Y�     a�     m�  !   �     ��  !   ��     َ  	   �     ��     ��     	�     �     3�     :�  (   L�     u�     ��     ��  "   ��  
   Ə     я     ׏  O   �  D   7�  &   |�  V   ��  �   ��  �   ��  %   �     A�     Q�  0   b�     ��     ��     ��     ��     ��     Ò  #   ǒ     �  "   �  /   )�  )   Y�     ��     ��     ��  3   ̓      �     �  )   $�  ,   N�     {�     ��     ��     ��     Ȕ     ڔ  	   ��      �     �     �     -�     D�     Z�     y�     ��     ��     ��  �   ��  j   ��  K   �  &   ;�  g   b�  ]   ʗ     (�  	   ,�     6�     >�     D�  L   M�     ��     ��     ��     Ș     Ϙ  @   ��     !�     9�  
   T�     _�     m�     ��     ��     ��  	   ��     ��  $   ��     �     �     �     	�     �     &   k  b  3  #  �     �       �           �  �  �  �         �      �      ?             �   ~  a       �   a      s   �              t  �  �         �             >  �              �   Z     �      �   �           9               )   �  "  �  �   d   �   �   �       �   }      �        �   �   g            �   �      �  �   %  ^    �      �      �  �  �       �        �      �      _  �    �  �          y            �   Q  �  '  �      _   R                  �      �  �       �   *  d  �           �        %   ;   |   �     �   (      w          V  �  Q      %     �   1   �     K  �   $    '       |     �  +  �   �  �           �  �      �      �         �   �     c   #  �   �  o      �      �   �  �                   Y   i          �       ;  �         �  �  !      E  F   D  �    �  �     '  p  �          �       [             �  �            �  �   �  �       \  �  �                   C     b       "  �   8    f  1  B  �   (  �   r  =           �  �      �  -   7   �  N       �      �   j   n      �      (   P  }     N      �       :  �   �   *   i   �   A   /  @  o   �          �     g       �  �             �       �  r                  �          l    {               �  �           S  s              !   �         �       �       �   \   �  [  �       �  �   0   �  B           O      �   �   e   <   �      z   �   �  H            �  E       R  $  -  O   $   m    &  �  �   �                M   &  f         �               �   )  �  !              #   >           q  V     �  6       �         �       �   
   ^   P   �   �   /   �  ?      4  �   �   �   �        �   �      )  @   F  A  �   �       W         M  �  �   n             ~           ]  �               �       �  6  �  .   �   �   2   �   .      H   I   J   K   L   �      �   �  �  l   G   L      x  Y  �  `  U       T      �  =  �   �       �   �  �  �   �  	      �   �          �   �      U  �  e    �   �   D   �  �           �  �  p   �      �      �  ,   �     :            �  �   �       �        9      �  �   
      �    Z  �  G        7          J  �      �      +   W    h           �  �   �     �      �   �   �       t   u   v   w   x   4   *        5  �  �          m   �  �   �       �  �               �  X  �     z  �   �  �   v  �    �       �      �  �   �   �  �  �    "   {   ]         C   �   �   y   5   8     c      �  �   �   <  0  �             �  T   u  �  �           `     �               S           �             h   �   �  �             �    �  2  �  �     �   	    �  �      
  j  �   �   q     �   �  �      �  I  ,  3   �  k   	   X   �    
Bibus can directly insert citations in OpenOffice.org
if you start OpenOffice.org in 'listening mode'.


Do you want to activate this mode by default?


  1. A document will open in OpenOffice.org,
  2. Click on 'Accept UNO connections',
  3. Quit OpenOffice (and the Quickstarter if used),
  4. Restart OpenOffice.org. 
Bibus can directly insert citations in OpenOffice.org
if you start OpenOffice.org in 'listening mode'.


Do you want to activate this mode by default?


A document will open in OpenOffice.org,
Click on 'Accept UNO connections',
Then restart OpenOffice.org.
Under Windows, you must also quit the OpenOffice.org Quickstarter.

  Paste your text below:   min). No answer received. eTBLAST server might be down. % %s connection parameters %s references : %s selected ,  , et al. - - Click on Cancel - Click on Next - Follow the instructions in mysql_config.txt - If you want to use Bibus on a single computer (mono-user), 
            SQLite is presumably the best choice. - If you want to use Bibus on multiple computers (multi-user),
            MySQL is presumably the best choice. - You can change this choice at anytime in the preferences. -- /var/run/mysqld/mysqld.sock 3306 :  ;  ARTICLE Abbreviate author list with Abstract Acrobat file Activate Add Add Text Add a child to the selected key Add a field Add a line Add a reference associated with the selected key Add child Address Advanced preferences After All All files All references Annote Anonymous Anonymous ... Anonymous author format As in database Ascending Author Author list Author list format Author-Date BOOK BOOKLET Base Style Before last BibTeX Bibliographic index BibliographicType Bibliography title Bibus MySQL Setup Bibus WEB site Bibus can directly insert citations
in you favorite word processor Bibus may use two database engines: Bibus needs a USERNAME to identify yourself.
Please enter a name in the box below.
Your login name might be a good idea Bibus uses a file to store the bibliographic database.

Please enter in the box below the path to 
the bibliographic database you want to use.

- If the file EXIST, Bibus will 
	- not modify it
	- assume it to be a correct Bibus database.

- If the file DOES NOT EXIST, Bibus will 
	- create it
	- use it as your bibliographic database. Bibus uses a file to store the bibliographic database.
Please enter in the box below the path to 
the bibliographic database you want to use.
- If the file EXIST, Bibus will 
	- not modify it
	- assume it to be a correct Bibus database.
- If the file DOES NOT EXIST, Bibus will 
	- create it
	- use it as your bibliographic database. Black Blue Bold Booktitle Brackets CONFERENCE CUSTOM1 CUSTOM2 CUSTOM3 CUSTOM4 CUSTOM5 Cancel Cannot connect to Openoffice. See Documentation.
%s
%s) Caps Capture ... Capture Citations Capture from database Capture from field Capture selection Capture the references absent from the database Case Change author list Chapter Check for duplicates Choose a File to import Choose the destination file Choose the file location and name Choose the name of the SQLite database Citation Cited Clear Codage Codecs Common Connect Connect to database Connect... Connection Connection type Content Copy Count Create Index on Insert Create User Create a new category Create the Bibliography if absent Create user... Creating or choosing the database Creating styles ... Current key Custom1 Custom2 Custom3 Custom4 Custom5 Cut Cyan DB engine Database Database = %s Database = None Database Creation Database Error Database Name Database Type Database choice Database engine Database file ... Database file... Database type Database used at startup Database... Delete Delete All Delete query Delete reference Delete the selected reference Delete this key Deleting old index and citations ... Descending Details Directory ... Display Display only keys like: Displayed fields Document position Documentation Done Down Duplicate reference Duplicates EMAIL Edit Edit ... Edit Identifier Edit query Edit reference Edit shortcuts ... Edit the selected reference Edition Editor Encoding Enter your connection parameters below Enter your email below (Optional): Error Error during query Expert Expert Search Experts search is not available yet! Experts' papers Export Export a file Export formatted references in an HTML file Export to BibTeX format Export to Medline format Export to RIS format Export to Refer format for EndNote Export to a SQLite database Field Field Value Field color Field name Field name color Fields Fields formatting Fields ordering File File ... File name ... Fill character in tab stops Finalize Finalize the document Finalize... Finalizing the current document First Connection Wizard First Editor First author First key Format Format Author as Format Bibliography Format Editor as Format fields as Format reference as Formating citations ... Full Fuse: [1] [3] [2] -> [1; 3; 2] Fuse: [Martin] [John] -> [Martin; John] Fusing citations ... GoTo Writer Grants Green HTML Help Hilight Citations Hilight citations with a yellow background Host Howpublished INBOOK INCOLLECTION INPROCEEDINGS ISBN ISI Id Identifier If a duplicate is found If at least If author Field is empty, replace it with: If you have the necessary rights (MySQL Administrator),
click the button below to create/change the MySQL database If you want to use MySQL If you want to use SQLite Import Import && Quit Import a BibTeX file Import a Medline XML file Import a Medline file Import a RIS file Import a Refer file Import a Refer file exported from EndNote Import a file Import a text file Import an ISI Web of science file Import an XML library generated with Endnote 9 or Later Import buffer Information Insert Insert Citation Inserting citations in text Inserting citations in text (%s ranges) Institution It seems that your database is not compatible with your PySQLite version.
If it does not work,
see the SQLite site about database format change in SQLite3 http://www.sqlite.org/version3.html Italic JOURNAL Joining Journal Keep the best citations:  Key type Keys Language Last Editor Last author License Light_grey List all authors on first occurence if authors number is not higher than List more authors until it is unique Load ... Looking for duplicates ... MANUAL MASTERTHESIS MISC MS Word MSWord Main Main Fields Main Fields = %s Main fields (+ Abstract) Main fields searched Make a copy of the first selected reference Making a copy of the current document ... Medline Medline Search MedlineXML Microsoft Word Microsoft Word Document Middle Editors Middle authors Mode Modify User Grants Modify user... Month MySQL MySQL database MySQL database setup ... MySQL or SQLite MySQL setup MySQL v3 and v4 MySQL v4.1 and v5 Name Name format New New ... New Database... New Name format New Query New query New reference NewKey No Choice No Database selected None Norma Jean Baker Normal Normal (rw) Not connected Note Nothing Number Number entries Number of records Numbering OK OOo_pipe Online Online buffer Open URL OpenDocument Text OpenOffice connection settings OpenOffice.org OpenOffice.org Text Document OpenOffice.org connection OpenOffice.org connection settings Organizations Other Fields PHDTHESIS PROCEEDINGS Page Setup Pages PassWord Password Paste Paths Perso Pipe Pipe name Please choose below the Word Processor you want to use with Bibus.
You can change this setting at any time in Bibus menu Edit/Preferences. Please choose the bibliographic database Please choose the database engine you want to use Please choose the file encoding Please enter password for database %s Please select a style file Please select the file Please, enter the new name Please, enter the new query name Please, enter the query name Please, select the default directory Port Position Position of the tab stop (mm) Preferences Preview Preview ... Preview : Preview printing Print ... Print references Printed fields Printing Publisher Pubmed search ... Put a letter after the year Queries Quit Quit Bibus RIS Read-only with private and shared trees (ro) Read-only with shared tree only (rk) Read-only without tree (rr) Records with Z-Score >= Red Refer Refer (EndNote) Reference Reference Editor Reference display Reference fields Reference list Reference type References References formatting Remove Rename Query Rename folder Rename query Rename this key Report_Type SQLite SQLite file SQLite setup SQlite database file Save Save as ... Save as... School Search Search && Close Search ... Search PubMed Search Pubmed on the Web Search Pubmed with eTBlast Search in Search the database Second key Select All Selection Separate authors with Separator 1 Separator 2 Separator 3 Separators Separators ... Series Set printer page format Set some preferences Setting database and rights Setting username Shared Shortcuts Show All Small Caps Socket Solving duplicates ... (%s series of duplicates) Some needed fields are absent from the selected database/tables.
Continue at your own risk Sorry but codec '%s' is not available.
I will try to use ascii Sorry but the MySQL python module (MySQLdb) is not available.
Read installation.txt Sorry but the SQLite python extension is not available.
Read installation.txt Sorry but the key you are moving already exist in this folder Sorry, but I was not able to find the python module for the %s database.
Please check your installation Sorry, you cannot modify this key. Sorry, you cannot move a reference associated with this key. Sort Ascending Sort Descending Sort bibliography by Sort order Sort: [1] [3] [2] -> [1] [2] [3] Sorting Standard Start at record Style Style ... Style author Style creation date Style informations Style modification date Style name Styles Subscript Superscript Supplementary Fields Supplementary fields TCP/IP TECHREPORT Tab stop is right aligned Tabulation Tag Tag reference Tag the selected reference Tagged Template Text Window The identifier was not unique and it has been changed to %s The key must be unique.
Please choose a new key name. The selected style is not a bibus style The version of the style file is too old.
 I will convert it to the new Bibus style format. The version of the style file is too old.
 I will convert it to the new Bibus style format.
To get rid of this message, please save it again. The version of the style file is too recent. Please update bibus to a new version.
 I will open a default style. There is %s such records in Medline Third key Time Out ( Time Out (3 min). No answer received Title UNPUBLISHED URL URL/File... Underline Up Update Bibliographic Index Update Citations Update Index on Insert Update and Pre-format all... Update the Bibliography automatically Updating index ... Updating references ... Use Database: Use OpenOffice.org format for printing Use TCP/IP Use a pipe Use current language for month Use range: [1] [3] [2] -> [1-3] User User Creation User Name UserName Username Using separator Volume WWW Warning Welcome to Bibus ! Welcome to bibus What do you want to export? Where to save the style file? White Word Processor XML From Endnote Year You can set here the environment variable $FILES.
This variable is substituted when an URL is opened.
This allows you to set a central repository
for all the articles.
 You cannot edit default styles
.I have made a copy of the style for editing You cannot use %r and %r characters.
Please choose a new key name. You did not select a valid database You must first edit Pref/Shortcuts to use this menu You must first save the current document before using this function.
 Should I save it and proceed? ]-[ a Field a String ascii authors authors, replace authors 2 to n with below biblioDB case sensitive cp1252 default eTBlast main page: http://invention.swmed.edu/etblast/index.shtml eTBlast on PubMed ... eTBlast search on PubMed in 'ARTICLE' in database keep the new reference keep the old reference last latin-1 localhost root save password (Not secure!) shortcut using separator utf-8 utf_8 when there are more than Project-Id-Version: bibus-1.1.0
POT-Creation-Date: 
PO-Revision-Date: 2007-08-14 20:22+0100
Last-Translator: Farkas Gergely <psziroot@yahoo.co.uk>
Language-Team: Farkas Gergely <psziroot@yahoo.co.uk>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Poedit-Basepath: .
X-Poedit-Language: Hungarian
X-Poedit-Country: HUNGARY
X-Poedit-SearchPath-0: /home/pmartino/Desktop/Bibus/bibus-cvs
 
A Bibus közvetlenül tud hivatkozásokat illeszteni az OpenOffice.org-ba,
ha az OpenOffice.org-ot "listening mode"-ban indítja.


Akarja ezt a módot alapértelmezettként aktívvá tenni?


  1. Egy dokumentum fog megnyílni az OpenOffice.org-ban.
  2. Klikkeljen az "'Accept UNO connections"-ra.
  3. Lépjen ki az OpenOffice-ból (és a Quickstarter-ből, ha használja).
  4. Indítsa újra az OpenOffice.org-ot. 
A Bibus közvetlenül tud hivatkozásokat illeszteni az OpenOffice.org-ba,
ha az OpenOffice.org-ot "listening mode"-ban indítja.


Akarja ezt a módot alapértelmezettként aktívvá tenni?


Egy dokumentum fog megnyílni az OpenOffice.org-ban.
Klikkeljen az "'Accept UNO connections"-ra.
Indítsa újra az OpenOffice.org-ot.
Windows alatt ki kell lépnie az OpenOffice.org Quickstarter-ből is.

 Illessze ide a szöveget: perc). Nem érkezett válasz. Talán nem működik az eTBLAST szerver. % %s kapcsolódási  %s tételek száma : %s kiválasztott ,  ,és mtsai. - - Klikkeljen a "Mégsem"-re - Klikkeljen a "Következő"-re Kövesse az instrukciókat a "mysql_config.txt"-ben Ha egy számítógépen akarja a Bibus-t használni (egy felhasználó)
            Valószínűleg az SQLite a jó megoldás. Ha több számítógépen akarja a Bibus-t használni (több felhasználó)
            Valószínűleg a MySQL a jó megoldás.  - Bármikor megváltoztathatja ezt a választást a beállításoknál. -- /var/run/mysqld/mysqld.sock 3306 :  ;  CIKK Szerzők listájának rövidítése a következővel: Absztrakt Acrobat file Aktiválás Hozzáadás Szöveg hozzáadása Alárendelt címke hozzáadása a kiválasztott címkéhez Mező hozzáadása Sor hozzáadása Tétel hozzáadása a kiválasztott címkéhez. Alárendelt címke hozzáadása Elérhetőség Haladó beállítások Után Mind Minden file Minden tétel Kommentár Névtelen Névtelen ... Névtelen szerző formátum Mint az adatbázisban Növekvő Szerző Szerzőlista Szerzőlista formátum Szerző-Dátum KÖNYV FÜZET Alap stílus Utolsó előtt BibTeX Irodalomjegyzék index Irodalomjegyzék típus Irodalomjegyzék címe Bibus MySQL beállítás Bibus weboldala A Bibus közvetlenül képes hivatkozások beillesztésére
a kedvenc szövegszerkesztőjébe. A Bibus 2 adatbázis típust tud használni: A Bibusnak szüksége van egy felhasználónévre, hogy  azonosítsa Önt.
Kérem írjon be a lenti mezőbe egy nevet.
A bejelentkezési neve jó ötlet lehet. A Bibus egy fájlt használ a bibliográfiai adatbázis tárolására.
Kérem írja be a lenti mezőbe az elérési utat
a használni kívánt bibiográfiai adatbázishoz.
- Ha a file LÉTEZIk, akkor a Bibus
	- nem módosítja
	- elfogadja, mint helyes Bibus adatbázist.
- Ha a file NEM LÉTEZIK, akkor a Bibus
	- létrehozza
	- használatba veszi, mint az Ön bibliográfiai adatbázisát. A Bibus egy fájlt használ a bibliográfiai adatbázis tárolására.
Kérem írja be a lenti mezőbe az elérési utat
a használni kívánt bibiográfiai adatbázishoz.
- Ha a file LÉTEZIk, akkor a Bibus
	- nem módosítja
	- elfogadja, mint helyes Bibus adatbázist.
- Ha a file NEM LÉTEZIK, akkor a Bibus
	- létrehozza
	- használatba veszi, mint az Ön bibliográfiai adatbázisát. Fekete Kék Félkövér Könyvcím Zárójelek KONFERENCIA EGYÉNI1 EGYÉNI2 EGYÉNI3 EGYÉNI4 EGYÉNI5 Mégsem Nem sikerült kapcsolódni az OpenOfficehoz. Tekinse meg a dokumentációt.
%s
%s) Nagybetűk Rögzítés ... Hivatkozások rögzítése Rögzítés adatbázisból Rögzítés mezőből Kijelölés rögzítése Adatbázisból hiányzó tételek rögzítése Eset Szerzőlista megváltoztatása Fejezet Azonosak ellenőrzése Importálandó file kiválasztása Célfájl kiválasztása Fájl helyének és nevének kiválasztása Válasszon nevet az SQLite adatbázis számára Hivatkozás Hivatkozott Kiürítés Kódolás Codecek Common Kapcsolódás Kapcsolódás adatbázishoz Kapcsolódás... Csatlakozás Kapcsolódás típusa Tartalom Másolás Sorszám Index létrehozása beillesztéskor Felhasználó létrehozása Új kategória készítése Irodalomjegyzék létrehozása, ha hiányzik Felhasználó létrehozása... Adatbázis létrehozása, vagy kiválasztása Stílusok készítése ... Jelenlegi címke Egyéni1 Egyéni2 Egyéni3 Egyéni4 Egyéni5 Kivágás Cián DB engine Adatbázis Adatbázis = %s Adatbázis = Nincs Adatbázis létrehozása Adatbázis hiba. Adatbázis név Adatbázis típusa Adatbázis választás Adatbázis típus Adatbázis file ... Adatbázis file ... Adatbázis típusa Indításnál használt adatbázis Adatbázis... Törlés Összes törlése Lekértezés törlése Tétel törlése Kiválasztott tétel törlése Címke törlése Régi index és hivatkozások törlése ... Csökkenő Részletek Könyvtár ... Kiijelző Csak ehhez hasonló címkék mutatása: Megjelenített mezők Dokumentum elhelyezkedés Dokumentáció Kész Le Tétel megkettőzése Azonosak EMAIL Szerkesztés Szerkesztés ... Azonosító szerkesztése Lekérdezés szerkesztése Tétel szerkesztése Rövid elérési utak szerkesztése ... Kiválasztott tétel szerkesztése Kiadás Szerkesztő Kódolás Írja be a csatlakozás paramétereit Írja be az e-mail címét (választható): Hiba Hiba a lekérdezés során. Szakértő Szakértő keresés A szakértői keresés még nem elérhető! Szakértők papírjai Exportálás File exportálása Formázott tételek exportálása HTML fileba Exportálás BibTeX formátumba Exportálás Medline formátumba Exportálás RIS formátumba Exportálás Refer formátumba  EndNote számára Exportálás SQLite adatbázisba Mező Mező érték Mező színe Mező név Mezőnév színe Mezők Mezők formázása Mezők rendezése Fájl File ... Fájl név ... Tabulátor megállások karakteres kitöltése Véglegesítés Dokumentum véglegesítése Véglegesítés ... Dokumentum véglegesítése Első kapcsolat varázsló Első szerkesztő Első szerző Első címke Formátum Szerző formázása, mint Irodalomjegyzék formázása Szerkesztő formázása, mint Mezők formázása, mint Tétel formázása, mint Hivatkozások formázása ... Teljes Egyesítés: [1] [3] [2] -> [1; 3; 2] Egyesítés: [Martin] [John] -> [Martin; John] Hivatkozások egyesítése ... GoTo Writer Hozzáférés Zöld HTML Súgó Hivatkozások kiemelése Hivatkozások kiemelése sárga háttérel Kiszolgáló Publikáció módja KÖNYVFEJEZET GYŰJTEMÉNYBEN JEGYZŐKÖNYVBEN ISBN ISI Id Azonosító Ha azonosat talál Ha legalább Ha a szerző mező üres, akkor helyettesítse a következővel: Ha akarja a szükséges jogokat (MySQL Adminisztrátor),
klikkeljen a lenti gombra, hogy létrehozza vagy megváltoztassa a MySQL adatbázist. Ha MySQL-t akar használni Ha SQLite-ot akar hsználni Importálás Importálás és kilépés BibTeX file importálása  Medline XML file importálása Medline file importálása RIS file imporálása Refer file importálása Refer file (EndNote-ból exportált) importálása Fájl importálása Szövegfájl importálása ISI Web of science file importálása XML könyvtár Endnote 9-el vagy későbbi verzióval generált fájlának importálása Import puffer Információ Beilleszt Hivatkozások beillesztése Hivatkozások beillesztése a szövegbe Hivatkozások beillesztése a szövegbe (%s helyen) Intézet Úgy tűnik, hogy az adatbázis nem kompatibilis a PySQLite verzióval.
Ha nem működik nézze meg az SQLite weboldalán az SQLite3 adatbázis formátumának változásait.
http://www.sqlite.org/version3.html   Dőlt FOLYÓIRAT Egyesítés Folyóirat Tartsa meg a legjobb hivatkozásokat: Címke típusa Címkék Nyelv Utolsó szerkesztő Utolsó szerző Licenc Világosszürke Minden szerző listázása első előforduláskor, ha a szerzők száma nem nagyobb, mint Szerzők listázása, amíg egyediek Betöltés ... Azonosak keresése ... KÉZIKÖNYV DIPLOMAMUNKA EGYÉB MS Word MS Word Fő Fő mezők Fő mezők = %s Fő mezők (+ Absztrakt) Keresett fő mezők Másolat készítése az először kiválasztott tételről Másolat készítése erről a dokumentumról ... Medline Medline keresés MedlineXML Microsoft Word Microsoft Word dokumentum Középső szerkesztők Középső szerzők Mód Felhasznál hozzáférésének módosítása Felhasználó módosítása... Hónap MySQL MySQL adatbázis MySQL adatbázi beállítása MySQL vagy SQLite MySQL beállítás MySQL v3 és v4 MySQL v4.1 és v5 Név Név formátum Új Új ... Új adatbázis... Új név formátum Új lekérdezés Új lekérdezés Új tétel Új címke Nincs választás Nincs kiválasztva adatbázis Semmi Norma Jean Baker Normál Normál (rw) Nincs kapcsolat Jegyzet Semmi Szám Szám bejegyzések Bejegyzések száma Számozás OK OOo pipe Online Online puffer URL megnyitása OpenDocument Szöveg OpenOffice kapcsolódás beállítások OpenOffice.org OpenOffice.org Szöveges Dokumentum OpenOffice.org csatlakozás OpenOffice.org kapcsolódás beállításai Szervezetek Egyéb mezők PHD MUNKA JEGYZŐKÖNYV Oldalbeállítás Oldalak Jelszó Jelszó Beillesztés Útvonalak Perso Pipe Pipe neve Kérem válassza ki a szövegszerkesztőt, amit a Bubus-al együtt kíván használni.
Bármikor megváltoztathatja ezt a beállítást a Bibus menüjének Szerkesztés/Beállítások pontjában. Kérem válassza ki a bibliográfiai adatbázist Kérem válassza ki a használni kívánt adatbázis típusát. Kérem válassza ki a fájl kódolását Kérjem adjon meg jelszót ennek az adatbázisnak: %s Kérem válasszon stílus fájlt. Kérem válassza ki a file-t Kérem, adja meg az új nevet Kérem, adja meg az új lekérdezés nevet Kérem, adja meg a lekérdezés nevét Kérem válassza ki az alapértelmezett könyvtárat Port Pozíció A tabulátor megállásnak pozíciója  (mm) Beállítások Előnézet Előnézet ... Előnézet: Előnézeti nyomtatás Nyomtatás ... Tételek nyomtatása Kinyomtatott mezők. Nyomtatás Kiadó Keresés Pubmed-del ... Betű illesztése az évszám után Lekérdezések Kilépés Kilépés a Bibus-ból RIS Csak olvasás saját és megosztott fákkal (ro) Csak olvasás kizárólag megosztott fákkal (rk) Csak olvasás fák nélkül. (rr) Bejegyzések Z-értéke >= Piros Refer Refer (EndNote) Tételek Tétel szerkesztő Tétel megjelenítése Tétel mezők Tétellista Tétel típusa Tételek Tételek formázása Eltávolítás Lekérdezés átnevezése Mappa átnevezése Lekérdezés átnevezése Címke átnevezése Tanulmány típus SQLite SQLite file SQLite beállítás SQLite adatbázis file Mentés Mentés mint ... Mentés mint ... Iskola Keresés Keresés és bezárás Keresés ... Keresés Pubmed-en Keresés Pubmed-del a világhálón Keresés a Pubmed-en eTBlast-tal Keresés a következőben: Keresés az adatbázisban Második címke Összes kijelölése Kiválasztás Szerzők elválasztásának módja Elválasztójel 1 Elválasztójel 2 Elválasztójel 3 Elválasztójelek Elválasztók ... Sorozat Nyomtató beállítása Néhány beállítás rögzítése Adatbázis és jogok beállítása Felhasználónév beállítása Megosztott Rövid elérési utak Minden mutatása Kisbetűk Socket Azonosak kezelése ... (%s sorban azonosságok) Néhány szükséges mező hiányzik a kiválasztott adatbázisból, vagy táblázatból.
Csak saját felelősségére folytassa. Sajnos a %s codec nem elérhető.
Megpróbálok ascii-t használni. Sajnálom, de a MySQL python modul (MySQLdb) nem elérhető.
Plvassa el az installation.txt-t. Sajnálom, de az SQLite python kiterjesztés nem elérhető.
Olvassa ela az installation.txt-t Sajnálom, de a címke, amit mozgat, már szerepel ebben a mappában. Sajnálom, de nem találtam python modult a %s atatbázis számára.
Kérem ellenőrizze a telepítést. Sajnálom, de nem módosíthatja ezt a címkét. Sajnálom, nem mozgathatja az ehhez a címkéhez rendelt tételt. Növekvő sorrend Csökkenő sorrend Irodalomjegyzék formázása a következő szerint: Rendezési sorrend Rendezés: [1] [3] [2] -> [1] [2] [3] Rendezés Standard Indítás adott bejegyzéstől Stílus Stílus ... Stílus szerzője Stílus létrehozásának dátuma Stílus információk Stílus módosításának dátuma Stílus neve Stílusok Alsó index Felső index Kiegészítő mezők Kiegészítő mezők TCP/IP MŰSZAKI LEÍRÁS A tabulátor megállás jobbra rendezett Tabuláció Megjelölés Tétel megjelölése Kiválasztott tétel megjelölése Megjelölt Minta Szöveges ablak Az azonosító nem volt egyedi és meg kellett változtatni a következőre: %s A kulcsnak egyedinek kell lennie.
Kérem válasszon új címkenevet. A kiválasztott stílus nem a bibus-é A stílus fájl verziószáma túl régi.
 Új Bibus stílus formátumra konvertálom. A stílus fájl verziószáma túl régi.
 Új Bibus stílus formátumra konvertálom.
Ennek az üzenetnek az eltüntetéséhez kérem mentse el újból. A stílus fájl verziószáma túl új. Kérem frissítse a bibus-t a legfrisebb verzióra.
Egy alapértelmezett stílust nyitok meg.  %s ilyen bejegyzés van a Medline-on. Harmadik címke Időtúllépés( Időtúllépés (3 perc). Nem érkezett válasz. Cím PUBLIKÁLATLAN URL URL/File... Aláhúzott Fel Irodalomjegyzék index frissítése Hivatkozások frissítése Index frissítése beillesztéskor Az összes frissítése és előformázása ... Irodalomjegyzék automatikus frissítése Index frissítése ... Tételek frissítése ... Adatbázis használata: OpenOffice.org formátum használata nyomtatáshoz. TCP/IP használata Pipe használata Jelenlegi nyelv alkalmazása a hónapokra Tartomány használata: [1] [3] [2] -> [1-3] Felhasználó Felhasználó létrehozás Felhasználónév Felhasználónév Felhasználónév Elválasztójel használata Évfolyam WWW Figyelmeztetés Üdvözlet a Bibus-ban ! Üdvözlet a bibus-ban Mit akar exportálni? Hova mentsem a stílus fájlt? Fehér Szövegszerkesztő XML Endnote-ból Év Itt környezeti változót tartalmazó fájlokat állíthat be.
A változó behelyettesítődik, amikor egy URL kerül megnyitásra.
Ez lehetőséget ad egy központi raktár beállítására az összes cikk számára.
 Alapértelmezett stílus nem szerkeszthető.
Készítettem egy másolatot a stílusról a szerkesztéshez. Nem használhat %r és %r  karaktereket.
Kérem válasszon új címkenevet. Nem választott érvényes adatbázist Először szerkesztenie kell a Beállítások/Rövid elérési utak-at, hogy használhassa ezt a menüt A funkció használata előtt menteni kell a dokumentumot.
 Mentsem és folytassam a munkát? ]-[ egy mező egy sor ascii szerzők szerző van, akkor 2-től n-ig a szerzők helyettesítése a következővel: lent biblioDB Kis és nagybetű különböző cp1252 alapértelmezett eTBlast weboldal: http://invention.swmed.edu/etblast/index.shtml eTBlast a Pubmed-en ... eTBlast keresés PubMed-en "CIKK"-ben adatbázisban új tétel megtartása régi tétel megtartása utolsó latin-1 localhost root jelszó mentése (Nem biztonságos!) shortcut elválasztó használata utf-8 utf_8 amikor több van, mint 
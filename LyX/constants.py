# constants.py         -*- coding: iso-8859-1 -*-
# Copyright (c) 2005 G�nter Milde
# Released under the terms of the GNU General Public License (ver. 2 or later)
"""
Customizable constants for the LyX package modules

This module is loaded by all LyX-package modules that need customizable 
constants for values that might differ in different settings.

 - LyX settings and properties
 - pyLyX settings and properties

CUSTOMIZATION:
    * per user:
        create ~/.lyx/scripts/python/config.py with your defaults
        
    * per module:
        import LyX.constants first, 
        set the new value, and
        import the needed module

      EXAMPLE:
          # myclient.py
          '''a client that connects to a different serverpipe'''
          from LyX import constants
          constants.SERVERPIPE = "~/.lyx/alternative-serverpipe"
          import LyX.lyxserver
          ...
"""

import sys, os, logging
import re

# LyX settings and properties
# ---------------------------

# the lyx binary
LYXCMD = 'lyx'                

# system wide LyX directory for layouts, bind-files, ...
SYSTEM_DIR = "/usr/share/lyx"  # this works for Debian

# per-user LyX directory
USER_DIR = "~/.lyx"            # what is the correct value for Windows?

# per-user LyX config file (Edit>Preferences)
LYXRC_FILE = os.path.join(USER_DIR, "preferences")

# the server pipe path (without ".in" and ".out") (Edit>Preferences>Paths)
SERVERPIPE = os.environ.get("LYXPIPE", "~/.lyx/lyxpipe" )

# command-line options (from `man lyx`)
LYX_OPTIONS = {'-help': 0, 
               '-version': 0, 
               '-sysdir': 1, 
               '-userdir': 1, 
               '-dbg': 1, 
               '-x': 1, '--execute': 1, 
               '-e': 1, '--export': 1,
               '-i': 2, '--import': 2,
               # xfroms fronted options
               '-geometry': 1, 
               '-display': 1, 
               '-bw': 1, 
               '-visual': 1, 
               '-depth': 1, 
               '-debug': 1, 
               '-sync': 0, 
               '-private': 0, 
               '-shared': 0, 
               '-stdcmap': 0, 
               '-name': 1
               }


# pyLyX settings and properties
# -----------------------------

VERSION = "pyLyX 0.3"

# per-user pyLyX configuration file
RC_FILE = os.path.join(USER_DIR, "scripts", "python", "config.py")

# Verbosity: show all messages of severity >= LOG_LEVEL 
# 0: ALL, 10: DEBUG, 20: INFO, 30: WARN, 40: ERROR, 50: CRITICAL
# (see `logging` standard module)
LOG_LEVEL = logging.INFO   

# default timeout for reading the outpipe (in ms)
LYXSERVER_POLL_TIMEOUT = 1000 # 1 s

# after starting, lyx, needs some time to setup the serverpipes
LYXSERVER_SETUP_DELAY = 2      # wait n seconds
LYXSERVER_SETUP_RETRIES = 10   # try n times to connect


# Setup
# -----

def setup():
    """Set up constants
    
       * Set up the logger instance
       * Read user configuration
       * Normalize paths and convert '~' to home directory
    """
    global SERVERPIPE, RC_FILE, USER_DIR, SYSTEM_DIR, LYXRC_FILE
    # Set up the logger instance
    logging.basicConfig()
    logger = logging.getLogger("constants")
    #set verbosity to show all messages of severity >= LOG_LEVEL
    logger.setLevel(LOG_LEVEL)
    # Read user configuration, log IOErrors (no file, ...) as warning
    RC_FILE = os.path.abspath(os.path.expanduser(RC_FILE))
    namespace = sys.modules[__name__].__dict__
    try:
        execfile(RC_FILE, namespace)
    except IOError:
        logger.debug(" Could not load config file '%s'"%RC_FILE)
        logger.debug(" Error message was", exc_info=True)
    # Normalize paths and convert '~' to home directory
    SERVERPIPE = os.path.abspath(os.path.expanduser(SERVERPIPE))
    USER_DIR = os.path.abspath(os.path.expanduser(USER_DIR))
    SYSTEM_DIR = os.path.abspath(os.path.expanduser(SYSTEM_DIR))
    LYXRC_FILE = os.path.abspath(os.path.expanduser(LYXRC_FILE))
    return

# run setup when imported
setup()

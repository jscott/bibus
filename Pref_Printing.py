# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# generated by wxGlade 0.3.4 on Mon Jan 31 21:17:09 2005

import wx
import BIB

COLORS = [_('Black'),_('White'),_('Red'),_('Blue'),_('Green'),_('Cyan'),_('Light_grey')]
HTLM_COLORS = ['000000','FFFFFF','FF0000','0000FF','00FF00','00FFFF','A0A0A0']

class Pref_Printing(wx.Panel):
	def __init__(self, *args, **kwds):
		self.lastType = None				# last selected Type
		self.format = BIB.PRINTER_FORMAT.copy()		# copy of the current format
		# begin wxGlade: Pref_Printing.__init__
		kwds["style"] = wx.TAB_TRAVERSAL
		wx.Panel.__init__(self, *args, **kwds)
		self.panel_print_alt = wx.Panel(self, -1)
		self.useOOoFormat = wx.CheckBox(self, -1, _("Use OpenOffice.org format for printing"))
		self.label_2 = wx.StaticText(self.panel_print_alt, -1, _("Reference type"))
		self.RefType = wx.ListBox(self.panel_print_alt, -1, choices=[], style=wx.LB_SINGLE)
		self.label_3 = wx.StaticText(self.panel_print_alt, -1, _("Printed fields"))
		self.PrintedFields = wx.ListBox(self.panel_print_alt, -1, choices=[], style=wx.LB_SINGLE)
		self.Fields = wx.Choice(self.panel_print_alt, -1, choices=[])
		self.Add = wx.Button(self.panel_print_alt, -1, _("Add"))
		self.Up = wx.Button(self.panel_print_alt, -1, _("Up"))
		self.Remove = wx.Button(self.panel_print_alt, -1, _("Remove"))
		self.Down = wx.Button(self.panel_print_alt, -1, _("Down"))
		self.FieldName = wx.StaticText(self.panel_print_alt, -1, _("Field name"))
		self.FN_Bold = wx.CheckBox(self.panel_print_alt, -1, _("Bold"))
		self.FN_Italic = wx.CheckBox(self.panel_print_alt, -1, _("Italic"))
		self.FN_Underline = wx.CheckBox(self.panel_print_alt, -1, _("Underline"))
		self.FN_Color = wx.Choice(self.panel_print_alt, -1, choices=[])
		self.FieldValue = wx.StaticText(self.panel_print_alt, -1, _("Field Value"))
		self.FV_Bold = wx.CheckBox(self.panel_print_alt, -1, _("Bold"))
		self.FV_Italic = wx.CheckBox(self.panel_print_alt, -1, _("Italic"))
		self.FV_Underline = wx.CheckBox(self.panel_print_alt, -1, _("Underline"))
		self.FV_Color = wx.Choice(self.panel_print_alt, -1, choices=[])

		self.__set_properties()
		self.__do_layout()
		# end wxGlade
		self.__set_evt()

	def __set_properties(self):
		# begin wxGlade: Pref_Printing.__set_properties
		self.Fields.SetSelection(0)
		self.FN_Color.SetSelection(0)
		self.FV_Color.SetSelection(0)
		# end wxGlade
		self.RefType.Set([BIB.NAME_TYPE[t] for t in BIB.BIB_TYPE])
		self.RefType.SetSelection(0)	# ARTICLE
		# Choice corresponding to self.format
		self.onType(None)
		# Setting colors
		self.FN_Color.AppendItems(COLORS)
		self.FV_Color.AppendItems(COLORS)
		namec,valuec = BIB.PRINTER_COLORS
		self.FN_Color.SetSelection( HTLM_COLORS.index(namec) )
		self.FV_Color.SetSelection( HTLM_COLORS.index(valuec) )
		# Setting style
		self.FN_Bold.SetValue(BIB.PRINTER_STYLE[0][0])
		self.FN_Italic.SetValue(BIB.PRINTER_STYLE[0][1])
		self.FN_Underline.SetValue(BIB.PRINTER_STYLE[0][2])
		self.FV_Bold.SetValue(BIB.PRINTER_STYLE[1][0])
		self.FV_Italic.SetValue(BIB.PRINTER_STYLE[1][1])
		self.FV_Underline.SetValue(BIB.PRINTER_STYLE[1][2])
		# printing type
		self.useOOoFormat.SetValue(BIB.PRINTER_USE_OOo_FORMAT)
		self.onCheckOOo(None)


	def __do_layout(self):
		# begin wxGlade: Pref_Printing.__do_layout
		sizer_1 = wx.BoxSizer(wx.VERTICAL)
		sizer_2 = wx.BoxSizer(wx.VERTICAL)
		sizer_5 = wx.StaticBoxSizer(wx.StaticBox(self.panel_print_alt, -1, _("Style")), wx.HORIZONTAL)
		grid_sizer_1 = wx.FlexGridSizer(2, 5, 2, 2)
		sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_6 = wx.BoxSizer(wx.VERTICAL)
		sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_4 = wx.GridSizer(2, 2, 2, 2)
		sizer_7 = wx.BoxSizer(wx.VERTICAL)
		sizer_1.Add(self.useOOoFormat, 0, wx.ALL, 5)
		sizer_7.Add(self.label_2, 0, wx.BOTTOM, 5)
		sizer_7.Add(self.RefType, 1, wx.EXPAND, 0)
		sizer_3.Add(sizer_7, 1, wx.EXPAND, 0)
		sizer_3.Add((10, 10), 0, 0, 0)
		sizer_6.Add(self.label_3, 0, wx.BOTTOM, 5)
		sizer_6.Add(self.PrintedFields, 1, wx.EXPAND, 0)
		sizer_8.Add(self.Fields, 1, wx.ALIGN_CENTER_VERTICAL, 0)
		sizer_4.Add(self.Add, 0, 0, 0)
		sizer_4.Add(self.Up, 0, 0, 0)
		sizer_4.Add(self.Remove, 0, 0, 0)
		sizer_4.Add(self.Down, 0, 0, 0)
		sizer_8.Add(sizer_4, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
		sizer_6.Add(sizer_8, 0, wx.EXPAND, 0)
		sizer_3.Add(sizer_6, 1, wx.EXPAND, 0)
		sizer_2.Add(sizer_3, 1, wx.ALL|wx.EXPAND, 5)
		grid_sizer_1.Add(self.FieldName, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
		grid_sizer_1.Add(self.FN_Bold, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FN_Italic, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FN_Underline, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FN_Color, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FieldValue, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
		grid_sizer_1.Add(self.FV_Bold, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FV_Italic, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FV_Underline, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.Add(self.FV_Color, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		grid_sizer_1.AddGrowableCol(0)
		sizer_5.Add(grid_sizer_1, 1, 0, 0)
		sizer_2.Add(sizer_5, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.EXPAND, 5)
		self.panel_print_alt.SetAutoLayout(1)
		self.panel_print_alt.SetSizer(sizer_2)
		sizer_2.Fit(self.panel_print_alt)
		sizer_2.SetSizeHints(self.panel_print_alt)
		sizer_1.Add(self.panel_print_alt, 1, wx.EXPAND, 0)
		self.SetAutoLayout(1)
		self.SetSizer(sizer_1)
		sizer_1.Fit(self)
		sizer_1.SetSizeHints(self)
		# end wxGlade

	def __set_evt(self):
		wx.EVT_LISTBOX(self,self.RefType.GetId(),self.onType)
		wx.EVT_LISTBOX(self,self.PrintedFields.GetId(),self.onPrintedFields)
		wx.EVT_BUTTON(self,self.Add.GetId(),self.onAdd)
		wx.EVT_BUTTON(self,self.Remove.GetId(),self.onRemove)
		wx.EVT_BUTTON(self,self.Up.GetId(),self.onUp)
		wx.EVT_BUTTON(self,self.Down.GetId(),self.onDown)
		wx.EVT_CHECKBOX(self,self.useOOoFormat.GetId(),self.onCheckOOo)
		
	def onCheckOOo(self,event):
		self.panel_print_alt.Enable( not self.useOOoFormat.GetValue() )

	def onType(self,event):
		# saving the format of previous type
		if self.lastType:
			self.format[self.lastType] = tuple( [BIB.FIELD_NAME[field] for field in self.PrintedFields.GetStrings()] )
		self.lastType = BIB.TYPE_NAME[self.RefType.GetStringSelection()]	# saving the choice for later
		# Choice corresponding to self.format
		self.PrintedFields.Set( [BIB.NAME_FIELD[field] for field in self.format[self.lastType]] )
		# Available Fields not already present
		self.Fields.Clear()
		for field in BIB.BIB_FIELDS:
			if field not in self.format[BIB.TYPE_NAME[self.RefType.GetStringSelection()]]:
				self.Fields.Append(BIB.NAME_FIELD[field])
		self.Fields.SetSelection(0)

	def onPrintedFields(self,event):
		# we could eventually activate/desactivate buttons
		return

	def onAdd(self,event):
		item = self.Fields.GetSelection()
		if item != -1:
			self.PrintedFields.Append(self.Fields.GetString(item))
			self.Fields.Delete(item)
			self.Fields.Select(0)

	def onRemove(self,event):
		item = self.PrintedFields.GetSelection()
		if item != -1:
			self.Fields.Append(self.PrintedFields.GetString(item))
			self.PrintedFields.Delete(item)

	def onUp(self,event):
		item = self.PrintedFields.GetSelection()
		if item > 0:
			self.PrintedFields.InsertItems((self.PrintedFields.GetString(item),),item-1)
			self.PrintedFields.Delete(item+1)
			self.PrintedFields.SetSelection(item-1)

	def onDown(self,event):
		item = self.PrintedFields.GetSelection()
		if item < self.PrintedFields.GetCount()-1 and item != -1:
			self.PrintedFields.InsertItems((self.PrintedFields.GetString(item),),item+2)
			self.PrintedFields.Delete(item)
			self.PrintedFields.SetSelection(item+1)

	def getSettings(self):
		"""Return the Printing settings"""
		if self.useOOoFormat.GetValue():
			return BIB.PRINTER_FORMAT, BIB.PRINTER_COLORS, BIB.PRINTER_STYLE, True
		else:
			self.onType(None)						# saving last modifications
			namec = HTLM_COLORS[self.FN_Color.GetSelection()]
			valuec = HTLM_COLORS[self.FV_Color.GetSelection()]
			style = (\
			(self.FN_Bold.GetValue() , self.FN_Italic.GetValue() , self.FN_Underline.GetValue() ),
			(self.FV_Bold.GetValue() , self.FV_Italic.GetValue() , self.FV_Underline.GetValue() ) )
			return self.format, (namec,valuec), style, False

# end of class Pref_Printing



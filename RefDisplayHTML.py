# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# -*- coding: ISO-8859-1 -*-

import wx
import StringIO
import Export.html

# we use as Base the wx.html.HtmlWindow
from wx.html import HtmlWindow as RefDisplayBase
import wx.html

class RefDisplay(RefDisplayBase):
	def __init__(self, *args, **kwds):
		RefDisplayBase.__init__(self, *args, **kwds)
		# we need a file to use the filter. We use a StringIO
		self.output = StringIO.StringIO()
		self.ref=''

	def resetFormat(self,format):
		"""Set the formatting style"""
		self.htmlExporter = Export.html.exportRef(self.output,format)
		self.display(self.ref)

	def Clear(self):
		self.output.truncate(0)
		self.SetPage('')

	def display(self,ref):
		"""Display the reference ref
		If ref = '' => clear the display
		"""
		self.Clear()
		self.ref = ref
		if ref:
			self.htmlExporter.write(ref)
			self.SetPage(self.output.getvalue())


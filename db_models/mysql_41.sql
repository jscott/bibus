-- MySQL dump 10.9
--
-- Host: localhost    Database: Biblio_41
-- ------------------------------------------------------
-- Server version	4.1.14-Debian_6-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bibquery`
--

DROP TABLE IF EXISTS `bibquery`;
CREATE TABLE `bibquery` (
  `query_id` int(10) unsigned NOT NULL auto_increment,
  `user` varchar(255) NOT NULL default '',
  `name` varchar(255) NOT NULL default 'query',
  `query` text NOT NULL,
  PRIMARY KEY  (`query_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bibquery`
--


/*!40000 ALTER TABLE `bibquery` DISABLE KEYS */;
LOCK TABLES `bibquery` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `bibquery` ENABLE KEYS */;

--
-- Table structure for table `bibref`
--

DROP TABLE IF EXISTS `bibref`;
CREATE TABLE `bibref` (
  `Id` int(10) unsigned NOT NULL auto_increment,
  `Identifier` varchar(255) default NULL,
  `BibliographicType` tinyint(3) unsigned NOT NULL default '0',
  `Address` varchar(255) NOT NULL default '',
  `Annote` varchar(255) NOT NULL default '',
  `Author` text NOT NULL,
  `Booktitle` varchar(255) NOT NULL default '',
  `Chapter` varchar(255) NOT NULL default '',
  `Edition` varchar(255) NOT NULL default '',
  `Editor` varchar(255) NOT NULL default '',
  `Howpublished` varchar(255) NOT NULL default '',
  `Institution` varchar(255) NOT NULL default '',
  `Journal` varchar(255) NOT NULL default '',
  `Month` varchar(255) NOT NULL default '',
  `Note` varchar(255) NOT NULL default '',
  `Number` varchar(255) NOT NULL default '',
  `Organizations` varchar(255) NOT NULL default '',
  `Pages` varchar(255) NOT NULL default '',
  `Publisher` varchar(255) NOT NULL default '',
  `School` varchar(255) NOT NULL default '',
  `Series` varchar(255) NOT NULL default '',
  `Title` varchar(255) NOT NULL default '',
  `Report_Type` varchar(255) NOT NULL default '',
  `Volume` varchar(255) NOT NULL default '',
  `Year` varchar(255) NOT NULL default '',
  `URL` varchar(255) NOT NULL default '',
  `Custom1` varchar(255) NOT NULL default '',
  `Custom2` varchar(255) NOT NULL default '',
  `Custom3` varchar(255) NOT NULL default '',
  `Custom4` varchar(255) NOT NULL default '',
  `Custom5` varchar(255) NOT NULL default '',
  `ISBN` varchar(255) NOT NULL default '',
  `Abstract` text NOT NULL,
  PRIMARY KEY  (`Id`),
  UNIQUE KEY `Identifier` (`Identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bibref`
--


/*!40000 ALTER TABLE `bibref` DISABLE KEYS */;
LOCK TABLES `bibref` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `bibref` ENABLE KEYS */;

--
-- Table structure for table `bibrefKey`
--

DROP TABLE IF EXISTS `bibrefKey`;
CREATE TABLE `bibrefKey` (
  `user` varchar(255) NOT NULL default '',
  `key_Id` int(10) unsigned NOT NULL auto_increment,
  `parent` int(10) unsigned default NULL,
  `key_name` varchar(255) NOT NULL default 'newkey',
  PRIMARY KEY  (`key_Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bibrefKey`
--


/*!40000 ALTER TABLE `bibrefKey` DISABLE KEYS */;
LOCK TABLES `bibrefKey` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `bibrefKey` ENABLE KEYS */;

--
-- Table structure for table `bibrefLink`
--

DROP TABLE IF EXISTS `bibrefLink`;
CREATE TABLE `bibrefLink` (
  `key_Id` int(10) unsigned NOT NULL default '0',
  `ref_Id` int(10) unsigned NOT NULL default '0',
  UNIQUE KEY `link` (`key_Id`,`ref_Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bibrefLink`
--

DROP TABLE IF EXISTS table_modif;
CREATE TABLE table_modif (
  `ref_Id` int(10) unsigned NOT NULL default '0',
  `creator` varchar(255) NOT NULL default '',
  `date` float NOT NULL default 0,
  `user_modif` varchar(255) NOT NULL default '',
  `date_modif` float NOT NULL default 0,
  UNIQUE KEY `ref_Id` (`ref_Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/*!40000 ALTER TABLE `bibrefLink` DISABLE KEYS */;
LOCK TABLES `bibrefLink` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `bibrefLink` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


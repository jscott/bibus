-- MySQL dump 9.11
--
-- Host: localhost    Database: Biblio2
-- ------------------------------------------------------
-- Server version	4.0.20-log

--
-- Table structure for table `bibquery`
--

DROP TABLE IF EXISTS bibquery;
CREATE TABLE bibquery (
  query_id int(10) unsigned NOT NULL auto_increment,
  user varchar(255) NOT NULL default '',
  name varchar(255) NOT NULL default 'query',
  query text NOT NULL,
  PRIMARY KEY  (query_id)
) TYPE=MyISAM;

--
-- Dumping data for table `bibquery`
--


--
-- Table structure for table `bibref`
--

DROP TABLE IF EXISTS bibref;
CREATE TABLE bibref (
  Id int(10) unsigned NOT NULL auto_increment,
  Identifier varchar(255) default NULL,
  BibliographicType tinyint(3) unsigned NOT NULL default '0',
  Address varchar(255) NOT NULL default '',
  Annote varchar(255) NOT NULL default '',
  Author text NOT NULL,
  Booktitle varchar(255) NOT NULL default '',
  Chapter varchar(255) NOT NULL default '',
  Edition varchar(255) NOT NULL default '',
  Editor varchar(255) NOT NULL default '',
  Howpublished varchar(255) NOT NULL default '',
  Institution varchar(255) NOT NULL default '',
  Journal varchar(255) NOT NULL default '',
  Month varchar(255) NOT NULL default '',
  Note varchar(255) NOT NULL default '',
  Number varchar(255) NOT NULL default '',
  Organizations varchar(255) NOT NULL default '',
  Pages varchar(255) NOT NULL default '',
  Publisher varchar(255) NOT NULL default '',
  School varchar(255) NOT NULL default '',
  Series varchar(255) NOT NULL default '',
  Title varchar(255) NOT NULL default '',
  Report_Type varchar(255) NOT NULL default '',
  Volume varchar(255) NOT NULL default '',
  Year varchar(255) NOT NULL default '',
  URL varchar(255) NOT NULL default '',
  Custom1 varchar(255) NOT NULL default '',
  Custom2 varchar(255) NOT NULL default '',
  Custom3 varchar(255) NOT NULL default '',
  Custom4 varchar(255) NOT NULL default '',
  Custom5 varchar(255) NOT NULL default '',
  ISBN varchar(255) NOT NULL default '',
  Abstract text NOT NULL,
  PRIMARY KEY  (Id),
  UNIQUE KEY Identifier (Identifier)
) TYPE=MyISAM;

--
-- Dumping data for table `bibref`
--


--
-- Table structure for table `bibrefKey`
--

DROP TABLE IF EXISTS bibrefKey;
CREATE TABLE bibrefKey (
  user varchar(255) NOT NULL default '',
  key_Id int(10) unsigned NOT NULL auto_increment,
  parent int(10) unsigned default NULL,
  key_name varchar(255) NOT NULL default 'newkey',
  PRIMARY KEY  (key_Id)
) TYPE=MyISAM;

--
-- Dumping data for table `bibrefKey`
--


--
-- Table structure for table `bibrefLink`
--

DROP TABLE IF EXISTS bibrefLink;
CREATE TABLE bibrefLink (
  key_Id int(10) unsigned NOT NULL default '0',
  ref_Id int(10) unsigned NOT NULL default '0',
  UNIQUE KEY link (key_Id,ref_Id)
) TYPE=MyISAM;

--
-- Dumping data for table `bibrefLink`
--

DROP TABLE IF EXISTS table_modif;
CREATE TABLE table_modif (
  ref_Id int(10) unsigned NOT NULL default '0',
  creator varchar(255) NOT NULL default '',
  date float NOT NULL default 0,
  user_modif varchar(255) NOT NULL default '',
  date_modif float NOT NULL default 0,
  UNIQUE KEY ref_Id (ref_id)
) TYPE=MyISAM;


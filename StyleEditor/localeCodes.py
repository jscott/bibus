# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# locales supported by OOo
LOCALE_NAMES = ['Afrikaans (South Africa)', 'Arabic (Egypt)', 'Arabic (Lebanon)', 'Arabic (Saudi Arabia)', 'Arabic (Tunisia)', 'Catalan (Spain)', 'Chinese (China)', 'Chinese (Hong Kong)', 'Chinese (Macau)', 'Chinese (Singapore)', 'Chinese (Taiwan)', 'Czech (Czech Republic)', 'Danish (Denmark)', 'Dutch (Belgium)', 'Dutch (Netherlands)', 'English (Australia)', 'English (Belize)', 'English (Canada)', 'English (Ireland)', 'English (Jamaica)', 'English (New Zealand)', 'English (Philippines)', 'English (South Africa)', 'English (Trinidad And Tobago)', 'English (United Kingdom)', 'English (United States)', 'English (Zimbabwe)', 'Estonian (Estonia)', 'Finnish (Finland)', 'French (Belgium)', 'French (Canada)', 'French (France)', 'French (Luxembourg)', 'French (Monaco)', 'French (Switzerland)', 'German (Austria)', 'German (Germany)', 'German (Liechtenstein)', 'German (Luxembourg)', 'German (Switzerland)', 'Greek (Greece)', 'Gujarati (India)', 'Hebrew (Israel)', 'Hindi (India)', 'Hungarian (Hungary)', 'Icelandic (Iceland)', 'Indonesian (Indonesia)', 'Italian (Italy)', 'Italian (Switzerland)', 'Japanese (Japan)', 'Kannada (India)', 'Korean (Korea)', 'Latvian (Latvia)', 'Lithuanian (Lithuania)', 'Marathi (India)', 'Norwegian (Norway)', 'Polish (Poland)', 'Portuguese (Brazil)', 'Portuguese (Portugal)', 'Punjabi (India)', 'Romanian (Romania)', 'Russian (Russian Federation)', 'Slovak (Slovakia (Slovak Republic))', 'Slovenian (Slovenia)', 'Spanish (Argentina)', 'Spanish (Bolivia)', 'Spanish (Chile)', 'Spanish (Colombia)', 'Spanish (Costa Rica)', 'Spanish (Dominican Republic)', 'Spanish (Ecuador)', 'Spanish (El Salvador)', 'Spanish (Guatemala)', 'Spanish (Honduras)', 'Spanish (Mexico)', 'Spanish (Nicaragua)', 'Spanish (Panama)', 'Spanish (Paraguay)', 'Spanish (Peru)', 'Spanish (Puerto Rico)', 'Spanish (Spain)', 'Spanish (Uruguay)', 'Spanish (Venezuela)', 'Swedish (Finland)', 'Swedish (Sweden)', 'Tamil (India)', 'Telugu (India)', 'Thai (Thailand)', 'Turkish (Turkey)', 'Ukrainian (Ukraine)', 'Welsh (United Kingdom)']
LOCALE_CODES = ['af_ZA', 'ar_EG', 'ar_LB', 'ar_SA', 'ar_TN', 'ca_ES', 'zh_CN', 'zh_HK', 'zh_MO', 'zh_SG', 'zh_TW', 'cs_CZ', 'da_DK', 'nl_BE', 'nl_NL', 'en_AU', 'en_BZ', 'en_CA', 'en_IE', 'en_JM', 'en_NZ', 'en_PH', 'en_ZA', 'en_TT', 'en_GB', 'en_US', 'en_ZW', 'et_EE', 'fi_FI', 'fr_BE', 'fr_CA', 'fr_FR', 'fr_LU', 'fr_MC', 'fr_CH', 'de_AT', 'de_DE', 'de_LI', 'de_LU', 'de_CH', 'el_GR', 'gu_IN', 'he_IL', 'hi_IN', 'hu_HU', 'is_IS', 'id_ID', 'it_IT', 'it_CH', 'ja_JP', 'kn_IN', 'ko_KR', 'lv_LV', 'lt_LT', 'mr_IN', 'no_NO', 'pl_PL', 'pt_BR', 'pt_PT', 'pa_IN', 'ro_RO', 'ru_RU', 'sk_SK', 'sl_SI', 'es_AR', 'es_BO', 'es_CL', 'es_CO', 'es_CR', 'es_DO', 'es_EC', 'es_SV', 'es_GT', 'es_HN', 'es_MX', 'es_NI', 'es_PA', 'es_PY', 'es_PE', 'es_PR', 'es_ES', 'es_UY', 'es_VE', 'sv_FI', 'sv_SE', 'ta_IN', 'te_IN', 'th_TH', 'tr_TR', 'uk_UA', 'cy_GB']

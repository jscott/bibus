# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#

# This module take care of styles.
# It autumatically inStyleert old style to new format
# It will generate a defautl style if needed

VERSION = '3.3'
# 3.3 added sorting order 'how,asc' in ['citation']['ad'] = fuse, sep, how, asc # how = (None,'index',field) asc = True if ascending
# 3.2 added numbering_rangeseparator in page_citation
# inStyleerter version used by this editor. 3.1 is an extension of 3.0 with underline and caps in addition to italic/bold 'fields ordering'
# inStyle['version'] = 2.0
# inStyle['fields'] = inStyle version 1.0 sauf version => fields formatting
# inStyle['info'] = page_Info
# inStyle['index'] = page_Index
# inStyle['ordering'] = page_Ordering
# inStyle['citation'] = page_Citation

# default_fields = Format.inStyleerter.def_inStyle
# default_info = {'name':u'','author':u'','creation':u'','modif':u'','note':u''}
# default_index = {'Title':u'Bibliography','IsNumberEntries':True,'Bracket':u'[]','Locale':u'en_US','SortAlgorithm':u'alphanumeric','IsSortByPosition':True,'SortKeys':((4,True),(23,True))}
#default_ordering = {}
# style 0=normal; 1=italic ; 2=bold ; 3=bold+italic
# 'field' -> TokenBibliographyDataField ; 'text' -> TokenText ; 'tab' -> TokenTabStop
#default_ordering['ARTICLE']=[('field','Author',0),('text',' (',0),('field','Year',0),('text','). ',0),('field','Title',0),('text','. ',0),('field','Journal',1),('text',' ',0),('field','Volume',2),('text',': ',0),('field','Pages',0),('text','.',0)]

# default style defined in Bibus/StyleEditor/default_style
# this is a pickle dictionary

from wx import LogError as wxLogError
import cPickle, os.path, BIB
DEFAULT_STYLE = os.path.join(BIB.SOURCEDIR,'StyleEditor','default_style')

def style_convert(inStyle = None):
	"""inStyleert the input Style inStyle to the current format with default values and return it
	If inStyle == None => we return default style"""

	if inStyle and inStyle['version'] == VERSION:
		return inStyle
	else:
		f=file(DEFAULT_STYLE,'r')	# we load the default style
		default = cPickle.load(f)
		f.close()
		try:
			if inStyle == None: raise KeyError
			#
			if inStyle['version'] > VERSION:
				wxLogError(_("The version of the style file is too recent. Please update bibus to a new version.\n I will open a default style."))
				raise KeyError
			#
			if inStyle['version'] == "1.0" :	# we convert to the new style
				wxLogError(_("The version of the style file is too old.\n I will convert it to the new Bibus style format.\nTo get rid of this message, please save it again."))
				inStyle['info'] = default['info']
				inStyle['index'] = default['index']
				inStyle['ordering'] = default['ordering']
				inStyle['citation'] = default['citation']
				inStyle['fields'] = inStyle
				inStyle['version'] = "2.0"
			#
			if inStyle['version'] == "2.0" :	# we convert to the new style
				wxLogError(_("The version of the style file is too old.\n I will convert it to the new Bibus style format.\nTo get rid of this message, please save it again."))
				inStyle['citation'] = default['citation']
				inStyle['version'] = "3.0"
			#
			if inStyle['version'] in ("3.0","3.1") :# 3.0 is compatible with 3.1 => no need to convert
				wxLogError(_("The version of the style file is too old.\n I will convert it to the new Bibus style format.\nTo get rid of this message, please save it again."))
				inStyle['citation']['numbering'] = inStyle['citation']['numbering'] + default['citation']['numbering'][-1:] # added numbering_rangeseparator
				inStyle['version'] = "3.2"
			#
			if inStyle['version'] == "3.2" :
				wxLogError(_("The version of the style file is too old.\n I will convert it to the new Bibus style format.\nTo get rid of this message, please save it again."))
				inStyle['citation']['ad'] = inStyle['citation']['ad'] + default['citation']['ad'][2:4] # added citation ordering	
				inStyle['version'] = "3.3"
		except KeyError:
			inStyle = default
		inStyle['version'] = VERSION
		return inStyle


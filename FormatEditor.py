import StyleEditor.FormatEditor

class FormatEditor(StyleEditor.FormatEditor.FormatEditor):
	def Save(self,evt):
		"""Save the Format.Converter and exit"""
		self.__Save()
		try:
			f = open(self.filename,'wb')
			cPickle.dump(self.conv,f,False)		# we save the style file. ascii mode for readibility
			f.flush()
			f.close()
			BIB.FORMAT_DICO = self.conv			# we keep the current format
			BIB.FORMAT_CONV = Format.Converter.Converter(self.conv['fields'])	# we update the Format.Converter
			self.Close()
		except IOError:
			self.SaveAs(evt)

	def SaveAs(self,evt):
		self.filename = wxFileSelector(_('Where to save the style file?'))
		#print "%r"%self.filename
		if self.filename:
			self.Save(evt)
			if os.path.dirname(self.filename) != os.path.join(BIB.SOURCEDIR,'Format','Styles'):
				BIB.CONFIG.writeStyle(self.filename)
			if self.filename not in BIB.STYLES.values():	# not already in style menu
				self.GetParent().addUserStyle(self.filename)
		else:
			self.Close()	# the user clicked cancel in the file dialog => we cancel everything


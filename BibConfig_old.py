# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import wx
import BIBbase, BIB, os.path, cPickle

def moveStyles(styles):
		"""We move old Styles to the new ~/.bibus/Styles directory"""
		for style in styles:
			try:
				os.rename( style, os.path.join( wx.StandardPaths.Get().GetUserDataDir(), "Styles", os.path.basename(style) ))
			except OSError:
				pass
		
def moveShortcuts(shortcuts,shortfiles):
		"""We move old Shortcuts to the new ~/.bibus/Shortcuts directory"""
		for i in xrange(len(shortcuts)):
			try:
				os.rename( shortfiles[i], os.path.join( wx.StandardPaths.Get().GetUserDataDir(), "Shortcuts", shortcuts[i] ))
			except OSError:
				pass

class BibConfig(wx.Config):
	#def __init__(self,appname):
	#	wx.Config.__init__(self)

	def readConfig(self):
		"""We set the variables in BIB module"""
		self.SetPath('/')
		if self.HasGroup('general'):
			BIB.FIRST_START = self.ReadBool('/general/firststart')
			BIB.CONFIG_VERSION = self.Read('/general/config_version')
			BIB.WP = self.Read('/general/wp',BIB.WP)	# word processor connection type OOo/mswDoc
		if self.HasGroup('mysql'):
			BIB.SOCKET = self.Read('/mysql/socket',BIB.SOCKET)
			BIB.PORT = self.ReadInt('/mysql/port',BIB.PORT)
			BIB.HOST = self.Read('/mysql/host',BIB.HOST)
			BIB.USER = self.Read('/mysql/user',BIB.USER)
			BIB.PASSWORD = self.Read('/mysql/passwd',BIB.PASSWORD)
			BIB.DB_NAME = self.Read('/mysql/name',BIB.DB_NAME)
		else:
			if not self.HasEntry('/mysql/user'):
				try:
					BIB.USER = getpass.getuser()	# We try to get a default value if not defined.
				except:
					BIB.USER = ''
		if self.HasGroup('sqlite'):
			BIB.SQLiteFile = self.Read('/sqlite/file',BIB.SQLiteFile)
			BIB.SQLiteUSER = self.Read('/sqlite/user',BIB.SQLiteUSER)
		else:
			if not self.HasEntry('/sqlite/user'):
				try:
					BIB.SQLiteUSER = getpass.getuser()	# We try to get a default value if not defined.
				except:
					BIB.SQLiteUSER = ''
		if self.HasGroup('db'):
			BIB.STORE_PASSWD = self.ReadBool('/db/store_passwd',BIB.STORE_PASSWD)
			BIB.DB_STARTUP = self.ReadInt('/db/last_db',BIB.DB_STARTUP)
			BIB.DB_TYPE = self.Read('/db/type',BIB.DB_TYPE)
			BIB.DUPLICATES_TEST = self.ReadBool('/db/duplicates_test',BIB.DUPLICATES_TEST)	# True if we want to test for duplicate entries
			if self.HasEntry('/db/duplicates'): BIB.DUPLICATES = eval(self.Read('/db/duplicates'))	# fields to test
			BIB.DUPLICATES_CASE = self.ReadBool('/db/duplicates_case',BIB.DUPLICATES_CASE)	# True == comparison is Case sensitive
			BIB.DUPLICATES_KEEP_OLD = self.ReadBool('/db/duplicates_keep_old',BIB.DUPLICATES_KEEP_OLD)	# True == we keep the old ref
		if self.HasGroup('display'):
			if self.HasEntry('/display/fields'): BIB.LIST_DISPLAY = eval(self.Read('/display/fields'))	# Fields displayed in reflist wx.ListCtrl
			BIB.LIST_ORDER = self.Read('/display/order',BIB.LIST_ORDER)				# Display order in reflist
			BIB.LIST_HOW = self.Read('/display/how',BIB.LIST_HOW)	# ASC or DESC ordering
			if self.HasEntry('/display/col_size'): BIB.LIST_COL_SIZE = eval( self.Read('/display/col_size') ) # column sizes (tuple)
			if self.HasEntry('/display/display_format'): BIB.DISPLAY_FORMAT = self.ReadInt('/display/display_format',BIB.DISPLAY_FORMAT)	# select tab in bottom-right main window
			if self.HasEntry('/display/key_color'): BIB.KEY_COLOR = eval('wx.Colour'+self.Read('/display/key_color'))		# color display of keys (Title, AUthor, etc..) in textCtrl-1
			if self.HasEntry('/display/text_color'): BIB.TEXT_COLOR = eval('wx.Colour'+self.Read('/display/text_color'))		# color display of text (Title, AUthor, etc..) in textCtrl-1
			if self.HasEntry('/display/ref_editor'): BIB.EDIT = eval( self.Read('/display/ref_editor')	)	# ReferenceEditor
		if self.HasGroup('search'):
			if self.HasEntry('/search/fields'): BIB.BIB_SEARCH_FIELDS= eval(self.Read('/search/fields'))
		if self.HasGroup('window'):
			BIB.WX = self.ReadInt('/window/x',BIB.WX)
			BIB.WY = self.ReadInt('/window/y',BIB.WY)
			BIB.WIDTH = self.ReadInt('/window/width',BIB.WIDTH)
			BIB.HEIGHT = self.ReadInt('/window/height',BIB.HEIGHT)
			BIB.SASH_LIST = self.ReadInt('/window/sash_list',BIB.SASH_LIST)
			BIB.SASH_KEYTREE = self.ReadInt('/window/sash_keytree',BIB.SASH_KEYTREE)
			BIB.REF_Ed_WIDTH = self.ReadInt('/window/ref_width',BIB.REF_Ed_WIDTH)	# RefEditor width
			BIB.REF_Ed_X = self.ReadInt('/window/ref_x',BIB.REF_Ed_X)				# x pos
			BIB.REF_Ed_Y = self.ReadInt('/window/ref_y',BIB.REF_Ed_Y)				# y pos
			BIB.IMPORT_WIDTH = self.ReadInt('/window/import_width',BIB.IMPORT_WIDTH)	# text Import window
			BIB.IMPORT_HEIGHT = self.ReadInt('/window/import_height',BIB.IMPORT_HEIGHT)# text Import width
			BIB.IMPORT_X = self.ReadInt('/window/import_x',BIB.IMPORT_X)				# x pos
			BIB.IMPORT_Y = self.ReadInt('/window/import_y',BIB.IMPORT_Y)				# y pos
			BIB.IMPORT_FORMAT = self.ReadInt('/window/import_format',BIB.IMPORT_FORMAT)	# import format
			BIB.SEARCH_WIDTH = self.ReadInt('/window/search_width',BIB.SEARCH_WIDTH)	# text Import window
			BIB.SEARCH_HEIGHT = self.ReadInt('/window/search_height',BIB.SEARCH_HEIGHT)# text Import width
			BIB.SEARCH_X = self.ReadInt('/window/search_x',BIB.SEARCH_X)				# x pos
			BIB.SEARCH_Y = self.ReadInt('/window/search_y',BIB.SEARCH_Y)				# y pos
			BIB.PUBMED_WIDTH = self.ReadInt('/window/pubmed_width',BIB.PUBMED_WIDTH)	# text Import window
			BIB.PUBMED_HEIGHT = self.ReadInt('/window/pubmed_height',BIB.PUBMED_HEIGHT)# text Import width
			BIB.PUBMED_X = self.ReadInt('/window/pubmed_x',BIB.PUBMED_X)				# x pos
			BIB.PUBMED_Y = self.ReadInt('/window/pubmed_y',BIB.PUBMED_Y)				# y pos
			BIB.PUBMED_NB = self.ReadInt('/window/pubmed_nb',BIB.PUBMED_NB)			# number of pubmed record to import
		if self.HasGroup('OOo'):	# OOo settings
			BIB.OO_CON_TYPE = self.ReadInt('/OOo/con_type',BIB.OO_CON_TYPE)
			BIB.OO_HOST = self.Read('/OOo/host',BIB.OO_HOST)
			BIB.OO_PORT = self.ReadInt('/OOo/port',BIB.OO_PORT)
			BIB.OO_PIPE = self.Read('/OOo/pipe',BIB.OO_PIPE)
			BIB.OO_HILIGHT = self.ReadBool('/OOo/hilight',BIB.OO_HILIGHT)
			BIB.OO_AUTO_UPDATE = self.ReadBool('/OOo/auto_update',BIB.OO_AUTO_UPDATE)
			BIB.OO_CREATE_BIB = self.ReadBool('/OOo/create_bib',BIB.OO_CREATE_BIB)
		if self.HasGroup('printer'):	# printer settings
			if self.HasEntry('/printer/format'): BIB.PRINTER_FORMAT = eval(self.Read('/printer/format'))
			if self.HasEntry('/printer/colors'): BIB.PRINTER_COLORS = eval(self.Read('/printer/colors'))
			if self.HasEntry('/printer/style'): BIB.PRINTER_STYLE = eval(self.Read('/printer/style'))
			BIB.PRINTER_USE_OOo_FORMAT = self.ReadBool('/printer/use_ooo_format',BIB.PRINTER_USE_OOo_FORMAT)
		if self.HasGroup('shortcuts'):	# shortcuts settings
			if self.HasEntry('/shortcuts/shortcuts'): BIB.SHORTCUTS = eval(self.Read('/shortcuts/shortcuts'))
			if self.HasEntry('/shortcuts/shortfile'): BIB.SHORTFILE = eval(self.Read('/shortcuts/shortfile'))
		if self.HasGroup('path'):	# path settings
			BIB.FILES = self.Read('/path/files',BIB.FILES)

	def writeConfig(self,dbSave=True):
		"""Write config file. If dbSave=True save db config
		We use the current values in BIB module """
		# Now we save only when it is modified between BIB and BIBbase
		# and we delete the keys not needed any more
		# We put bDeleteGroupIfEmpty=False in DeleteEntry
		# because wx.2.4 crash when the category becomes empty
		# it is ok under wx.2.5 to put True
		#
		self.SetPath('/general')
		if BIBbase.WP != BIB.WP:
			self.Write('wp',BIB.WP)		# type of word processor OOo / mswDoc
		elif self.HasEntry('wp'):
			self.DeleteEntry('wp',False)
		#
		self.SetPath('/display')
		if BIBbase.LIST_DISPLAY != BIB.LIST_DISPLAY:
			self.Write('fields',repr(BIB.LIST_DISPLAY))		# Fields displayed in reflist wx.ListCtrl
		else:
			if self.HasEntry('fields'): self.DeleteEntry('fields',False)
		#
		if BIBbase.LIST_ORDER != BIB.LIST_ORDER:
			self.Write('order',BIB.LIST_ORDER)				# Display order in reflist
		else:
			if self.HasEntry('order'): self.DeleteEntry('order',False)
		#
		if BIBbase.LIST_HOW != BIB.LIST_HOW:
			self.Write('how',BIB.LIST_HOW)					# how we order in reflist 'ASC'|'DESC'
		else:
			if self.HasEntry('how'): self.DeleteEntry('how',False)
		#
		if BIBbase.LIST_COL_SIZE != BIB.LIST_COL_SIZE:
			self.Write('col_size', repr(BIB.LIST_COL_SIZE) )	# tuple of column sizes
		else:
			if self.HasEntry('col_size'): self.DeleteEntry('col_size',False)
		#
		if BIBbase.DISPLAY_FORMAT != BIB.DISPLAY_FORMAT:
			self.WriteInt('display_format',BIB.DISPLAY_FORMAT)		# True if we use HTML format to display ref in the bottom-right panel
		else:
			if self.HasEntry('display_format'): self.DeleteEntry('display_format',False)
		#
		if BIBbase.KEY_COLOR != BIB.KEY_COLOR:
			self.Write('key_color',repr(BIB.KEY_COLOR.Get()))		# color display of keys (Title, AUthor, etc..) in textCtrl-1
		else:
			if self.HasEntry('key_color'): self.DeleteEntry('key_color',False)
		#
		if BIBbase.TEXT_COLOR != BIB.TEXT_COLOR:
			self.Write('text_color',repr(BIB.TEXT_COLOR.Get()))	# color display of text (Title, AUthor, etc..) in textCtrl-1
		else:
			if self.HasEntry('text_color'): self.DeleteEntry('text_color',False)
		#
		if BIBbase.EDIT != BIB.EDIT:
			self.Write('ref_editor',repr(BIB.EDIT))			# ReferenceEditor + Reference display in panel bottom-right of Main window
		else:
			if self.HasEntry('ref_editor'): self.DeleteEntry('ref_editor',False)
		#
		self.SetPath('/search')
		if BIBbase.BIB_SEARCH_FIELDS != BIB.BIB_SEARCH_FIELDS:
			self.Write('fields',repr(BIB.BIB_SEARCH_FIELDS))
		else:
			if self.HasEntry('fields'): self.DeleteEntry('fields',False)
		#
		self.SetPath('/db')
		if BIBbase.DB_STARTUP != BIB.DB_STARTUP:
			self.WriteInt('last_db',BIB.DB_STARTUP)
		else:
			if self.HasEntry('last_db'): self.DeleteEntry('last_db',False)
		#
		if BIBbase.STORE_PASSWD != BIB.STORE_PASSWD:
			self.WriteBool('store_passwd',BIB.STORE_PASSWD)
		else:
			if self.HasEntry('store_passwd'): self.DeleteEntry('store_passwd',False)
		#
		if BIBbase.DB_TYPE != BIB.DB_TYPE:
			self.Write('type',BIB.DB_TYPE)
		else:
			if self.HasEntry('type'): self.DeleteEntry('type',False)
		#
		if BIBbase.DUPLICATES_TEST != BIB.DUPLICATES_TEST:
			self.WriteBool('duplicates_test',BIB.DUPLICATES_TEST)
		else:
			if self.HasEntry('duplicates_test'): self.DeleteEntry('duplicates_test',False)
		#
		if BIBbase.DUPLICATES != BIB.DUPLICATES:
			self.Write('duplicates',repr(BIB.DUPLICATES))
		else:
			if self.HasEntry('duplicates'): self.DeleteEntry('duplicates',False)
		#
		if BIBbase.DUPLICATES_CASE != BIB.DUPLICATES_CASE:
			self.WriteBool('duplicates_case',BIB.DUPLICATES_CASE)
		else:
			if self.HasEntry('duplicates_case'): self.DeleteEntry('duplicates_case',False)
		#
		if BIBbase.DUPLICATES_KEEP_OLD != BIB.DUPLICATES_KEEP_OLD:
			self.WriteBool('duplicates_keep_old',BIB.DUPLICATES_KEEP_OLD)
		else:
			if self.HasEntry('duplicates_keep_old'): self.DeleteEntry('duplicates_keep_old',False)
		#
		if dbSave:					# we save db
			if BIB.DB_TYPE == 'MySQL':
				self.SetPath('/mysql')
				if BIBbase.DB_NAME != BIB.DB_NAME:
					self.Write('name',BIB.DB_NAME)
				else:
					if self.HasEntry('name'): self.DeleteEntry('name',False)
				#
				if BIBbase.USER != BIB.USER:
					self.Write('user',BIB.USER)
				else:
					if self.HasEntry('user'): self.DeleteEntry('user',False)
				#
				if BIBbase.PASSWORD != BIB.PASSWORD:
					self.Write('passwd',BIB.PASSWORD)	# PASSWORD='' if not to be stored
				else:
					if self.HasEntry('passwd'): self.DeleteEntry('passwd',False)
				#
				if BIBbase.HOST != BIB.HOST:
					self.Write('host',BIB.HOST)
				else:
					if self.HasEntry('host'): self.DeleteEntry('host',False)
				#
				if BIBbase.PORT != BIB.PORT:
					self.WriteInt('port',BIB.PORT)
				else:
					if self.HasEntry('port'): self.DeleteEntry('port',False)
				#
				if BIBbase.SOCKET != BIB.SOCKET:
					self.Write('socket',BIB.SOCKET)
				else:
					if self.HasEntry('socket'): self.DeleteEntry('socket',False)
				#
			elif BIB.DB_TYPE == 'SQLite':
				self.SetPath('/sqlite')
				if BIBbase.SQLiteFile != BIB.SQLiteFile:
					self.Write('file',BIB.SQLiteFile)
				else:
					if self.HasEntry('file'): self.DeleteEntry('file',False)
				#
				if BIBbase.SQLiteUSER != BIB.SQLiteUSER:
					self.Write('user',BIB.SQLiteUSER)
				else:
					if self.HasEntry('user'): self.DeleteEntry('user',False)
		#
		self.SetPath('/window')
		if BIBbase.WX != BIB.WX:
			self.WriteInt('x',BIB.WX)
		else:
			if self.HasEntry('x'): self.DeleteEntry('x',False)
		#
		if BIBbase.WY != BIB.WY:
			self.WriteInt('y',BIB.WY)
		else:
			if self.HasEntry('y'): self.DeleteEntry('y',False)
		#
		if BIBbase.WIDTH != BIB.WIDTH:
			self.WriteInt('width',BIB.WIDTH)
		else:
			if self.HasEntry('width'): self.DeleteEntry('width',False)
		#
		if BIBbase.HEIGHT != BIB.HEIGHT:
			self.WriteInt('height',BIB.HEIGHT)
		else:
			if self.HasEntry('height'): self.DeleteEntry('height',False)
		#
		if BIBbase.SASH_LIST != BIB.SASH_LIST:
			self.WriteInt('sash_list',BIB.SASH_LIST)
		else:
			if self.HasEntry('sash_list'): self.DeleteEntry('sash_list',False)
		#
		if BIBbase.SASH_KEYTREE != BIB.SASH_KEYTREE:
			self.WriteInt('sash_keytree',BIB.SASH_KEYTREE)
		else:
			if self.HasEntry('sash_keytree'): self.DeleteEntry('sash_keytree',False)
		#
		if BIBbase.REF_Ed_WIDTH != BIB.REF_Ed_WIDTH:
			self.WriteInt('ref_width',BIB.REF_Ed_WIDTH)
		else:
			if self.HasEntry('ref_width'): self.DeleteEntry('ref_width',False)
		#
		if BIBbase.REF_Ed_X != BIB.REF_Ed_X:
			self.WriteInt('ref_x',BIB.REF_Ed_X)
		else:
			if self.HasEntry('ref_x'): self.DeleteEntry('ref_x',False)
		#
		if BIBbase.REF_Ed_Y != BIB.REF_Ed_Y:
			self.WriteInt('ref_y',BIB.REF_Ed_Y)
		else:
			if self.HasEntry('ref_y'): self.DeleteEntry('ref_y',False)
		#
		if BIBbase.IMPORT_WIDTH != BIB.IMPORT_WIDTH:
			self.WriteInt('import_width',BIB.IMPORT_WIDTH)
		else:
			if self.HasEntry('import_width'): self.DeleteEntry('import_width',False)
		#
		if BIBbase.IMPORT_HEIGHT != BIB.IMPORT_HEIGHT:
			self.WriteInt('import_height',BIB.IMPORT_HEIGHT)
		else:
			if self.HasEntry('import_height'): self.DeleteEntry('import_height',False)
		#
		if BIBbase.IMPORT_X != BIB.IMPORT_X:
			self.WriteInt('import_x',BIB.IMPORT_X)
		else:
			if self.HasEntry('import_x'): self.DeleteEntry('import_x',False)
		#
		if BIBbase.IMPORT_Y != BIB.IMPORT_Y:
			self.WriteInt('import_y',BIB.IMPORT_Y)
		else:
			if self.HasEntry('import_y'): self.DeleteEntry('import_y',False)
		#
		if BIBbase.IMPORT_FORMAT != BIB.IMPORT_FORMAT:
			self.WriteInt('import_format',BIB.IMPORT_FORMAT)
		else:
			if self.HasEntry('import_format'): self.DeleteEntry('import_format',False)
		#
		if BIBbase.SEARCH_WIDTH != BIB.SEARCH_WIDTH:
			self.WriteInt('search_width',BIB.IMPORT_WIDTH)
		else:
			if self.HasEntry('search_width'): self.DeleteEntry('search_width',False)
		#
		if BIBbase.SEARCH_HEIGHT != BIB.SEARCH_HEIGHT:
			self.WriteInt('search_height',BIB.SEARCH_HEIGHT)
		else:
			if self.HasEntry('search_height'): self.DeleteEntry('search_height',False)
		#
		if BIBbase.SEARCH_X != BIB.SEARCH_X:
			self.WriteInt('search_x',BIB.SEARCH_X)
		else:
			if self.HasEntry('search_x'): self.DeleteEntry('search_x',False)
		#
		if BIBbase.SEARCH_Y != BIB.SEARCH_Y:
			self.WriteInt('search_y',BIB.SEARCH_Y)
		else:
			if self.HasEntry('search_y'): self.DeleteEntry('search_y',False)
		#
		if BIBbase.SEARCH_WIDTH != BIB.PUBMED_WIDTH:
			self.WriteInt('pubmed_width',BIB.IMPORT_WIDTH)
		else:
			if self.HasEntry('pubmed_width'): self.DeleteEntry('pubmed_width',False)
		#
		if BIBbase.PUBMED_HEIGHT != BIB.PUBMED_HEIGHT:
			self.WriteInt('pubmed_height',BIB.PUBMED_HEIGHT)
		else:
			if self.HasEntry('pubmed_height'): self.DeleteEntry('pubmed_height',False)
		#
		if BIBbase.PUBMED_X != BIB.PUBMED_X:
			self.WriteInt('pubmed_x',BIB.PUBMED_X)
		else:
			if self.HasEntry('pubmed_x'): self.DeleteEntry('pubmed_x',False)
		#
		if BIBbase.PUBMED_Y != BIB.PUBMED_Y:
			self.WriteInt('pubmed_y',BIB.PUBMED_Y)
		else:
			if self.HasEntry('pubmed_y'): self.DeleteEntry('pubmed_y',False)
		#
		if BIBbase.PUBMED_NB != BIB.PUBMED_NB:
			self.WriteInt('pubmed_nb',BIB.PUBMED_NB)
		else:
			if self.HasEntry('pubmed_nb'): self.DeleteEntry('pubmed_nb',False)
		#
		self.SetPath('/OOo')		# OOo settings
		if BIBbase.OO_CON_TYPE != BIB.OO_CON_TYPE:
			self.WriteInt('con_type',BIB.OO_CON_TYPE)
		else:
			if self.HasEntry('con_type'): self.DeleteEntry('con_type',False)
		#
		if BIBbase.OO_HOST != BIB.OO_HOST:
			self.Write('host',BIB.OO_HOST)
		else:
			if self.HasEntry('host'): self.DeleteEntry('host',False)
		#
		if BIBbase.OO_PORT != BIB.OO_PORT:
			self.WriteInt('port',BIB.OO_PORT)
		else:
			if self.HasEntry('port'): self.DeleteEntry('port',False)
		#
		if BIBbase.OO_PIPE != BIB.OO_PIPE:
			self.Write('pipe',BIB.OO_PIPE)
		else:
			if self.HasEntry('pipe'): self.DeleteEntry('pipe',False)
		#
		if BIBbase.OO_HILIGHT != BIB.OO_HILIGHT:
			self.WriteBool('hilight',BIB.OO_HILIGHT)
		else:
			if self.HasEntry('hilight'): self.DeleteEntry('hilight',False)
		#
		if BIBbase.OO_AUTO_UPDATE != BIB.OO_AUTO_UPDATE:
			self.WriteBool('auto_update',BIB.OO_AUTO_UPDATE)
		else:
			if self.HasEntry('auto_update'): self.DeleteEntry('auto_update',False)
		#
		if BIBbase.OO_CREATE_BIB != BIB.OO_CREATE_BIB:
			self.WriteBool('create_bib',BIB.OO_CREATE_BIB)
		else:
			if self.HasEntry('create_bib'): self.DeleteEntry('create_bib',False)
		#
		self.SetPath('/printer')		# printer settings
		if BIBbase.PRINTER_FORMAT != BIB.PRINTER_FORMAT:
			self.Write('format',repr(BIB.PRINTER_FORMAT))
		else:
			if self.HasEntry('format'): self.DeleteEntry('format',False)
		#
		if BIBbase.PRINTER_COLORS != BIB.PRINTER_COLORS:
			self.Write('colors',repr(BIB.PRINTER_COLORS))
		else:
			if self.HasEntry('colors'): self.DeleteEntry('colors',False)
		#
		if BIBbase.PRINTER_STYLE != BIB.PRINTER_STYLE:
			self.Write('style',repr(BIB.PRINTER_STYLE))
		else:
			if self.HasEntry('style'): self.DeleteEntry('style',False)
		#
		if BIBbase.PRINTER_USE_OOo_FORMAT != BIB.PRINTER_USE_OOo_FORMAT:
			self.WriteBool('use_ooo_format',BIB.PRINTER_USE_OOo_FORMAT)
		else:
			if self.HasEntry('use_ooo_format'): self.DeleteEntry('use_ooo_format',False)
		#
		self.SetPath('/shortcuts')	# shortcuts settings
		if BIBbase.SHORTCUTS != BIB.SHORTCUTS:
			self.Write('shortcuts',repr(BIB.SHORTCUTS))
		else:
			if self.HasEntry('shortcuts'): self.DeleteEntry('shortcuts',False)
		#
		if BIBbase.SHORTFILE != BIB.SHORTFILE:
			self.Write('shortfile',repr(BIB.SHORTFILE))
		else:
			if self.HasEntry('fields'): self.DeleteEntry('shortfile',False)
		#
		self.SetPath('/path')	# path settings
		if BIBbase.FILES != BIB.FILES:
			self.Write('files',BIB.FILES)
		else:
			if self.HasEntry('files'): self.DeleteEntry('files',False)
		#
		self.Flush()	# save the config changes

#---------------------- Shortcuts ----------------------------------------------
	def readShortcut(self,n):
		"""Load shortcut number n"""
		try:
			f = file(BIB.SHORTFILE[n],'r')
			short = cPickle.load(f)		# we unpickle the file then check that it is a list of strings
			f.close()
			if type(short) != list: raise TypeError('notalist')
			else:
				for i in short:
					if type(i) not in (unicode,str): raise TypeError('notastring')
		except:
			short = []
		BIB.SHORT[BIB.SHORTCUTS[n]] = short

	def writeShortcuts(self):
		for index in xrange(len(BIB.SHORTCUTS)):
			if BIB.SHORT.has_key(BIB.SHORTCUTS[index]):
				f = file(BIB.SHORTFILE[index],'w')
				cPickle.dump( BIB.SHORT[BIB.SHORTCUTS[index]] , f)
				f.close()

#---------------------- OOo styles ---------------------------------------------
	def writeStyle(self,filename):
		"""Write the filename of the user Style
		key = basename, value = path+basename"""
		self.SetPath('/styles')
		self.Write(os.path.basename(filename),filename)

	def deleteStyle(self,filename):
		self.SetPath('/styles')
		self.DeleteEntry(os.path.basename(filename),False)

	def readStyles(self):
		"""Return a tuple of the filenames of user styles."""
		if self.HasGroup('/styles'):
			self.SetPath('/styles')
			styles=[]
			flag,s,cookie = self.GetFirstEntry()
			while flag:
				styles.append(self.Read(s))
				flag,s,cookie = self.GetNextEntry(cookie)
			return tuple(styles)
		else:
			return ()

	def writeCurrentStyle(self,style):
		"""Save the Current style filename under key style/current"""
		self.Write('/style/current',style)

	def getCurrentStyle(self):
		"""Return the Current style"""
		return self.Read('/style/current')

# --------------------------- FirstStart ---------------------------------------
	def firstStartDone(self):
		"""Set to False after first start"""
		self.WriteBool('/general/firststart',False)
		self.Write('/general/config_version',BIB.BIBUS_VERSION)
		self.Flush()

	def userDefined(self,dbType):
		"""Return true if the username was already defined in the config"""
		return self.HasEntry('/%s/user'%dbType)

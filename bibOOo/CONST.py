# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of bibOOo, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#

# we must redefine BIB_FIELDS because of a spelling error in OOo. cf OOo bug #28271
# we can test when OOo is running by using __testFields below but it is not needed for the moment since
# all the known OOo versions contain this bug.

from BIB import SOURCEDIR,NAME_FIELD,FIELD_NAME,NAME_TYPE,TYPE_NAME,SEP
from BIB import BIB_TYPE,BIBLIOGRAPHIC_TYPE
from BIB import JOURNAL

BIB_FIELDS = ('Identifier', 'BibliographicType', 'Address', 'Annote', 'Author',
		 'Booktitle', 'Chapter', 'Edition', 'Editor', 'Howpublished', 'Institution',
		 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher',
		 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1',
		 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN')
BIBLIOGRAPHIC_FIELDS = {}
for i in range(len(BIB_FIELDS)):
	BIBLIOGRAPHIC_FIELDS[BIB_FIELDS[i]] = i
#{'Identifier':0, 'BibliographicType':1,...'ISBN':30}
OO_BIB_FIELDS = ('Identifier', 'BibiliographicType', 'Address', 'Annote', 'Author',
		 'Booktitle', 'Chapter', 'Edition', 'Editor', 'Howpublished', 'Institution',
		 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher',
		 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1',
		 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN')
OO_BIBLIOGRAPHIC_FIELDS = {}
for i in range(len(OO_BIB_FIELDS)):
	OO_BIBLIOGRAPHIC_FIELDS[OO_BIB_FIELDS[i]] = i

# constants used by styles
# CharStyle names
bibOOo_cit_baseCharStyleName = 'bibOOo_cit_base'	# base style for citations
bibOOo_index_baseCharStyleName = 'bibOOo_index_base'			# base style for bibliography index
#
# styles = OR of following values
bibOOo_regular = 0
bibOOo_italic = 1
bibOOo_bold = 2
bibOOo_caps = 4
bibOOo_smallcaps = 8
bibOOo_underline = 16
#
bibOOo_base = -1	# if style == -1 => style = base_style
# exposant
bibOOo_normal = 0
bibOOo_exposant = 1
bibOOo_indice = -1
#
# strings to be translated in your program
# by overwriting them
# import CONST
# CONST.msg1 = _("Making a copy of the current document ...") etc...
# in bibOOoBase
msg1 = _("Making a copy of the current document ...")
msg2 = _("Creating styles ...")
msg3 = _("Updating references ...")
msg4 = _("Updating index ...")
msg5 = _("Deleting old index and citations ...")
msg6 = _("Done")
msg7 = _("Inserting citations in text")
# in bibOOoPlus
msg8 = _("Formating citations ...")
msg9 = _("Looking for duplicates ...")
msg10 = _("Solving duplicates ... (%s series of duplicates)")
msg11 = _("Fusing citations ...")
msg12 = _("Inserting citations in text (%s ranges)")





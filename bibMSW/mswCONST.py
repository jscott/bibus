# Copyright 2006 Mounir Errami. m.errami@gmail.com
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#File create by Mounir Errami. Don't change these constants unless you have excellent reasons to
#If a constant is change, this can certainly affect the way Bibus will interact with MSWord.
#each me_MSWBIBUS_... constant must be unique.
me_wdFieldAddin=0x51    #Don't change From microsoft word FieldType.wdFieldAddin; OfficeXP
me_wdFieldEmpty=-1    #Don't change For some reason can't access constants.wdFieldAddin; OfficeXP
me_wdWindowStateMaximize         =0x1        #  don't change (from enum WdWindowState); OfficeXP
me_wdWindowStateMinimize         =0x2        #  don'tchange (from enum WdWindowState); OfficeXP
me_wdWindowStateNormal           =0x0        # don't change (from enum WdWindowState); OfficeXP
me_wdPageBreak           	=0x7
me_MSWBIBUS_BIBLIO_TAG = "bibusMeBiblio"#defined by Mounir Errami
me_MSWBIBUS_CITE_TAG = "bibusMeCitation"#defined by Mounir Errami
me_MSWBIBUS_ADDIN_REF_TAG = "ADDIN BibusRef" #Has to contain ADDIN in it
me_MSWBIBUS_ADDIN_BIB_TAG = "ADDIN BibusBiblio" #Has to contain ADDIN in it
me_MSWBIBUS_BIBLIO_IDENTIFIER = "textPosition" #anything except  BibiliographicType ('ARTICLE', etc...) or Bibliographic fields ('Author' etc...)
me_MSWBIBUS_BIBLIO_NEWENTRY = "newEntry" #anything except  BibiliographicType ('ARTICLE', etc...) or Bibliographic fields ('Author' etc...)
me_MSWBIBUS_CITE_RANGES = "CitationRanges" #anything except  BibiliographicType ('ARTICLE', etc...) or Bibliographic fields ('Author' etc...)
me_MSWBIBUS_CITE_DUPLICATES = "DuplicateInfo" #anything except  BibiliographicType ('ARTICLE', etc...) or Bibliographic fields ('Author' etc...)
me_MSWBIBUS_CITE_RANGE_SEP =';' #better not to change
me_MSWBIBUS_CITE_RANGE_SEP_IN = '*' #better not to change. Can be anything except me_MSWBIBUS_CITE_RANGE_SEP, letters, numbers, '-' 

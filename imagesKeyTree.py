# Copyright 2004,2008 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
#----------------------------------------------------------------------
import wx

def getImageList():
	images = wx.ImageList(16, 16)
	ap=wx.ArtProvider()
	images.Add(ap.GetBitmap(wx.ART_DELETE,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_FILE_OPEN,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_FIND,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_NORMAL_FILE,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_TICK_MARK,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_GO_HOME,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_QUESTION,size=(16,16)))
	images.Add(ap.GetBitmap(wx.ART_LIST_VIEW,size=(16,16)))
	return images


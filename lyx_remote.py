#!/usr/bin/env python
# lyx-remote         -*- coding: iso-8859-1 -*-
# Copyright (c) 2005 G�nter Milde
# Released under the terms of the GNU General Public License (ver. 2 or later)

"""Open a file in a running LyX or start LyX with the file

   Needs the LyX-Server running (Set Edit>Preferences>Paths>Server-pipe)
"""

# import sys, os
import logging
import os.path

# Customizable values (defaults shown)
# ------------------------------------

import LyX.constants, BIB
LyX.constants.SERVERPIPE = os.path.abspath(os.path.expanduser(os.path.expandvars(BIB.LYX_PIPE)))
# default timeout for reading the outpipe (in ms)
# constants.LYXSERVER_POLL_TIMEOUT = 1000 # default 1 s
# constants.LOG_LEVEL = logging.DEBUG # default INFO
LyX.constants.LOG_LEVEL = logging.WARN # default INFO

# now import with the set defaults
import LyX.lyxclient

class bibusLyx(object):
    def __init__(self):        
        self.client = LyX.lyxclient.lyx_remote()
        self.client.open()

    def insert(self,identifier):
        self.client.write_lfun("citation-insert",identifier)

def resetPipe(self):
    reload(LyX.constants)
    LyX.constants.SERVERPIPE = os.path.abspath(os.path.expanduser(os.path.expandvars(BIB.LYX_PIPE)))
    reload(LyX.lyxclient)


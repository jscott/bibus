#!/usr/bin/env python
#
# This is an utility file that parse the PubMed list of journal
# <http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=journals>
# to use them in bibus
#
#	--------------------------------------------------------
#	JrId: 1392
#	JournalTitle: The Journal of molecular and cellular immunology : JMCI
#	MedAbbr: J Mol Cell Immunol
#	ISSN: 0724-6803
#	ESSN: 
#	IsoAbbr: J. Mol. Cell. Immunol.
#	NlmId: 8405005
#	--------------------------------------------------------
#	JrId: 1394
#	JournalTitle: Journal of molecular endocrinology
#	MedAbbr: J Mol Endocrinol
#	ISSN: 0952-5041
#	ESSN: 
#	IsoAbbr: J. Mol. Endocrinol.
#	NlmId: 8902617
#	--------------------------------------------------------
#	
# We get for bibus
# JournalTitle, MedAbbr, IsoAbbr

# usage:
# >> parsePubMedJ.py J_Entrez > journals.csv
# or
# cat cat J_Entrez | parsePubMedJ.py > journals.csv

import fileinput,csv,sys
from title_case import title_case

output = csv.writer(sys.stdout)
journal = {}

tmpj = {}
for line in fileinput.input():
	ls = line.split(':',1)
	if len(ls) == 2:
		if ls[0] == 'JrId' and tmpj and (tmpj['MedAbbr'] or tmpj['IsoAbbr']):
			if tmpj['MedAbbr']=="":
					tmpj['MedAbbr']=tmpj['IsoAbbr'].replace('.','')
			output.writerow( (tmpj['MedAbbr'],tmpj['IsoAbbr'],title_case(tmpj['JournalTitle'])) )
			tmpj.clear()
		tmpj[ls[0]] = ls[1].strip()


# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#

from Pref_Duplicates_Base import *
import BIB

class Pref_Duplicates(Pref_Duplicates_Base):
	def __init__(self,parent,id,(checkDuplicates,duplicates,case,solve)):
		self.checkDuplicates = checkDuplicates
		self.duplicates = duplicates
		self.case = case
		self.solve = solve
		Pref_Duplicates_Base.__init__(self,parent,id)
		wx.EVT_CHECKBOX(self,self.checkbox_checkDuplicates.GetId(),self.onCheckDuplicates)

	def _Pref_Duplicates_Base__set_properties(self):
		self.checkbox_case.SetValue(self.case)
		self.list_fields.Set([BIB.NAME_FIELD[f] for f in BIB.BIB_FIELDS[2:]])	# choosing Id or Identifier does not make sens
		for field in self.duplicates:
			self.list_fields.SetStringSelection(BIB.NAME_FIELD[field])
		self.checkbox_checkDuplicates.SetValue(self.checkDuplicates)
		self.radio_box_duplicateSolve.SetSelection( int(not self.solve) )
		self.onCheckDuplicates(None)

	def onCheckDuplicates(self,event):
		self.panel_2.Enable(self.checkbox_checkDuplicates.GetValue())

	def getSettings(self):
		# to get the list of selected fields we must activate the control
		self.panel_2.Enable(True)
		checkDuplicates = bool(self.checkbox_checkDuplicates.GetValue())
		# having self.checkDuplicates == True and self.duplicates = () does not make sens
		# in that case we force checkDuplicates to False
		if self.list_fields.GetSelections() == (): checkDuplicates = False
		return  checkDuplicates, tuple( [BIB.BIB_FIELDS[i+2] for i in self.list_fields.GetSelections()] ), bool(self.checkbox_case.GetValue()), not bool( self.radio_box_duplicateSolve.GetSelection() )


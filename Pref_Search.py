#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
# generated by wxGlade 0.4.1 on Sun Dec 03 14:48:19 2006

import wx
import BIB

class Pref_Search_Base(wx.Panel):
    def __init__(self, *args, **kwds):
        # begin wxGlade: Pref_Search_Base.__init__
        kwds["style"] = wx.TAB_TRAVERSAL
        wx.Panel.__init__(self, *args, **kwds)
        self.label_1 = wx.StaticText(self, -1, _("Main fields searched"))
        self.prefSearch = wx.ListBox(self, -1, choices=[], style=wx.LB_MULTIPLE)

        self.__set_properties()
        self.__do_layout()
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: Pref_Search_Base.__set_properties
        pass
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: Pref_Search_Base.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_1.Add(self.label_1, 0, wx.LEFT|wx.TOP|wx.ADJUST_MINSIZE, 5)
        sizer_1.Add(self.prefSearch, 1, wx.ALL|wx.EXPAND|wx.ADJUST_MINSIZE, 5)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        sizer_1.SetSizeHints(self)
        # end wxGlade

# end of class Pref_Search_Base


class Pref_Search(Pref_Search_Base):
    def __init__(self, parent, id, BIB_SEARCH_FIELDS):
        self.BIB_SEARCH_FIELDS = BIB_SEARCH_FIELDS
        Pref_Search_Base.__init__(self,parent,id)

    def _Pref_Search_Base__set_properties(self):
        Pref_Search_Base._Pref_Search_Base__set_properties(self)
        self.prefSearch.AppendItems([BIB.NAME_FIELD[field] for field in BIB.BIB_FIELDS])
        for tmpstr in self.BIB_SEARCH_FIELDS:
            self.prefSearch.SetStringSelection(BIB.NAME_FIELD[tmpstr],True)
            
    def getSettings(self):
        #
        return tuple([BIB.FIELD_NAME[self.prefSearch.GetString(i)] for i in self.prefSearch.GetSelections()])
# end of class Pref_Search



# Author: Jan Beyer <jan@beathovn.de>
# Copyright 2009 Pierre Martineau <pmartino@users.sourceforge.net>
#
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#

import uno
from com.sun.star.beans import PropertyValue

def getUNOconnection():
	"""Get current UNO connection info. Returns empty string, if nothing is set.
	Returns the UNO connection URL otherwise.
	"""
	
	ctx = uno.getComponentContext()
	configProvider = ctx.ServiceManager.createInstance("com.sun.star.configuration.ConfigurationProvider")
	
	prop = PropertyValue()
	prop.Name = "nodepath"
	prop.Value = "org.openoffice.Setup/Office"
	
	OOoConfigAccess = configProvider.createInstanceWithArguments ("com.sun.star.configuration.ConfigurationAccess", (prop,) )
	
	return OOoConfigAccess.ooSetupConnectionURL
	
	
def setUNOconnection( linktype='pipe', parameters=("OOo_pipe",), activate=True):
	"""Activate OOo UNO connection according to the given arguments.
	For linktype == 'pipe', the first parameter is taken as the pipename.
	For linktipe == 'socket', there should be two parameters: (host,port).
	If activate == False, the UNO connection URL is blanked.
	Returns the new UNO connection URL, if okay or 'Argument error.',
	in case of trouble with the given arguments.
	"""
	
	ctx = uno.getComponentContext()
	configProvider = ctx.ServiceManager.createInstance("com.sun.star.configuration.ConfigurationProvider")
	
	prop = PropertyValue()
	prop.Name = "nodepath"
	prop.Value = "org.openoffice.Setup/Office"
	
	nolazywrite = PropertyValue()
	nolazywrite.Name = "lazywrite"
	nolazywrite.Value = uno.Bool(0)
	
	OOoConfigAccess = configProvider.createInstanceWithArguments ("com.sun.star.configuration.ConfigurationUpdateAccess",(prop,nolazywrite) )
	
	if activate:
		if (linktype == 'pipe'):
			# The following is not really fool-proof. You should never give two
			# parameters, when linktype=='pipe'. But right now I don't know, how
			# to check for this.
			OOoConfigAccess.ooSetupConnectionURL = "pipe,name="+parameters[0]+";urp;StarOffice.ServiceManager"
			OOoConfigAccess.commitChanges()
			return OOoConfigAccess.ooSetupConnectionURL
		elif (linktype == 'socket') and (len(parameters) == 2):
			OOoConfigAccess.ooSetupConnectionURL = "socket,host="+parameters[0]+",port="+parameters[1]+";urp;StarOffice.ServiceManager"
			OOoConfigAccess.commitChanges()
			return OOoConfigAccess.ooSetupConnectionURL
		else:
			return "Argument error."
	else:
		OOoConfigAccess.ooSetupConnectionURL = ""
		OOoConfigAccess.commitChanges()
		return ""


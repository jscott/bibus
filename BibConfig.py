# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import wx
import BIBbase, BIB, os, cPickle

class BibConfig(wx.Config):
	def __init__(self,appname):
		#wx.Config.__init__(self)			# ,style=wx.CONFIG_USE_SUBDIR) usable only with wx.Python >= 2.8.2 We must do it by hand
		# we have to set the path to ~/.bibus => wx.CONFIG_USE_RELATIVE_PATH with os.chdir
		self.createConfDir( appname )
		self.setFilePath()
		wx.Config.__init__(self,appname,localFilename = "%s.conf"%appname, style = wx.CONFIG_USE_RELATIVE_PATH )
		
					
	def	createConfDir(self,app):
		"""We create all the necessary configuration Directories if needed"""
		bibusConfDir = wx.StandardPaths.Get().GetUserDataDir()
		#
		if not os.path.exists( bibusConfDir ):	# we create application config directory
			os.mkdir( bibusConfDir )
		if not os.path.exists( os.path.join( bibusConfDir, "Data") ):	# Some data, journal abbreviations, ...
			os.mkdir( os.path.join( bibusConfDir, "Data") )
		if not os.path.exists( os.path.join( bibusConfDir, "Styles") ):	# Personnal styles
			os.mkdir( os.path.join( bibusConfDir, "Styles") )
		if not os.path.exists( os.path.join( bibusConfDir, "Shortcuts") ):	# Shortcuts
			os.mkdir( os.path.join( bibusConfDir, "Shortcuts") )
		if not os.path.exists( os.path.join( bibusConfDir, "Databases") ):	# Slite databases
			os.mkdir( os.path.join( bibusConfDir, "Databases") )
		if not os.path.exists( os.path.join( bibusConfDir, "Import") ):	# personal Import filters
			os.mkdir( os.path.join( bibusConfDir, "Import") )
		if not os.path.exists( os.path.join( bibusConfDir, "Export") ):	# personal Export filters
			os.mkdir( os.path.join( bibusConfDir, "Export") )
		
	def setFilePath(self):
		# we have to set the path explicitly for wx.Python < 2.8.2
		os.chdir( wx.StandardPaths.Get().GetUserDataDir() )
		
	def getFilePath(self):
		return wx.StandardPaths.Get().GetUserDataDir()

	def readConfig(self):
		"""We set the variables in BIB module"""
		self.setFilePath()
		self.SetPath('/')
		if self.HasGroup('general'):
			BIB.FIRST_START = self.ReadBool('/general/firststart')
			BIB.CONFIG_VERSION = self.Read('/general/config_version')
			BIB.WP = self.Read('/general/wp',BIB.WP)	# word processor connection type OOo/mswDoc/LyX
			BIB.TEXT_CLIPBOARD = self.Read('/general/text_clipboard',BIB.TEXT_CLIPBOARD)
		if self.HasGroup('mysql'):
			BIB.SOCKET = self.Read('/mysql/socket',BIB.SOCKET)
			BIB.PORT = self.ReadInt('/mysql/port',BIB.PORT)
			BIB.HOST = self.Read('/mysql/host',BIB.HOST)
			BIB.USER = self.Read('/mysql/user',BIB.USER)
			BIB.PASSWORD = self.Read('/mysql/passwd',BIB.PASSWORD)
			BIB.DB_NAME = self.Read('/mysql/name',BIB.DB_NAME)
		else:
			if not self.HasEntry('/mysql/user'):
				try:
					BIB.USER = getpass.getuser()	# We try to get a default value if not defined.
				except:
					BIB.USER = ''
		if self.HasGroup('sqlite'):
			BIB.SQLiteFile = self.Read('/sqlite/file',BIB.SQLiteFile)
			BIB.SQLiteUSER = self.Read('/sqlite/user',BIB.SQLiteUSER)
		else:
			if not self.HasEntry('/sqlite/user'):
				try:
					BIB.SQLiteUSER = getpass.getuser()	# We try to get a default value if not defined.
				except:
					BIB.SQLiteUSER = ''
		if self.HasGroup('db'):
			BIB.STORE_PASSWD = self.ReadBool('/db/store_passwd',BIB.STORE_PASSWD)
			BIB.DB_STARTUP = self.ReadInt('/db/last_db',BIB.DB_STARTUP)
			BIB.DB_TYPE = self.Read('/db/type',BIB.DB_TYPE)
			BIB.DUPLICATES_TEST = self.ReadBool('/db/duplicates_test',BIB.DUPLICATES_TEST)	# True if we want to test for duplicate entries
			if self.HasEntry('/db/duplicates'): BIB.DUPLICATES = eval(self.Read('/db/duplicates'))	# fields to test
			BIB.DUPLICATES_CASE = self.ReadBool('/db/duplicates_case',BIB.DUPLICATES_CASE)	# True == comparison is Case sensitive
			BIB.DUPLICATES_KEEP_OLD = self.ReadBool('/db/duplicates_keep_old',BIB.DUPLICATES_KEEP_OLD)	# True == we keep the old ref
		if self.HasGroup('display'):
			if self.HasEntry('/display/fields'): BIB.LIST_DISPLAY = eval(self.Read('/display/fields'))	# Fields displayed in reflist wx.ListCtrl
			BIB.LIST_ORDER = self.Read('/display/order',BIB.LIST_ORDER)				# Display order in reflist
			BIB.LIST_HOW = self.Read('/display/how',BIB.LIST_HOW)	# ASC or DESC ordering
			if self.HasEntry('/display/col_size'): BIB.LIST_COL_SIZE = eval( self.Read('/display/col_size') ) # column sizes (tuple)
			if self.HasEntry('/display/display_format'): BIB.DISPLAY_FORMAT = self.ReadInt('/display/display_format',BIB.DISPLAY_FORMAT)	# select tab in bottom-right main window
			if self.HasEntry('/display/key_color'): BIB.KEY_COLOR = eval('wx.Colour'+self.Read('/display/key_color'))		# color display of keys (Title, AUthor, etc..) in textCtrl-1
			if self.HasEntry('/display/text_color'): BIB.TEXT_COLOR = eval('wx.Colour'+self.Read('/display/text_color'))		# color display of text (Title, AUthor, etc..) in textCtrl-1
			if self.HasEntry('/display/ref_editor'): BIB.EDIT = eval( self.Read('/display/ref_editor')	)	# ReferenceEditor
		if self.HasGroup('search'):
			if self.HasEntry('/search/fields'): BIB.BIB_SEARCH_FIELDS= eval(self.Read('/search/fields'))
		if self.HasGroup('window'):
			BIB.WX = self.ReadInt('/window/x',BIB.WX)
			BIB.WY = self.ReadInt('/window/y',BIB.WY)
			BIB.WIDTH = self.ReadInt('/window/width',BIB.WIDTH)
			BIB.HEIGHT = self.ReadInt('/window/height',BIB.HEIGHT)
			BIB.SASH_LIST = self.ReadInt('/window/sash_list',BIB.SASH_LIST)
			BIB.SASH_KEYTREE = self.ReadInt('/window/sash_keytree',BIB.SASH_KEYTREE)
			BIB.REF_Ed_WIDTH = self.ReadInt('/window/ref_width',BIB.REF_Ed_WIDTH)	# RefEditor width
			BIB.REF_Ed_X = self.ReadInt('/window/ref_x',BIB.REF_Ed_X)				# x pos
			BIB.REF_Ed_Y = self.ReadInt('/window/ref_y',BIB.REF_Ed_Y)				# y pos
			BIB.IMPORT_WIDTH = self.ReadInt('/window/import_width',BIB.IMPORT_WIDTH)	# text Import window
			BIB.IMPORT_HEIGHT = self.ReadInt('/window/import_height',BIB.IMPORT_HEIGHT)# text Import width
			BIB.IMPORT_X = self.ReadInt('/window/import_x',BIB.IMPORT_X)				# x pos
			BIB.IMPORT_Y = self.ReadInt('/window/import_y',BIB.IMPORT_Y)				# y pos
			BIB.IMPORT_FORMAT = self.ReadInt('/window/import_format',BIB.IMPORT_FORMAT)	# import format
			BIB.SEARCH_WIDTH = self.ReadInt('/window/search_width',BIB.SEARCH_WIDTH)	# text Import window
			BIB.SEARCH_HEIGHT = self.ReadInt('/window/search_height',BIB.SEARCH_HEIGHT)# text Import width
			BIB.SEARCH_X = self.ReadInt('/window/search_x',BIB.SEARCH_X)				# x pos
			BIB.SEARCH_Y = self.ReadInt('/window/search_y',BIB.SEARCH_Y)				# y pos
			BIB.PUBMED_WIDTH = self.ReadInt('/window/pubmed_width',BIB.PUBMED_WIDTH)	# text Import window
			BIB.PUBMED_HEIGHT = self.ReadInt('/window/pubmed_height',BIB.PUBMED_HEIGHT)# text Import width
			BIB.PUBMED_X = self.ReadInt('/window/pubmed_x',BIB.PUBMED_X)				# x pos
			BIB.PUBMED_Y = self.ReadInt('/window/pubmed_y',BIB.PUBMED_Y)				# y pos
			BIB.PUBMED_NB = self.ReadInt('/window/pubmed_nb',BIB.PUBMED_NB)			# number of pubmed record to import
		if self.HasGroup('OOo'):	# OOo settings
			BIB.OO_CON_TYPE = self.ReadInt('/OOo/con_type',BIB.OO_CON_TYPE)
			BIB.OO_HOST = self.Read('/OOo/host',BIB.OO_HOST)
			BIB.OO_PORT = self.ReadInt('/OOo/port',BIB.OO_PORT)
			BIB.OO_PIPE = self.Read('/OOo/pipe',BIB.OO_PIPE)
			BIB.OO_HILIGHT = self.ReadBool('/OOo/hilight',BIB.OO_HILIGHT)
			BIB.OO_AUTO_UPDATE = self.ReadBool('/OOo/auto_update',BIB.OO_AUTO_UPDATE)
			BIB.OO_CREATE_BIB = self.ReadBool('/OOo/create_bib',BIB.OO_CREATE_BIB)
		if self.HasGroup('Lyx'):	# LyX settings
			BIB.LYX_PIPE = self.Read('/Lyx/lyx_pipe',BIB.LYX_PIPE)
		if self.HasGroup('printer'):	# printer settings
			if self.HasEntry('/printer/format'): BIB.PRINTER_FORMAT = eval(self.Read('/printer/format'))
			if self.HasEntry('/printer/colors'): BIB.PRINTER_COLORS = eval(self.Read('/printer/colors'))
			if self.HasEntry('/printer/style'): BIB.PRINTER_STYLE = eval(self.Read('/printer/style'))
			BIB.PRINTER_USE_OOo_FORMAT = self.ReadBool('/printer/use_ooo_format',BIB.PRINTER_USE_OOo_FORMAT)
		if self.HasGroup('path'):	# path settings
			BIB.FILES = self.Read('/path/files',BIB.FILES)
			head, tail = os.path.split(BIB.FILES) # We need to ensure that there is no trailing / in the path
			if not tail: BIB.FILES = head
			BIB.MOVEAUTO = self.ReadBool('/path/moveauto',BIB.MOVEAUTO)
		if self.HasGroup('pubmed'):	# pubmed settings
			BIB.PUBMED = self.Read('/pubmed/pubmed',BIB.PUBMED)
			BIB.PUBMEDVIEW = self.Read('/pubmed/pubmedview',BIB.PUBMEDVIEW)
			if self.HasEntry('/pubmed/proxies'): BIB.PROXIES = eval(self.Read('/pubmed/proxies'))
		# reading shortcut list from $HOME/.bibus/Shortcuts/
		tmp = os.listdir( os.path.join( wx.StandardPaths.Get().GetUserDataDir(), "Shortcuts") )
		if tmp: BIB.SHORTCUTS = tmp

	def writeConfig(self,dbSave=True):
		"""Write config file. If dbSave=True save db config
		We use the self values in BIB module """
		# Now we save only when it is modified between BIB and BIBbase
		# and we delete the keys not needed any more
		# We put bDeleteGroupIfEmpty=False in DeleteEntry
		# because wx.2.4 crash when the category becomes empty
		# it is ok under wx.2.5 to put True
		self.setFilePath()
		#
		self.SetPath('/general')
		if BIBbase.WP != BIB.WP:
			self.Write('wp',BIB.WP)		# type of word processor OOo / mswDoc
		elif self.HasEntry('wp'):
			self.DeleteEntry('wp',False)
		#
		if BIBbase.TEXT_CLIPBOARD != BIB.TEXT_CLIPBOARD:
			self.Write('text_clipboard',BIB.TEXT_CLIPBOARD)		# type of clipboard format (export filter name)
		elif self.HasEntry('text_clipboard'):
			self.DeleteEntry('text_clipboard',False)			
		#
		self.SetPath('/display')
		if BIBbase.LIST_DISPLAY != BIB.LIST_DISPLAY:
			self.Write('fields',repr(BIB.LIST_DISPLAY))		# Fields displayed in reflist wx.ListCtrl
		else:
			if self.HasEntry('fields'): self.DeleteEntry('fields',False)
		#
		if BIBbase.LIST_ORDER != BIB.LIST_ORDER:
			self.Write('order',BIB.LIST_ORDER)				# Display order in reflist
		else:
			if self.HasEntry('order'): self.DeleteEntry('order',False)
		#
		if BIBbase.LIST_HOW != BIB.LIST_HOW:
			self.Write('how',BIB.LIST_HOW)					# how we order in reflist 'ASC'|'DESC'
		else:
			if self.HasEntry('how'): self.DeleteEntry('how',False)
		#
		if BIBbase.LIST_COL_SIZE != BIB.LIST_COL_SIZE:
			self.Write('col_size', repr(BIB.LIST_COL_SIZE) )	# tuple of column sizes
		else:
			if self.HasEntry('col_size'): self.DeleteEntry('col_size',False)
		#
		if BIBbase.DISPLAY_FORMAT != BIB.DISPLAY_FORMAT:
			self.WriteInt('display_format',BIB.DISPLAY_FORMAT)		# True if we use HTML format to display ref in the bottom-right panel
		else:
			if self.HasEntry('display_format'): self.DeleteEntry('display_format',False)
		#
		if BIBbase.KEY_COLOR != BIB.KEY_COLOR:
			self.Write('key_color',repr(BIB.KEY_COLOR.Get()))		# color display of keys (Title, AUthor, etc..) in textCtrl-1
		else:
			if self.HasEntry('key_color'): self.DeleteEntry('key_color',False)
		#
		if BIBbase.TEXT_COLOR != BIB.TEXT_COLOR:
			self.Write('text_color',repr(BIB.TEXT_COLOR.Get()))	# color display of text (Title, AUthor, etc..) in textCtrl-1
		else:
			if self.HasEntry('text_color'): self.DeleteEntry('text_color',False)
		#
		if BIBbase.EDIT != BIB.EDIT:
			self.Write('ref_editor',repr(BIB.EDIT))			# ReferenceEditor + Reference display in panel bottom-right of Main window
		else:
			if self.HasEntry('ref_editor'): self.DeleteEntry('ref_editor',False)
		#
		self.SetPath('/search')
		if BIBbase.BIB_SEARCH_FIELDS != BIB.BIB_SEARCH_FIELDS:
			self.Write('fields',repr(BIB.BIB_SEARCH_FIELDS))
		else:
			if self.HasEntry('fields'): self.DeleteEntry('fields',False)
		#
		self.SetPath('/db')
		if BIBbase.DB_STARTUP != BIB.DB_STARTUP:
			self.WriteInt('last_db',BIB.DB_STARTUP)
		else:
			if self.HasEntry('last_db'): self.DeleteEntry('last_db',False)
		#
		if BIBbase.STORE_PASSWD != BIB.STORE_PASSWD:
			self.WriteBool('store_passwd',BIB.STORE_PASSWD)
		else:
			if self.HasEntry('store_passwd'): self.DeleteEntry('store_passwd',False)
		#
		if BIBbase.DB_TYPE != BIB.DB_TYPE:
			self.Write('type',BIB.DB_TYPE)
		else:
			if self.HasEntry('type'): self.DeleteEntry('type',False)
		#
		if BIBbase.DUPLICATES_TEST != BIB.DUPLICATES_TEST:
			self.WriteBool('duplicates_test',BIB.DUPLICATES_TEST)
		else:
			if self.HasEntry('duplicates_test'): self.DeleteEntry('duplicates_test',False)
		#
		if BIBbase.DUPLICATES != BIB.DUPLICATES:
			self.Write('duplicates',repr(BIB.DUPLICATES))
		else:
			if self.HasEntry('duplicates'): self.DeleteEntry('duplicates',False)
		#
		if BIBbase.DUPLICATES_CASE != BIB.DUPLICATES_CASE:
			self.WriteBool('duplicates_case',BIB.DUPLICATES_CASE)
		else:
			if self.HasEntry('duplicates_case'): self.DeleteEntry('duplicates_case',False)
		#
		if BIBbase.DUPLICATES_KEEP_OLD != BIB.DUPLICATES_KEEP_OLD:
			self.WriteBool('duplicates_keep_old',BIB.DUPLICATES_KEEP_OLD)
		else:
			if self.HasEntry('duplicates_keep_old'): self.DeleteEntry('duplicates_keep_old',False)
		#
		if dbSave:					# we save db
			if BIB.DB_TYPE == 'MySQL':
				self.SetPath('/mysql')
				if BIBbase.DB_NAME != BIB.DB_NAME:
					self.Write('name',BIB.DB_NAME)
				else:
					if self.HasEntry('name'): self.DeleteEntry('name',False)
				#
				if BIBbase.USER != BIB.USER:
					self.Write('user',BIB.USER)
				else:
					if self.HasEntry('user'): self.DeleteEntry('user',False)
				#
				if BIBbase.PASSWORD != BIB.PASSWORD:
					self.Write('passwd',BIB.PASSWORD)	# PASSWORD='' if not to be stored
				else:
					if self.HasEntry('passwd'): self.DeleteEntry('passwd',False)
				#
				if BIBbase.HOST != BIB.HOST:
					self.Write('host',BIB.HOST)
				else:
					if self.HasEntry('host'): self.DeleteEntry('host',False)
				#
				if BIBbase.PORT != BIB.PORT:
					self.WriteInt('port',BIB.PORT)
				else:
					if self.HasEntry('port'): self.DeleteEntry('port',False)
				#
				if BIBbase.SOCKET != BIB.SOCKET:
					self.Write('socket',BIB.SOCKET)
				else:
					if self.HasEntry('socket'): self.DeleteEntry('socket',False)
				#
			elif BIB.DB_TYPE == 'SQLite':
				self.SetPath('/sqlite')
				if BIBbase.SQLiteFile != BIB.SQLiteFile:
					self.Write('file',BIB.SQLiteFile)
				else:
					if self.HasEntry('file'): self.DeleteEntry('file',False)
				#
				if BIBbase.SQLiteUSER != BIB.SQLiteUSER:
					self.Write('user',BIB.SQLiteUSER)
				else:
					if self.HasEntry('user'): self.DeleteEntry('user',False)
		#
		self.SetPath('/window')
		if BIBbase.WX != BIB.WX:
			self.WriteInt('x',BIB.WX)
		else:
			if self.HasEntry('x'): self.DeleteEntry('x',False)
		#
		if BIBbase.WY != BIB.WY:
			self.WriteInt('y',BIB.WY)
		else:
			if self.HasEntry('y'): self.DeleteEntry('y',False)
		#
		if BIBbase.WIDTH != BIB.WIDTH:
			self.WriteInt('width',BIB.WIDTH)
		else:
			if self.HasEntry('width'): self.DeleteEntry('width',False)
		#
		if BIBbase.HEIGHT != BIB.HEIGHT:
			self.WriteInt('height',BIB.HEIGHT)
		else:
			if self.HasEntry('height'): self.DeleteEntry('height',False)
		#
		if BIBbase.SASH_LIST != BIB.SASH_LIST:
			self.WriteInt('sash_list',BIB.SASH_LIST)
		else:
			if self.HasEntry('sash_list'): self.DeleteEntry('sash_list',False)
		#
		if BIBbase.SASH_KEYTREE != BIB.SASH_KEYTREE:
			self.WriteInt('sash_keytree',BIB.SASH_KEYTREE)
		else:
			if self.HasEntry('sash_keytree'): self.DeleteEntry('sash_keytree',False)
		#
		if BIBbase.REF_Ed_WIDTH != BIB.REF_Ed_WIDTH:
			self.WriteInt('ref_width',BIB.REF_Ed_WIDTH)
		else:
			if self.HasEntry('ref_width'): self.DeleteEntry('ref_width',False)
		#
		if BIBbase.REF_Ed_X != BIB.REF_Ed_X:
			self.WriteInt('ref_x',BIB.REF_Ed_X)
		else:
			if self.HasEntry('ref_x'): self.DeleteEntry('ref_x',False)
		#
		if BIBbase.REF_Ed_Y != BIB.REF_Ed_Y:
			self.WriteInt('ref_y',BIB.REF_Ed_Y)
		else:
			if self.HasEntry('ref_y'): self.DeleteEntry('ref_y',False)
		#
		if BIBbase.IMPORT_WIDTH != BIB.IMPORT_WIDTH:
			self.WriteInt('import_width',BIB.IMPORT_WIDTH)
		else:
			if self.HasEntry('import_width'): self.DeleteEntry('import_width',False)
		#
		if BIBbase.IMPORT_HEIGHT != BIB.IMPORT_HEIGHT:
			self.WriteInt('import_height',BIB.IMPORT_HEIGHT)
		else:
			if self.HasEntry('import_height'): self.DeleteEntry('import_height',False)
		#
		if BIBbase.IMPORT_X != BIB.IMPORT_X:
			self.WriteInt('import_x',BIB.IMPORT_X)
		else:
			if self.HasEntry('import_x'): self.DeleteEntry('import_x',False)
		#
		if BIBbase.IMPORT_Y != BIB.IMPORT_Y:
			self.WriteInt('import_y',BIB.IMPORT_Y)
		else:
			if self.HasEntry('import_y'): self.DeleteEntry('import_y',False)
		#
		if BIBbase.IMPORT_FORMAT != BIB.IMPORT_FORMAT:
			self.WriteInt('import_format',BIB.IMPORT_FORMAT)
		else:
			if self.HasEntry('import_format'): self.DeleteEntry('import_format',False)
		#
		if BIBbase.SEARCH_WIDTH != BIB.SEARCH_WIDTH:
			self.WriteInt('search_width',BIB.IMPORT_WIDTH)
		else:
			if self.HasEntry('search_width'): self.DeleteEntry('search_width',False)
		#
		if BIBbase.SEARCH_HEIGHT != BIB.SEARCH_HEIGHT:
			self.WriteInt('search_height',BIB.SEARCH_HEIGHT)
		else:
			if self.HasEntry('search_height'): self.DeleteEntry('search_height',False)
		#
		if BIBbase.SEARCH_X != BIB.SEARCH_X:
			self.WriteInt('search_x',BIB.SEARCH_X)
		else:
			if self.HasEntry('search_x'): self.DeleteEntry('search_x',False)
		#
		if BIBbase.SEARCH_Y != BIB.SEARCH_Y:
			self.WriteInt('search_y',BIB.SEARCH_Y)
		else:
			if self.HasEntry('search_y'): self.DeleteEntry('search_y',False)
		#
		if BIBbase.SEARCH_WIDTH != BIB.PUBMED_WIDTH:
			self.WriteInt('pubmed_width',BIB.IMPORT_WIDTH)
		else:
			if self.HasEntry('pubmed_width'): self.DeleteEntry('pubmed_width',False)
		#
		if BIBbase.PUBMED_HEIGHT != BIB.PUBMED_HEIGHT:
			self.WriteInt('pubmed_height',BIB.PUBMED_HEIGHT)
		else:
			if self.HasEntry('pubmed_height'): self.DeleteEntry('pubmed_height',False)
		#
		if BIBbase.PUBMED_X != BIB.PUBMED_X:
			self.WriteInt('pubmed_x',BIB.PUBMED_X)
		else:
			if self.HasEntry('pubmed_x'): self.DeleteEntry('pubmed_x',False)
		#
		if BIBbase.PUBMED_Y != BIB.PUBMED_Y:
			self.WriteInt('pubmed_y',BIB.PUBMED_Y)
		else:
			if self.HasEntry('pubmed_y'): self.DeleteEntry('pubmed_y',False)
		#
		if BIBbase.PUBMED_NB != BIB.PUBMED_NB:
			self.WriteInt('pubmed_nb',BIB.PUBMED_NB)
		else:
			if self.HasEntry('pubmed_nb'): self.DeleteEntry('pubmed_nb',False)
		#
		self.SetPath('/OOo')		# OOo settings
		if BIBbase.OO_CON_TYPE != BIB.OO_CON_TYPE:
			self.WriteInt('con_type',BIB.OO_CON_TYPE)
		else:
			if self.HasEntry('con_type'): self.DeleteEntry('con_type',False)
		#
		if BIBbase.OO_HOST != BIB.OO_HOST:
			self.Write('host',BIB.OO_HOST)
		else:
			if self.HasEntry('host'): self.DeleteEntry('host',False)
		#
		if BIBbase.OO_PORT != BIB.OO_PORT:
			self.WriteInt('port',BIB.OO_PORT)
		else:
			if self.HasEntry('port'): self.DeleteEntry('port',False)
		#
		if BIBbase.OO_PIPE != BIB.OO_PIPE:
			self.Write('pipe',BIB.OO_PIPE)
		else:
			if self.HasEntry('pipe'): self.DeleteEntry('pipe',False)
		#
		if BIBbase.OO_HILIGHT != BIB.OO_HILIGHT:
			self.WriteBool('hilight',BIB.OO_HILIGHT)
		else:
			if self.HasEntry('hilight'): self.DeleteEntry('hilight',False)
		#
		if BIBbase.OO_AUTO_UPDATE != BIB.OO_AUTO_UPDATE:
			self.WriteBool('auto_update',BIB.OO_AUTO_UPDATE)
		else:
			if self.HasEntry('auto_update'): self.DeleteEntry('auto_update',False)
		#
		if BIBbase.OO_CREATE_BIB != BIB.OO_CREATE_BIB:
			self.WriteBool('create_bib',BIB.OO_CREATE_BIB)
		else:
			if self.HasEntry('create_bib'): self.DeleteEntry('create_bib',False)
		#
		self.SetPath('/Lyx')		# LyX settings
		if BIBbase.LYX_PIPE != BIB.LYX_PIPE:
			self.Write('lyx_pipe',BIB.LYX_PIPE)
		else:
			if self.HasEntry('lyx_pipe'): self.DeleteEntry('lyx_pipe',False)	
		#
		self.SetPath('/printer')		# printer settings
		if BIBbase.PRINTER_FORMAT != BIB.PRINTER_FORMAT:
			self.Write('format',repr(BIB.PRINTER_FORMAT))
		else:
			if self.HasEntry('format'): self.DeleteEntry('format',False)
		#
		if BIBbase.PRINTER_COLORS != BIB.PRINTER_COLORS:
			self.Write('colors',repr(BIB.PRINTER_COLORS))
		else:
			if self.HasEntry('colors'): self.DeleteEntry('colors',False)
		#
		if BIBbase.PRINTER_STYLE != BIB.PRINTER_STYLE:
			self.Write('style',repr(BIB.PRINTER_STYLE))
		else:
			if self.HasEntry('style'): self.DeleteEntry('style',False)
		#
		if BIBbase.PRINTER_USE_OOo_FORMAT != BIB.PRINTER_USE_OOo_FORMAT:
			self.WriteBool('use_ooo_format',BIB.PRINTER_USE_OOo_FORMAT)
		else:
			if self.HasEntry('use_ooo_format'): self.DeleteEntry('use_ooo_format',False)
		#
		self.SetPath('/path')	# path settings
		if BIBbase.FILES != BIB.FILES:
			self.Write('files',BIB.FILES)
		else:
			if self.HasEntry('files'): self.DeleteEntry('files',False)
		#
		if BIBbase.MOVEAUTO != BIB.MOVEAUTO:
			self.WriteBool('moveauto',BIB.MOVEAUTO)
		else:
			if self.HasEntry('moveauto'): self.DeleteEntry('moveauto',False)
		#
		self.SetPath('/pubmed')	# pubmed settings
		if BIBbase.PUBMED != BIB.PUBMED:
			self.Write('pubmed',BIB.PUBMED)
		else:
			if self.HasEntry('pubmed'): self.DeleteEntry('pubmed',False)
			
		if BIBbase.PUBMEDVIEW != BIB.PUBMEDVIEW:
			self.Write('pubmedview',BIB.PUBMEDVIEW)
		else:
			if self.HasEntry('pubmedview'): self.DeleteEntry('pubmedview',False)
			
		if BIBbase.PROXIES != BIB.PROXIES:
			self.Write('proxies',repr(BIB.PROXIES))
		else:
			if self.HasEntry('proxies'): self.DeleteEntry('proxies',False)
		#
		self.Flush()	# save the config changes

#---------------------- Shortcuts ----------------------------------------------
	def readShortcut(self,shortname):
		"""Load shortcut number n"""
		try:
			f = file( os.path.join(wx.StandardPaths.Get().GetUserDataDir(), "Shortcuts", shortname), 'r' )
			short = cPickle.load(f)		# we unpickle the file then check that it is a list of strings
			f.close()
			if type(short) != list: raise TypeError('notalist')
			else:
				for i in short:
					if type(i) not in (unicode,str): raise TypeError('notastring')
		except:
			short = []
		BIB.SHORT[ shortname ] = short

	def writeShortcuts(self):
		for shortname in BIB.SHORTCUTS:
			if BIB.SHORT.has_key( shortname ):
				f = file( os.path.join(wx.StandardPaths.Get().GetUserDataDir(), "Shortcuts", shortname), 'w' )
				cPickle.dump( BIB.SHORT[ shortname ] , f)
				f.close()

#---------------------- OOo styles ---------------------------------------------

	def writeCurrentStyle(self,style):
		"""Save the current style filename under key style/self"""
		self.setFilePath()
		self.Write('/style/current',style)

	def getCurrentStyle(self):
		"""Return the current style"""
		self.setFilePath()
		return self.Read('/style/current')

# --------------------------- FirstStart ---------------------------------------
	def firstStartDone(self):
		"""Set to False after first start"""
		self.setFilePath()
		self.WriteBool('/general/firststart',False)
		self.Write('/general/config_version',BIB.BIBUS_VERSION)
		self.Flush()

	def userDefined(self,dbType):
		"""Return true if the username was already defined in the config"""
		self.setFilePath()
		return self.HasEntry('/%s/user'%dbType)
		

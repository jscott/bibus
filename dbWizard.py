# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# Part of the Wizard code has been copied from the example "Simple Wizard" included with
# wxPython
#
import wx
import wx.wizard
#import dbBibMySQL
import getpass
import BIB


#----------------------------------------------------------------------
class TitledPage(wx.wizard.WizardPageSimple):
	def __init__(self, parent, title):
		wx.wizard.WizardPageSimple.__init__(self, parent)
		self.sizer = self.__makePageTitle(title)

	def __makePageTitle(self, title):
		sizer = wx.BoxSizer(wx.VERTICAL)
		self.SetSizer(sizer)
		title = wx.StaticText(self, -1, title)
		title.SetFont(wx.Font(18, wx.SWISS, wx.NORMAL, wx.BOLD))
		sizer.Add(title, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
		sizer.Add(wx.StaticLine(self, -1), 0, wx.EXPAND|wx.ALL, 5)
		return sizer

#----------------------------------------------------------------------

class dbWizard(wx.wizard.Wizard):
	def __init__(self,parent):
	# Create the wizard and the pages
		self.ID_wiz = wx.NewId()
		self.parent = parent
		self.db = None
		wx.wizard.Wizard.__init__(self,parent, self.ID_wiz, _("Database choice"))
		page1 = TitledPage(self, _("%s connection parameters")%BIB.DB_TYPE)
		page2 = TitledPage(self, _("Database Name"))
		self.page1 = page1
		self.page2 = page2
#CPAO ->
#	Save the BIB values to retore these ones on a cancel event
		# save original BIB values
		self.Original_BIB_USER = BIB.USER
		self.Original_BIB_SOCKET = BIB.SOCKET
		self.Original_BIB_PORT = BIB.PORT
		self.Original_BIB_HOST = BIB.HOST
		self.Original_BIB_PASSWORD = BIB.PASSWORD
		# page1 Layout
		page1.sizer.Add(wx.StaticText(page1, -1, u""))
		page1.sizer1 = wx.FlexGridSizer(5,2,5,5)
		page1.sizer1.AddGrowableCol(1)
		self.user=wx.TextCtrl(page1,-1)
		self.user.SetValue(BIB.USER)
		self.passwd=wx.TextCtrl(page1,-1,style=wx.TE_PASSWORD)
		self.passwd.SetValue(BIB.PASSWORD)
		self.host=wx.TextCtrl(page1,-1)
		self.host.SetValue(BIB.HOST)
		self.port=wx.TextCtrl(page1,-1)
		self.port.SetValue(str(BIB.PORT))
		self.unix_socket=wx.TextCtrl(page1,-1)
		self.unix_socket.SetValue(BIB.SOCKET)
		page1.sizer1.Add(wx.StaticText(page1, -1, _("UserName")),wx.ALIGN_CENTER_VERTICAL,wx.ALIGN_RIGHT)
		page1.sizer1.Add(self.user, 0, wx.EXPAND)
		page1.sizer1.Add(wx.StaticText(page1, -1, _("PassWord")),wx.ALIGN_CENTER_VERTICAL,wx.ALIGN_RIGHT)
		page1.sizer1.Add(self.passwd, 0, wx.EXPAND)
		page1.sizer1.Add(wx.StaticText(page1, -1, _("Host")),wx.ALIGN_CENTER_VERTICAL,wx.ALIGN_RIGHT)
		page1.sizer1.Add(self.host, 0, wx.EXPAND)
		page1.sizer1.Add(wx.StaticText(page1, -1, _("Port")),wx.ALIGN_CENTER_VERTICAL,wx.ALIGN_RIGHT)
		page1.sizer1.Add(self.port, 0, wx.EXPAND)
		page1.sizer1.Add(wx.StaticText(page1, -1, _("Socket")),wx.ALIGN_CENTER_VERTICAL,wx.ALIGN_RIGHT)
		page1.sizer1.Add(self.unix_socket, 0, wx.EXPAND)
		page1.sizer.Add(page1.sizer1, 0, wx.EXPAND)
		self.FitToPage(page1)
		self.Layout()
		self.Show(1)

		# page2 Layout
		self.dbChoice = wx.Choice(self.page2,-1,choices=[])
		self.page2.sizer.Add(self.dbChoice, 0, wx.ALIGN_CENTER)

		# Use the convenience Chain function to connect the pages
		wx.wizard.WizardPageSimple_Chain(page1, page2)

		wx.wizard.EVT_WIZARD_PAGE_CHANGED(self, self.ID_wiz, self.OnWizPageChanged)
#CPAO ->
#	This event is no longer needed, replaced by FINISHED event
#		wx.EVT_WIZARD_PAGE_CHANGING(self, self.ID_wiz, self.OnWizPageChanging)
		wx.wizard.EVT_WIZARD_CANCEL(self, self.ID_wiz, self.OnWizCancel)
		wx.wizard.EVT_WIZARD_FINISHED(self, self.ID_wiz, self.OnWizFinished)

	def __page2(self):
		try:
			self.db.dbConnection.close()
			self.db.dbCursor.close()
		except AttributeError:
			self.db = None
		self.db = None
		saved_name = BIB.DB_NAME			# we save the old DB_NAME
		BIB.DB_NAME = ''
		self.db = BIB.DB_MODULE.dbBib(self.parent)	# we connect with no db name
		BIB.DB_NAME = saved_name				# restore db name
		self.databases=[]
		try:
			databases=self.db.getDatabases()
			for i in databases:
				self.databases.append(i[0])
		except AttributeError:
			self.db=None
		# page2 Layout
		self.dbChoice.Clear()
		for i in self.databases:
			self.dbChoice.Append(i)
		if BIB.DB_NAME in self.databases: self.dbChoice.SetStringSelection(BIB.DB_NAME)
		else: self.dbChoice.SetSelection(0)

	def OnWizPageChanged(self, evt):
		page = evt.GetPage()
		if page == self.page2:
			BIB.USER = self.user.GetValue()
			BIB.SOCKET = self.unix_socket.GetValue()
			BIB.PORT = int(self.port.GetValue())
			BIB.HOST = self.host.GetValue()
			BIB.PASSWORD = self.passwd.GetValue()
			self.__page2()

#
#CPAO ->
#	Tracking the FINISHED Event should be more efficient !
#
#	def OnWizPageChanging(self, evt):
#		page = evt.GetPage()
#		if (page == self.page2) & evt.GetDirection():
#		# we are leaving the wizard by clickink the Finish button
#		# we first check that the connection is working then we return the dbBib connection
#		# before leaving
#			try:
#				self.db.selectDatabase(self.dbChoice.GetStringSelection())
#				self.db.tableRef = BIB.DB_TABLE_REF
#				self.db.tableKey = BIB.DB_TABLE_KEY
#				self.db.tableLink = BIB.DB_TABLE_LINK
#				self.db.tableQuery = BIB.DB_TABLE_QUERY
#				self.db.check_db()	# pop an error if some fields are missing
#			except (AttributeError, TypeError):
#				self.db = None
#
	def OnWizFinished(self, evt):
		# we are leaving the wizard by clickink the Finish button
		# we first check that the connection is working then we return the dbBib connection
		# before leaving
		try:
			self.db.selectDatabase(self.dbChoice.GetStringSelection())
			self.db.tableRef = BIB.DB_TABLE_REF
			self.db.tableKey = BIB.DB_TABLE_KEY
			self.db.tableLink = BIB.DB_TABLE_LINK
			self.db.tableQuery = BIB.DB_TABLE_QUERY
			self.db.tableModif = BIB.DB_TABLE_MODIF
			self.db.tableFile = BIB.DB_TABLE_FILE
			self.db.check_db()	# pop an error if some fields are missing
		except (AttributeError, TypeError):
			self.db = None

	def OnWizCancel(self, evt):
#CPAO ->
#	Restore BIB original values for some WriteConfig that will later erease original one if not restored
		#Restore previous BIB values
		BIB.USER = self.Original_BIB_USER
		BIB.SOCKET = self.Original_BIB_SOCKET
		BIB.PORT = self.Original_BIB_PORT
		BIB.HOST = self.Original_BIB_HOST
		BIB.PASSWORD = self.Original_BIB_PASSWORD
		#No opened BD
		self.db = None

	def getDbSelected(self):
		"""Return db. None if no selected db. Clear password for security"""
		if self.db:
			BIB.USER = self.user.GetValue()
			BIB.SOCKET = self.unix_socket.GetValue()
			BIB.PORT = int(self.port.GetValue())
			BIB.HOST = self.host.GetValue()
			BIB.DB_NAME = self.db.db
	#		if BIB.STORE_PASSWD:	BIB.PASSWORD = self.passwd.GetValue()
	#		else:	BIB.PASSWORD = ''
			BIB.PASSWORD = self.passwd.GetValue()
			if BIB.DB_STARTUP == 0: # we store the current database
				BIB.CONFIG.writeConfig(True)
			self.passwd = u''
			return self.db
		else:
			return None



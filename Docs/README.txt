Creation date: 2004.05.18
Last revision: 2005.02.13

**************************************************
** IF YOU ARE UPGRADING FROM A PREVIOUS VERSION **
** YOU MUST READ importantNote.txt              **
**************************************************

Bibus is a bibliographic database.
It has been developed with OpenOffice.org (http://www.openoffice.org/) in mind.
This means that the database is modeled on OpenOffice.org bibliographic database engine.
As such it has the features and the limitations of this format.
This may change in the future.

*************
** License **
*************
 Copyright 2004 Pierre Martineau <pmartino@users.sourceforge.net>

 Bibus is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Bibus is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Bibus; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Other files included in the distribution:
Part of the Wizard code included in dbWizard.py and dbBibSQLite.py has been copied
            from the example "Simple Wizard" included with wxPython


*******************
** Main Features **
*******************
1) Compatible with two database engines: MySQL <http://www.mysql.com/> and SQLite <http://www.sqlite.org/>.
2) Hierarchical organization of the references with user defined keys.
3) Designed for multiuser.
      - You can share the database between an "unlimited" number of users.
      - Each user will have its own classification.
4) Search engine.
5) On-line PubMed access.
6) Live queries.
7) Connection to OpenOffice.org.
       - You can insert references the open OpenOffice.org text document and
       - format the bibliography directly in Bibus.
       - format the citations in Bibus.
8) Thanks to Python and wxWidget, Bibus should work on most modern platform (linux/gtk; Windows; MacOS; etc...).
9) Foreign language support through unicode and gettext.
       - For the moment, Bibus is available in English and in French (I'm French).
       - Feel free to translate it in your favorite language, this is easy and fast.
         It took me about 1 hour to translate the pot file in French.
10) Import PubMed (Medline), EndNote/Refer and RIS records

******************
** Installation **
******************
The installation is for the moment a bit tricky.
This is due to the fact that Bibus is entirely written in Python (http://www.python.org/)
with the wxWidget library (http://www.wxwidget.org/ and http://www.wxPython.org/)
and that the python-uno bridge installed with OpenOffice.org does not contain
all those libraries.

You must read the doc!

*********************
** Where to go now **
*********************
1) Read LinuxInstall.html for linux and installationWin.txt for Windows
2) Finish with mysql_config.txt if you want to use a MySQL database

Enjoy

PS: If Bibus worked for you and now crash at startup, there is presumably some problem
with the configuration file.
     - Under linux it is an hidden file called .bibus and located in your $HOME.
     - Under Windows95 and up it is a registry key called bibus.
Delete the file/registry key and try to start bibus again.


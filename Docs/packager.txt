If you want to make a package for you favorite linux distribution, it is now
much easier.

1) You can have a look at the debian package.

2) You just have to change the following files:

a) bibus.cfg
You must set in this file 5 environment variables:
Here are the values for Debian.

docdir = /usr/share/doc/bibus/html
licence = /usr/share/doc/bibus/copying
localedir = /usr/share/locale
oopath = /usr/lib/openoffice/program
systemconf = /etc/bibus.config

b) You can eventually set some system-wide variables in bibus.config
This file is supposed to go in /etc/

c) Now package Bibus, if possible in 2 packages:

bibus
bibus-doc-en

in the future we may have other languages for the doc



As an example, in Debian we have the following files:

bibus.deb package
-----------------
/etc/bibus.config              # system-wide configuration
/usr/bin/bibus                 # sh script starting Bibus (look in the debian directory)
/usr/lib/menu/bibus            # install Bibus in the system menu with icon
/usr/share/doc/bibus/          # contains the doc
/usr/share/man/man1/bibus.1.gz # man page
/usr/share/locale/fr/LC_MESSAGES/bibus.mo # gettext translation
idem for other languages (pt for the moment)
/usr/share/pixmaps/bibus.xpm   # Bibus icon. There are also png and svg versions availables
/usr/share/bibus/              # python scripts. The Bibus directory except locale/ Docs/ debian/ Pixmaps/ and Setup/

bibus-doc-en.deb package
------------------------
/usr/share/doc/bibus/html/ScreenShots/  # screenshots png files
/usr/share/doc/bibus/html/en/           # english doc in html

3) There is a makefile in Setup than can help you:

a)
make -f Setup/Makefile install DESTDIR=/usr python=/usr/bin/python

will install:
bibus in /usr/share/bibus/
man in /usr/share/man/man1/
locales in /usr/share/locales/

b)
make -f Setup/Makefile install-doc-en DESTDIR=/usr python=/usr/bin/python

will install the doc in /usr/share/doc/bibus/

c)
You can compile all the files (in pyc and pyo) with:

make -f Setup/Makefile compile DESTDIR=/usr python=/usr/bin/python

d)
You can make freedesktop compatible .desktop entries with something like:

install -m644 debian/bibus.desktop /usr/share/applications/
install -m644 Pixmaps/bibus.png /usr/share/icons/hicolor/48x48/apps/

Don't forget to make a copy of debian/bibus.desktop before and edit it to suit your preferred distribution.

e)
You should define a bibus command in /usr/bin
look at the debian/bibus.sh script:

	#!/bin/sh
	export LD_LIBRARY_PATH=/usr/lib/openoffice/program
	export PYTHONPATH=/usr/lib/openoffice/program
	/usr/bin/python /usr/share/bibus/bibus.py
	
This should be OK for most distribution except for the OOo directory name.
With modern distributions (debian with OOo2), you don't need to set the PYTHONPATH because 
the OOo python scripts are located in the system directory

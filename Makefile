# you presumably will only have to change the three first lines below
DESTDIR = /usr/local
python = /usr/bin/python
oopath = /usr/lib/openoffice/program
ooure = /usr/lib/openoffice.org/basis-link/ure-link/lib
oobasis = /usr/lib/openoffice/basis-link/program
#
prefix = $(DESTDIR)
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
datadir = $(prefix)/share
sysconfdir = /etc
mandir = $(prefix)/share/man
man1dir = $(mandir)/man1

define install-files
	install -d $(datadir)/bibus
	install -m644 *.py MySQL_Bibus.ini $(datadir)/bibus/
	#
	for dir in Export Format Import StyleEditor FirstStart bibOOo db_models Pixmaps Data LyX Utilities; do \
		find $$dir ! -wholename '*CVS*' -a -type f -exec install -m644 '{}' -D $(datadir)/bibus/'{}' ';' ; \
	done
	install -m644 Setup/UnoConnectionListener.odg -D $(datadir)/bibus/Setup/UnoConnectionListener.odg
	# locale files. We look for all the directory in locale/
	for dir in $(wildcard locale/*); do \
		if [ -d $$dir ] && [ $$dir != "locale/CVS" ]; then \
			install -m644 $$dir/LC_MESSAGES/bibus.mo -D $(datadir)/$$dir/LC_MESSAGES/bibus.mo ; \
		fi; \
	done
	# bibus.config in /etc
	install -m644 bibus.config -D $(sysconfdir)/bibus.config
	# man page
	install -m644 Setup/bibus.1 -D $(man1dir)/bibus.1

	# freedesktop icon and shortcut
	install -m644 Pixmaps/bibus.png -D $(datadir)/icons/hicolor/48x48/apps/bibus.png
	install -m644 Setup/bibus.desktop -D $(datadir)/applications/bibus.desktop
	# The following lines are presumably not needed since icons/desktop are in standard locations
	#echo 'Exec=$(bindir)/bibus' >> $(datadir)/applications/bibus.desktop
	#echo 'Icon=$(datadir)/icons/hicolor/48x48/apps/bibus.png' >> $(datadir)/applications/bibus.desktop

	# bibus command
	install -d $(bindir)
	ln -sf $(datadir)/bibus/bibusStart.py $(bindir)/bibus
	chmod 755 $(datadir)/bibus/bibusStart.py

	# basic doc files
	install -m755 -d $(datadir)/doc/bibus
	install -m644 Docs/*.txt Docs/CHANGELOG Docs/copying $(datadir)/doc/bibus

	# generating bibus.cfg file
	echo '[PATH]' > $(datadir)/bibus/bibus.cfg
	echo 'python = $(python)' >> $(datadir)/bibus/bibus.cfg
	echo 'oopath = $(oopath)' >> $(datadir)/bibus/bibus.cfg
	echo 'ooure = $(ooure)' >> $(datadir)/bibus/bibus.cfg
	echo 'oobasis = $(oobasis)' >> $(datadir)/bibus/bibus.cfg
	echo 'docdir = $(datadir)/doc/bibus/html' >> $(datadir)/bibus/bibus.cfg
	echo 'licence = $(datadir)/doc/bibus/copying' >> $(datadir)/bibus/bibus.cfg
	echo 'localedir = $(datadir)/locale' >> $(datadir)/bibus/bibus.cfg
	echo 'systemconf = $(sysconfdir)/bibus.config' >> $(datadir)/bibus/bibus.cfg
endef

define install-doc-en
	# copying docs in from Docs/html/en/ for bibus-doc-en
	cd Docs;\
	find html/en/* -type f -exec install -m644 '{}' -D $(datadir)/doc/bibus/'{}' ';'
endef

define compile
	# compile recursively all the python files located in $(datadir)/bibus
	$(python) -c "import compileall ; compileall.compile_dir('$(datadir)/bibus')"
	$(python) -O -c "import compileall ; compileall.compile_dir('$(datadir)/bibus')"
endef

install-files:
	$(install-files)

install-doc-en:
	$(install-doc-en)

install:
	$(install-files)
	$(install-doc-en)
	$(compile)

	# write uninstaller in $(datadir)/bibus/Setup/uninstall.sh
		echo "#!/bin/sh" > $(datadir)/bibus/Setup/uninstall.sh
		echo "rm -rf $(datadir)/bibus" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "rm $(bindir)/bibus" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "find $(datadir)/locale -name bibus.mo -exec rm -f {} \;" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "rm $(sysconfdir)/bibus.config" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "rm $(man1dir)/bibus.1" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "rm -rf $(datadir)/doc/bibus" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "rm $(datadir)/applications/bibus.desktop" >> $(datadir)/bibus/Setup/uninstall.sh
		echo "rm $(datadir)/icons/hicolor/48x48/apps/bibus.png" >> $(datadir)/bibus/Setup/uninstall.sh
	chmod 744 $(datadir)/bibus/Setup/uninstall.sh
	# end uninstaller

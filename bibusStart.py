#!/usr/bin/env python
# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import ConfigParser,os,sys,urllib,urlparse
cp=ConfigParser.ConfigParser()
fileName = sys.argv[0]
while os.path.islink(fileName): fileName = os.readlink(fileName)
sourcedir = os.path.abspath( os.path.dirname(fileName) )
cp.read( os.path.join(sourcedir,'bibus.cfg') )
# We read first, then if it is not null we convert to absolute path
# otherwise, empty path will be converted to the current directory path
python = cp.get('PATH','python')
if python: python = os.path.abspath(python)
oopath = os.path.abspath( cp.get('PATH','oopath') )
if oopath: oopath = os.path.abspath(oopath)
oobasis = cp.get('PATH','oobasis')
if oobasis: oobasis = os.path.abspath(oobasis)
ooure = cp.get('PATH','ooure')
if ooure: ooure = os.path.abspath(ooure)
# set environ
# OOo version 3.0 (and up ?)
if ooure: # This is an OOo >= 3 install
	if sys.platform.startswith('win'):
		LIBPATH = 'PATH'	# Windows
		QUOTE='"'
		os.environ["URE_BOOTSTRAP"] = urlparse.urlunparse( ('file','',urllib.pathname2url( os.path.join(oopath,"fundamental.ini") ),'','','') )
	else:
		LIBPATH = 'LD_LIBRARY_PATH'	# linux (unix)
		QUOTE=''
		os.environ["URE_BOOTSTRAP"] = urlparse.urlunparse( ('file','',urllib.pathname2url( os.path.join(oopath,"fundamentalrc") ),'','','') )
	try:
		os.environ['PYTHONPATH'] = os.pathsep.join( (oobasis,os.environ['PYTHONPATH']) )
	except KeyError:
		os.environ['PYTHONPATH'] = oobasis
	try:
		os.environ[LIBPATH] = os.pathsep.join( (oobasis,ooure,os.environ[LIBPATH]) )
	except KeyError:
		os.environ[LIBPATH] = os.pathsep.join( (oobasis,ooure) )
# OOo version 2.x
else:
	if sys.platform.startswith('win'):
		LIBPATH = 'PATH'	# Windows
		QUOTE='"'
	else:
		LIBPATH = 'LD_LIBRARY_PATH'	# linux (unix)
		QUOTE=''
	try:
		os.environ['PYTHONPATH'] = os.pathsep.join( (oopath,os.environ['PYTHONPATH']) )
	except KeyError:
		os.environ['PYTHONPATH'] = oopath
	try:
		os.environ[LIBPATH] = os.pathsep.join( (oopath,os.environ[LIBPATH]) )
	except KeyError:
		os.environ[LIBPATH] = oopath
# starting Bibus
os.execl( python , os.path.basename(python) , QUOTE+ os.path.join(sourcedir,'bibus.py') +QUOTE )

